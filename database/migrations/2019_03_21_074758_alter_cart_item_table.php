<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCartItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cart_items', function(Blueprint $table)
        {
            DB::statement('ALTER TABLE `cart_items` MODIFY `product_id` INTEGER UNSIGNED NULL;');
            $table->string('url_amazon',300)->nullable();
            $table->string('url_image',300)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart_items', function(Blueprint $table)
        {
            $table->dropColumn('url_amazon');
            $table->dropColumn('url_image');
        });
    }
}
