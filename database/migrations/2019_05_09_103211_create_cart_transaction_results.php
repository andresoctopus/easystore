<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartTransactionResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('cart_transaction_results', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('cart_id');
		    $table->string('currency');
		    $table->string('value');
		    $table->string('status');
		    $table->string('message');
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('cart_transaction_results');
    }
}
