@extends('shop::layouts.master')
@section('content-wrapper')
<div class="container py-3">
    <div class="row">
        <div class="col-12 col-md-8 offset-md-2 mt-5 text-center">
            <h2 style="color:#c02c2b; font-size:30px;">¡Ya estás a pocos pasos!</h2>
            <hr style="border-top: 2px solid #333; width: 100px;">
            <p style="color:#777777;">Solo necesitamos que por seguridad, des click en el siguiente botón y así te enviaremos a nuestra pasarela de pagos.</p>
        </div>
        <div class="col-12 text-center my-3">
            <form method="post" action="{!! $url !!}">
                <input name="merchantId" type="hidden" value="{{$merchantId}}">
                <input name="accountId" type="hidden" value="{{$accountId}}">
                <input name="description" type="hidden" value="{{$referenceCode}}">
                <input name="referenceCode" type="hidden" value="{{$referenceCode}}">
                <input name="amount" type="hidden" value="{{$amount}}">
                <input name="tax" type="hidden" value="{{$tax}}">
                <input name="taxReturnBase" type="hidden" value="{{$taxReturnBase}}">
                <input name="currency" type="hidden" value="{{$currency}}">
                <input name="signature" type="hidden" value="{{$signature}}">
                <input name="test" type="hidden" value="{{$test}}">
                <input name="buyerEmail" type="hidden" value="{{$buyerEmail}}">
                <input name="responseUrl" type="hidden" value="{{$responseUrl}}">
                <input name="confirmationUrl" type="hidden" value="{{$confirmUrl}}">
                <input class="btn btn-primary btn-lg pagar mb-3" name="Submit" type="submit" value="Proceder al pago">
            </form>
        </div>
    </div>
</div>
@endsection

</body>
</html>
