@extends('admin::layouts.content')

@section('page_title')
    Editar estado personalizado
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('admin.order_statuses.update', $orderStatus->id) }}">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link"
                           onclick="history.length > 1 ? history.go(-1) : window.location = '{{ url('/admin/dashboard') }}';"></i>

                        Editar estado personalizado
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Guardar estado personalizado
                    </button>
                </div>
            </div>
            <div class="page-content">
                <div class="form-container">
                    @csrf()
                    @method('PUT')

                    <div class="control-group">
                        <label for="name" class="required">Nombre</label>

                        <input type="text"   class="control" name="name" required value="{{$orderStatus->name}}"/>

                    </div>

                    <div class="control-group">
                        <label for="template" class="required">Plantilla</label>

                        <textarea name="template"  class="control" required rows="5">{!! $orderStatus->template !!}</textarea>
                    </div>


                </div>
            </div>

        </form>
    </div>
@stop