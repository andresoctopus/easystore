@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.settings.tax-categories.title') }}
@stop

@section('content')
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Estados de pedido personalizados</h1>
            </div>

            <div class="page-action">
                <a href="{{ route('admin.order_statuses.create') }}" class="btn btn-lg btn-primary">
                    Crear estado
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('orderStatuses','Webkul\Admin\DataGrids\OrderStatusDataGrid')
            {!! $orderStatuses->render() !!}
        </div>
    </div>
@stop