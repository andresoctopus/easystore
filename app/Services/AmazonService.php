<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 14/08/18
 * Time: 07:16 PM
 */

namespace App\Services;


use App\Facades\Amazon;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class AmazonService
{


    /**
     * @param $text
     * @param null $category
     * @param int $page
     * @return AmazonResponseModel
     */
    public function search($text, $category = null, $page = 1)
    {

        $response = new AmazonResponseModel();
        $time_start = microtime(true);

        $result = Amazon::itemSearch($text, $category, $page);
        $products = $result->products();

        $result = Amazon::itemSearch($text, $category, 2);
        $products2 = $result->products();

        Log::debug("this is the products", ['products1' => $products,'products2' => $products2]);

        $products = array_merge($products, $products2);
        $response->data = $products;
        $time_end = microtime(true);
        $response->time = $time_end - $time_start;
        $response->success = true;
        $response->pages = $result->pages();
        $response->current_page = $page;
        return $response;
        try {
        } catch (Exception $ex) {
            Log::error("Error searching in amazon", ['error' => $ex->getMessage()]);
            $time_end = microtime(true);
            $response->time = $time_end - $time_start;
            $response->success = false;
            return $response;
        }
    }

    /**
     * @param $code
     * @return AmazonResponseModel
     */
    public function show($code)
    {
        $response = new AmazonResponseModel();
        $time_start = microtime(true);
        if (Cache::has($code)) {
            $response->data = Cache::get($code);
            $time_end = microtime(true);
            $response->time = $time_end - $time_start;
            $response->success = true;
            return $response;
        }

        $result = Amazon::itemLookup(str_replace(' ', '_', $code), 'All');
        $product = $result->product();
        if ($product == null) {
            $time_end = microtime(true);
            $response->time = $time_end - $time_start;
            $response->success = false;
            return $response;
        }
        if (isset($product->variations)) {
            foreach ($product->variations as $variation) {
                $expiresAt = Carbon::now()->addMinutes(720);
                Cache::put($variation->getUpc(), $variation, $expiresAt);
            }
        }
        $response->data = $product;
        $expiresAt = Carbon::now()->addMinutes(720);
        Cache::put($response->data->getCode(), $response->data, $expiresAt);
        $time_end = microtime(true);
        $response->time = $time_end - $time_start;
        $response->success = true;
        return $response;
        try {
        } catch (Exception $ex) {
            Log::error("Error searching in amazon", ['error' => $ex->getMessage()]);
            $time_end = microtime(true);
            $response->time = $time_end - $time_start;
            $response->success = false;
            return $response;
        }

    }


    /**
     * @param $code
     * @return AmazonResponseModel
     */
    public function find($upc)
    {
        $response = new AmazonResponseModel();
        $time_start = microtime(true);
        if (Cache::has($upc)) {
            $response->data = Cache::get($upc);
            $time_end = microtime(true);
            $response->time = $time_end - $time_start;
            $response->success = true;
            return $response;
        }
        try {
            $result = Amazon::itemFind($upc);
            $product = $result->product();
            if ($product == null) {
                $time_end = microtime(true);
                $response->time = $time_end - $time_start;
                $response->success = false;
                return $response;
            }
            $response->data = $product;
            $expiresAt = Carbon::now()->addMinutes(720);
            Cache::put($response->data->getUpc(), $response->data, $expiresAt);
            $time_end = microtime(true);
            $response->time = $time_end - $time_start;
            $response->success = true;
            return $response;
        } catch (Exception $ex) {
            Log::error("Error searching in amazon", ['error' => $ex->getMessage()]);
            $time_end = microtime(true);
            $response->time = $time_end - $time_start;
            $response->success = false;
            return $response;
        }

    }


}