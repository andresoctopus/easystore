<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 14/08/18
 * Time: 07:16 PM
 */

namespace App\Services;


class AmazonResponseModel
{
    public $data;
    public $pages;
    public $current_page;
    public $success;
    public $time;
    public $message;

}