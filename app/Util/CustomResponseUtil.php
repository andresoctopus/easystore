<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 30/06/18
 * Time: 07:56 PM
 */

namespace App\Util;


class CustomResponseUtil
{
    /**
     * @param string $message
     * @param mixed  $data
     *
     * @return array
     */
    public static function makeResponse($message, $data, $time, $page, $pages, $results)
    {
        return [
            'success' => true,
            'data'    => $data,
            'time'    => $time,
            'page'    => $page,
            'pages'    => $pages,
            'results'    => $results,
            'message' => $message,
        ];
    }

    /**
     * @param string $message
     * @param array  $data
     *
     * @return array
     */
    public static function makeError($message, array $data = [], $time)
    {
        $res = [
            'success' => false,
            'time'    => $time,
            'message' => $message,
        ];

        if (!empty($data)) {
            $res['data'] = $data;
        }

        return $res;
    }
}