<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 30/06/18
 * Time: 09:11 PM
 */

namespace App\Util;


use App\TasaMoneda;
use Illuminate\Support\Facades\Log;

class PriceUtil
{
    public static function calculatePrice($price, $weight)
    {
        if ($price == 0) {
            return 0;
        }
        $precioMod = 0;
        $pesoMod = 0;
        if ($weight <= 1) {
            $pesoMod = $weight * 4;
        } elseif ($weight > 1 && $weight <= 10) {
            $pesoMod = $weight * 3.4;
        } else {
            $pesoMod = $weight * 3;
        }

        if ($price <= 9.9){
            $precioMod = $price * 1.2;
        }
        elseif ($price > 9.99 && $price <= 200){
            $precioMod = $price * 1.17;
        }
        else{
            $precioMod = $price * 1.15;
        }

        $res = $precioMod + $pesoMod;


        return $res;
    }

    /**
     * @param $value
     * @return float|int
     */
    public static function convertCurrency($value)
    {
        return $value;
    }


    private static function redondear($valor, $factor = 100)
    {
        //return $valor;
        if ((int)$valor < 0) {
            $valor = 100;
        }

        $valor = ceil($valor / $factor) * $factor;

        return $valor;
    }
}