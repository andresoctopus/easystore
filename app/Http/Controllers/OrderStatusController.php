<?php

namespace App\Http\Controllers;

use App\Models\OrderStatus;
use Illuminate\Http\Request;
use Webkul\Sales\Models\Order;

class OrderStatusController extends Controller
{
    public function index()
    {
        $orderStatuses = OrderStatus::all();

        return view('order_statuses.index')
            ->with('orderStatuses', $orderStatuses);
    }

    /**
     * Show the form for creating a new DocumentType.
     *
     * @return Response
     */
    public function create()
    {
        return view('order_statuses.create');
    }


    public function store(Request $request)
    {
        $input = $request->all();

        $orderStatus = new OrderStatus();

        $orderStatus->name = $input['name'];

        $orderStatus->template = $input['template'];

        $orderStatus->save();

        return redirect()->to('/admin/order-statuses');
    }

    /**
     * Display the specified DocumentType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified DocumentType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $orderStatus = OrderStatus::find($id);

        if(empty($orderStatus)){
            return redirect()->to('/admin/order-statuses');
        }

        return view('order_statuses.edit')->with('orderStatus', $orderStatus);
    }

    /**
     * Update the specified DocumentType in storage.
     *
     * @param  int $id
     * @param UpdateDocumentTypeRequest|Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        $orderStatus = OrderStatus::find($id);

        if(empty($orderStatus)){
            return redirect()->to('/admin/order-statuses');
        }

        $orderStatus->name = $input['name'];

        $orderStatus->template = $input['template'];

        $orderStatus->save();

        return redirect()->to('/admin/order-statuses');

    }

    /**
     * Remove the specified DocumentType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $orderStatus = OrderStatus::find($id);

        if(empty($orderStatus)){
            return redirect()->to('/admin/order-statuses');
        }

        $orderStatus->delete();

        return redirect()->to('/admin/order-statuses');

    }
}
