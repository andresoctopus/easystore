<?php

namespace App\Http\Controllers;

use App\Models\CartTransactionResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Webkul\Checkout\Facades\Cart;
use Webkul\Sales\Repositories\OrderRepository;

class PayUController extends Controller
{

    /**
     * OrderRepository object
     *
     * @var array
     */
    protected $orderRepository;


    /**
     * Create a new controller instance.
     *
     * @param Webkul\Attribute\Repositories\OrderRepository $orderRepository
     *
     * @return void
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;

    }

    public static $URL = "https://checkout.payulatam.com/ppp-web-gateway-payu/";

    public static $SANDBOX_URL = "https://sandbox.checkout.payulatam.com/ppp-web-gateway-payu";

    public static $RESPONSE_URL = "/payu/response";

    public static $CONFIRM_URL = "/payu/confirm";

    public function show()
    {

        $cart = Cart::getCart();

        $test = env('PAYU_TEST');

        if ($test == 1) {
            $url = PayUController::$SANDBOX_URL;
        } else {
            $url = PayUController::$URL;
        }
        $apiKey = env('PAYU_API_KEY');

        $merchantId = env('PAYU_MERCHANT_ID');

        $accountId = env('PAYU_ACCOUNT_ID');

        if (empty($apiKey) || empty($merchantId) || empty($accountId)) {
            return view('payu.error');
        }

        $referenceCode = "invoice" . $cart->id;

        $amount = number_format($cart->grand_total, 2, '.', '');

        $currency = $cart->cart_currency_code;

        $buyerEmail = $cart->customer_email;

        $signature = md5("$apiKey~$merchantId~$referenceCode~$amount~$currency");

        $tax = number_format($cart->base_tax_total, 2, '.', '');

        $taxReturnBase = number_format($cart->tax_total, 2, '.', '');


        $responseUrl = env('APP_URL') . PayUController::$RESPONSE_URL;

        $confirmUrl = env('APP_URL') . PayUController::$CONFIRM_URL;

        return view('payu.form')
            ->with('signature', $signature)
            ->with('test', $test)
            ->with('apiKey', $apiKey)
            ->with('merchantId', $merchantId)
            ->with('accountId', $accountId)
            ->with('referenceCode', $referenceCode)
            ->with('currency', $currency)
            ->with('amount', $amount)
            ->with('buyerEmail', $buyerEmail)
            ->with('signature', $signature)
            ->with('tax', $tax)
            ->with('taxReturnBase', $taxReturnBase)
            ->with('url', $url)
            ->with('responseUrl', $responseUrl)
            ->with('confirmUrl', $confirmUrl);

    }

    public function response(Request $request)
    {

        $input = $request->all();


        $description = $input['referenceCode'];

        $code = str_replace("invoice", "", $description);

        $cart = \Webkul\Checkout\Models\Cart::where('id', '=', $code)->get()->first();


        $test = env('PAYU_TEST');

        if ($test == 1) {
            $url = PayUController::$SANDBOX_URL;
        } else {
            $url = PayUController::$URL;
        }
        $apiKey = env('PAYU_API_KEY');


        $merchantId = $input['merchantId'];

        $referenceCode = $input['referenceCode'];


        $amount = $input['TX_VALUE'];

        $value = number_format($amount, 1, '.', '');

        $currency = $input['currency'];

        $transactionState = $input['transactionState'];

        $key = "$apiKey~$merchantId~$referenceCode~$value~$currency~$transactionState";

        $sign = md5($key);

        $signPayu = $input['signature'];

        $reference_pol = $input['reference_pol'];
        $cus = $input['cus'];
        $extra1 = $input['description'];
        $pseBank = $input['pseBank'];
        $lapPaymentMethod = $input['lapPaymentMethod'];
        $transactionId = $input['transactionId'];

        $status = '';
        $message = '';

        Log::debug("Response from payu", ['data' => $input]);

        if ($sign != $signPayu) {
            session()->flash('error', 'Error con la integridad de la informacion');

            return redirect()->route('shop.checkout.cart.index');
        }

        if ($input['transactionState'] == 4) {

            $order = $this->orderRepository->create(Cart::prepareDataForOrderUsingCart($cart));

            $status = 'Aprobada';
            $message = 'Transaccion aprobada';

        } else if ($input['transactionState'] == 6) {

            $status = 'Rechazada';
            $message = 'Transaccion rechazada';

        } else if ($input['transactionState'] == 104) {

            $status = 'Error';
            $message = 'Error en la plataforma';

        } else if ($input['transactionState'] == 7) {

            $status = 'Pendiente';
            $message = 'Transaccion pendiente';
        } else {
            $estadoTx = $input['mensaje'];

            $status = 'Pendiente';
            $message = $estadoTx;
        }

        Log::debug("this is the cart", ['cart' => $cart]);

        Cart::deActivateCartUsingCart($cart);


        $cartTransactionResult = new CartTransactionResult();
        $cartTransactionResult->cart_id = $code;
        $cartTransactionResult->currency = $currency;
        $cartTransactionResult->value = $value;
        $cartTransactionResult->status = $status;
        $cartTransactionResult->message = $message;
        $cartTransactionResult->save();

        return redirect('checkout/result/' . $code);

    }

    public function confirm(Request $request)
    {
        $input = $request->all();

        Log::debug("Confirm from payu", ['data' => $input]);

        $description = $input['description'];

        $code = str_replace("invoice", "", $description);

        $cart = \Webkul\Checkout\Models\Cart::where('id', '=', $code)->get()->first();

        if (!empty($cart)) {

            $apiKey = env('PAYU_API_KEY');

            $merchantId = $input['merchant_id'];

            $amount = $input['value'];

            $value = number_format($amount, 1, '.', '');

            $currency = $input['currency'];

            $transactionState = $input['state_pol'];

            $key = "$apiKey~$merchantId~$description~$value~$currency~$transactionState";

            $sign = md5($key);

            $signPayu = $input['signature'];

            if ($sign == $signPayu) {
                if ($input['transactionState'] == 4) {

                    $order = $this->orderRepository->create(Cart::prepareDataForOrderUsingCart($cart));

                    Cart::deActivateCartUsingCart($cart);
                }
            }

            $cartTransactionResult = new CartTransactionResult();
            $cartTransactionResult->cart_id = $code;
            $cartTransactionResult->currency = $currency;
            $cartTransactionResult->value = $value;
            $cartTransactionResult->status = $input['transactionState'];
            $cartTransactionResult->message = '';
            $cartTransactionResult->save();

        }

        return response()->json([
            'status' => 'Ok'
        ]);
    }
}
