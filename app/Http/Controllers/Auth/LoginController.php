<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Socialite;
use Webkul\Customer\Models\Customer;

class LoginController extends Controller
{


    /**
     * Redirect the user to the facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->stateless()->redirect();
    }

    /**
     * Obtain the user information from facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {

        $user = Socialite::driver('facebook')->stateless()->user();

        $customer = Customer::where('email', '=', $user->email)->get()->first();
        if (empty($customer)) {
            $customer = new Customer();
            $customer->channel_id = 1;
            $customer->first_name = $user->name;
            $customer->last_name = ' ';
            $customer->email = $user->email;
            $customer->status = 1;
            $customer->password = bcrypt($user->id);
            $customer->subscribed_to_news_letter = 0;
            $customer->is_verified = 1;
            $customer->save();
        }
        Log::debug("This is the result", ['data' => auth()->guard('customer')->attempt(['email' => $customer->email, 'password' => $user->id])]);
        if (!auth()->guard('customer')->attempt((['email' => $customer->email, 'password' => $user->id]))) {
            session()->flash('error', trans('shop::app.customer.login-form.invalid-creds'));
            return redirect('/customer/login');
        }
        Event::fire('customer.after.login', $customer->email);
        return redirect()->intended(route('customer.profile.index'));

    }
}
