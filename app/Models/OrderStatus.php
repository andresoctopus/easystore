<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    public $table = 'order_statuses';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = ['name'];
    
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'text' => 'string',
    ];

}
