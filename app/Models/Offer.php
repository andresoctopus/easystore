<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 14/08/18
 * Time: 04:06 PM
 */

namespace App\Models;


class Offer
{

    public $condition;
    public $price;
    public $priceTotal;
    public $priceAmazon;
    public $shippingSaver;
    public $prime;
    public $amountSaved;

    /**
     * @return mixed
     */
    public function getAmountSaved()
    {
        return $this->amountSaved;
    }

    /**
     * @param mixed $amountSaved
     */
    public function setAmountSaved($amountSaved)
    {
        $this->amountSaved = $amountSaved;
    }



    /**
     * @return mixed
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param mixed $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getPriceAmazon()
    {
        return $this->priceAmazon;
    }

    /**
     * @param mixed $priceAmazon
     */
    public function setPriceAmazon($priceAmazon)
    {
        $this->priceAmazon = $priceAmazon;
    }

    /**
     * @return mixed
     */
    public function getShippingSaver()
    {
        return $this->shippingSaver;
    }

    /**
     * @param mixed $shippingSaver
     */
    public function setShippingSaver($shippingSaver)
    {
        $this->shippingSaver = $shippingSaver;
    }

    /**
     * @return mixed
     */
    public function getPrime()
    {
        return $this->prime;
    }

    /**
     * @param mixed $prime
     */
    public function setPrime($prime)
    {
        $this->prime = $prime;
    }

    /**
     * @return mixed
     */
    public function getPriceTotal()
    {
        return $this->priceTotal;
    }

    /**
     * @param mixed $priceTotal
     */
    public function setPriceTotal($priceTotal)
    {
        $this->priceTotal = $priceTotal;
    }




}