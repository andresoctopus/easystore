<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartTransactionResult extends Model
{
	public $table = 'cart_transaction_results';

	const CREATED_AT = 'created_at';
	const UPDATED_AT = 'updated_at';

	public $fillable = ['cart_id','currency','value','status','message'];

	protected $casts = [
		'id' => 'integer',
		'cart_id' => 'integer',
		'currency' => 'string',
		'value' => 'string',
		'status' => 'string',
		'message' => 'string',

	];
}
