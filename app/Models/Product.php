<?php
/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 30/06/18
 * Time: 07:08 PM
 */

namespace App\Models;


class Product
{


    public $name;
    public $category;
    public $categoryId;
    public $code;
    public $weight;
    public $priceTotal;
    public $price;
    public $lowestNewPrice;
    public $LowestUsedPrice;
    public $images;
    public $condition;
    public $url;
    public $prime;
    public $offers;
    public $productGroup;
    public $upc;
    public $variations;
    public $properties;
    public $description;
    /***DEBUG PARAMS****/
    public $priceAmazon;
    public $weightAmazon;
    public $shippingRate;
    public $exchangeRate;
    public $units;

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }



    public function addImages($small, $medium, $large)
    {
        $this->images = [
            "small" => $small,
            "medium" => $medium,
            "large" => $large
        ];
    }

    /**
     * @return mixed
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param mixed $properties
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
    }


    /**
     * @param Offer $offer
     */
    public function addOffer(Offer $offer){
        if(!isset($this->offers)){
            $this->offers = [];
        }
        array_push($this->offers, $offer);
    }


    /**
     * @param Product $product
     */
    public function addVariation(Product $product){
        if(!isset($this->variations)){
            $this->variations = [];
        }
        array_push($this->variations, $product);
    }
    /**
     * @return mixed
     */
    public function getUpc()
    {
        return $this->upc;
    }

    /**
     * @param mixed $upc
     */
    public function setUpc($upc)
    {
        $this->upc = $upc;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * @param mixed $categoryId
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return mixed
     */
    public function getPriceAmazon()
    {
        return $this->priceAmazon;
    }

    /**
     * @param mixed $priceAmazon
     */
    public function setPriceAmazon($priceAmazon)
    {
        $this->priceAmazon = $priceAmazon;
    }

    /**
     * @return mixed
     */
    public function getPriceTotal()
    {
        return $this->priceTotal;
    }

    /**
     * @param mixed $priceTotal
     */
    public function setPriceTotal($priceTotal)
    {
        $this->priceTotal = $priceTotal;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getLowestNewPrice()
    {
        return $this->lowestNewPrice;
    }

    /**
     * @param mixed $lowestNewPrice
     */
    public function setLowestNewPrice($lowestNewPrice)
    {
        $this->lowestNewPrice = $lowestNewPrice;
    }

    /**
     * @return mixed
     */
    public function getLowestUsedPrice()
    {
        return $this->LowestUsedPrice;
    }

    /**
     * @param mixed $LowestUsedPrice
     */
    public function setLowestUsedPrice($LowestUsedPrice)
    {
        $this->LowestUsedPrice = $LowestUsedPrice;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return mixed
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param mixed $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getPrime()
    {
        return $this->prime;
    }

    /**
     * @param mixed $prime
     */
    public function setPrime($prime)
    {
        $this->prime = $prime;
    }

    /**
     * @return mixed
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param mixed $offers
     */
    public function setOffers($offers)
    {
        $this->offers = $offers;
    }

    /**
     * @return mixed
     */
    public function getWeightAmazon()
    {
        return $this->weightAmazon;
    }

    /**
     * @param mixed $weightAmazon
     */
    public function setWeightAmazon($weightAmazon)
    {
        $this->weightAmazon = $weightAmazon;
    }

    /**
     * @return mixed
     */
    public function getShippingRate()
    {
        return $this->shippingRate;
    }

    /**
     * @param mixed $shippingRate
     */
    public function setShippingRate($shippingRate)
    {
        $this->shippingRate = $shippingRate;
    }

    /**
     * @return mixed
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * @param mixed $exchangeRate
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;
    }

    /**
     * @return mixed
     */
    public function getUnits()
    {
        return $this->units;
    }

    /**
     * @param mixed $units
     */
    public function setUnits($units)
    {
        $this->units = $units;
    }

    /**
     * @return mixed
     */
    public function getProductGroup()
    {
        return $this->productGroup;
    }

    /**
     * @param mixed $productGroup
     */
    public function setProductGroup($productGroup)
    {
        $this->productGroup = $productGroup;
    }

    /**
     * @return mixed
     */
    public function getVariations()
    {
        return $this->variations;
    }

    /**
     * @param mixed $variations
     */
    public function setVariations($variations)
    {
        $this->variations = $variations;
    }



}