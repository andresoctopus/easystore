<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderStatusNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $order;


    private $text;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order, $text)
    {
        $this->order = $order;

        $this->text = $text;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $orderId = $this->order->id;
        return (new MailMessage)
            ->subject('Cambio de estado en tu orden')
            ->greeting('Hola ')
            ->salutation('Cordialmente, ')
            ->line('Tu pedido # ' . $orderId . ' ha cambiado de estado')
            ->line($this->text)
            ->action('Mi cuenta', url('/customer/login'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
