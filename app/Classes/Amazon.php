<?php

namespace App\Classes;

use App\Models\Offer;
use App\Models\Product;
use App\Util\PriceUtil;
use App\Util\ShippingRateUtil;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class Amazon
{

    public function __construct()
    {
        $this->response = null;
        $this->access_key = config('amazon.access_key');
        $this->secret_key = config('amazon.secret_key');
        $this->associate_tag = config('amazon.associate_tag');
        $this->locale = config('amazon.locale');
        $this->endpoint = "ecs.amazonaws." . $this->locale;
        $this->response_group = str_replace(' ', '', config('amazon.response_group'));
        $this->client = new Client;
    }

    public function itemSearch($keyword, $category = "All", $page = 1, $sort = "")
    {
        try {
            // TODO: Make function to validate correct product category
            $category = $this->validateCategory($category);
            $params = array(
                "Operation" => "ItemSearch",
                "SearchIndex" => $category,
                "Keywords" => $keyword,
                "ResponseGroup" => "Offers,OfferFull,OfferListings,Images,ItemIds,ItemAttributes,Variations,VariationMatrix,VariationSummary,ShippingCharges",
                "ItemPage" => $page,
            );

            // Sort not available on all
            // TODO: Make function to validate correct product sort param
            if ("All" !== $category)
                $params["Sort"] = $sort;

            $url = $this->makeUrl($params);


            $this->response = $this->client->get($url)->getBody()->getContents();
            return $this;
        } catch (ClientException $e) {
            Log::error("ClientException:" . $e->getMessage());
            throw new Exception($e->getMessage());
        } catch (Exception $e) {
            Log::error("Exception:\t" . $e->getMessage());
            throw new Exception($e->getMessage());
        }
    } // itemSearch


    public function itemLookup($code)
    {
        try {
            // TODO: Make function to validate correct product category
            $params = array(
                "Operation" => "ItemLookup",
                "ItemId" => $code,
                "ResponseGroup" => "Offers,OfferFull,OfferListings,Images,ItemIds,ItemAttributes,Variations,VariationMatrix,VariationSummary",
            );
            $url = $this->makeUrl($params);
            $this->response = $this->client->get($url)->getBody()->getContents();
            return $this;
        } catch (ClientException $e) {
            Log::error("ClientException:" . $e->getMessage());
            throw new Exception($e->getMessage());
        } catch (Exception $e) {
            Log::error("Exception:\t" . $e->getMessage());
            throw new Exception($e->getMessage());
        }
    } // itemSearch


    public function itemFind($upc)
    {
        try {
            // TODO: Make function to validate correct product category
            $params = array(
                "Operation" => "ItemLookup",
                "ItemId" => $upc,
                "SearchIndex" => "All",
                "IdType" => "UPC",
                "ResponseGroup" => "Offers,OfferFull,OfferListings,Images,ItemIds,ItemAttributes,Variations,VariationMatrix,VariationSummary",
            );
            $url = $this->makeUrl($params);
            $this->response = $this->client->get($url)->getBody()->getContents();
            return $this;
        } catch (ClientException $e) {
            Log::error("ClientException:" . $e->getMessage());
            throw new Exception($e->getMessage());
        } catch (Exception $e) {
            Log::error("Exception:\t" . $e->getMessage());
            throw new Exception($e->getMessage());
        }
    }

    // Format as product object
    public function products()
    {
        $xml = simplexml_load_string($this->response);
        $products = array();
        foreach ($xml->Items->Item as $item) {
            $product = $this->xmlToProduct($item);
            if ($product->getPriceTotal() > 0 && $product->getWeight() > 0) {
                array_push($products, $product);
            }
        }
        return $products;
    }


    public function product()
    {
        $xml = simplexml_load_string($this->response);
        $product = $this->xmlToProduct($xml->Items->Item);
        return $product;
    }

    public function pages()
    {
        $xml = simplexml_load_string($this->response);
        return (int)$xml->Items->TotalPages > 10 ? 10 : (int)$xml->Items->TotalPages;
    }

    public function results()
    {
        $xml = simplexml_load_string($this->response);
        return (int)$xml->Items->TotalResults;
    }


    // XML to product
    public function xmlToProduct($xml)
    {
        $product = new Product();
        $product->setUrl($this->cleanUrl((string)$xml->DetailPageURL));
        $product->setProductGroup((string)$xml->ItemAttributes->ProductGroup);
        $product->setCategory((string)$xml->ItemAttributes->ProductGroup);
        $product->setUpc((string)$xml->ItemAttributes->UPC);
        $units = 0;
        $weight = 0;
        if ($xml->ItemAttributes->PackageDimensions->Weight) {
            $data = $xml->ItemAttributes->PackageDimensions->Weight->attributes();
            $units = (string)$data['Units'];
            $weight = (double)$xml->ItemAttributes->PackageDimensions->Weight;
        } elseif (isset($xml->ItemAttributes->ItemDimensions->Weight)) {
            $data = $xml->ItemAttributes->ItemDimensions->Weight->attributes();
            $units = (string)$data['Units'];
            $weight = (double)$xml->ItemAttributes->ItemDimensions->Weight;
        }

        $product->setWeightAmazon($weight);
        $product->setUnits($units);
        $weight = $this->calculateWeight($weight, $units);
        $product->setWeight($weight);
        $product->setPrime(false);
        $product->setShippingRate(0);
        $priceNew = 0;
        $priceUsed = 0;
        if (isset($xml->OfferSummary->LowestNewPrice)) {
            $priceNew = (double)$xml->OfferSummary->LowestNewPrice->Amount / 100;
        }
        if (isset($xml->OfferSummary->LowestUsedPrice)) {
            $priceUsed = (double)$xml->OfferSummary->LowestUsedPrice->Amount / 100;
        }
        $priceNew = PriceUtil::calculatePrice($priceNew, $weight, true);
        $priceUsed = PriceUtil::calculatePrice($priceUsed, $weight, true);
        $product->setLowestNewPrice($priceNew);
        $product->setLowestUsedPrice($priceUsed);


        if (isset($xml->ItemAttributes->Feature)) {
            $description = "";
            foreach ($xml->ItemAttributes->Feature as $feature) {
                $description.=$feature."<br>";
            }
            $product->setDescription($description);
        }

        /*** get offers***/
        if (isset($xml->Offers->Offer)) {
            foreach ($xml->Offers->Offer as $o) {
                $price = 0;
                if (isset($o->OfferListing->SalePrice)) {
                    $price = $o->OfferListing->SalePrice->Amount / 100;
                } elseif (isset($o->OfferListing->Price)) {
                    $price = $o->OfferListing->Price->Amount / 100;
                }

                $offer = new Offer();
                $offer->setCondition((string)$o->OfferAttributes->Condition);
                $offer->setShippingSaver((int)$o->OfferListing->IsEligibleForSuperSaverShipping);
                if (isset($o->OfferListing->AmountSaved)) {
                    $offer->setAmountSaved((int)$o->OfferListing->AmountSaved->Amount / 100);
                }
                $offer->setPrime((int)$o->OfferListing->IsEligibleForPrime);
                $offer->setPriceAmazon($price);
                if ($offer->getPrime() === 1 || $offer->getShippingSaver() === 1) {
                    $offer->setPriceTotal(PriceUtil::calculatePrice($price, $weight, true));
                } else {
                    $offer->setPriceTotal(PriceUtil::calculatePrice($price, $weight, true) + $product->getShippingRate());
                }
                $offer->setPrice(PriceUtil::convertCurrency($offer->getPriceTotal()));
                $product->addOffer($offer);
            }
        }

        if (isset($xml->ItemAttributes->ListPrice->Amount)) {
            $priceAmazon = (double)$xml->ItemAttributes->ListPrice->Amount / 100;
            $product->setPriceAmazon($priceAmazon);
        } else {
            $product->setPriceAmazon($priceNew);
        }

        $product->setName((string)$xml->ItemAttributes->Title);
        $product->setCategoryId((string)$xml->ItemAttributes->Title);
        $product->setCode((!empty($xml->ParentASIN) && sizeof($xml->ParentASIN)) ? (string)$xml->ParentASIN : (string)$xml->ASIN);
        if ($xml->SmallImage->URL) {
            $product->addImages((string)$xml->SmallImage->URL, (string)$xml->MediumImage->URL, (string)$xml->LargeImage->URL);
        } else if ($xml->ImageSets->ImageSet) {
            $product->addImages((string)$xml->ImageSets->ImageSet->SmallImage->URL, (string)$xml->ImageSets->ImageSet->MediumImage->URL, (string)$xml->ImageSets->ImageSet->LargeImage->URL);
        }
        if($product->getPriceAmazon()>0) {
            $priceTotal = round(PriceUtil::calculatePrice($product->getPriceAmazon(), $weight, true), 2) + $product->getShippingRate();
            $product->setPriceTotal($priceTotal);
            $product->setPrice(PriceUtil::convertCurrency($priceTotal));
        }
        if (isset($xml->Variations)) {
            foreach ($xml->Variations->Item as $item) {
                $variation = $this->xmlToVariation($item, $product->url);
                if ($variation->getPriceTotal() > 0 && $variation->getWeight() > 0) {
                    $product->addVariation($variation);
                }
            }
        }
        if (($product->getPriceAmazon() <= 0 ||  $product->getWeight() <= 0) && count($product->variations)>0) {
            $variation = $product->variations[0];
            $product->setPriceAmazon($variation->priceAmazon);
            $product->setPriceTotal($variation->priceTotal);
            $product->setPrice($variation->price);
            $product->setWeight($variation->weight);
        }

        return $product;
    }


    public function xmlToVariation($xml, $url)
    {
        $product = new Product();
        $properties = [];
        foreach ($xml->VariationAttributes->VariationAttribute as $value) {
            $properties[(string)$value->Name] = (string)$value->Value;
        }
        $product->setProperties($properties);
        $product->setUrl($url);
        $product->setProductGroup((string)$xml->ItemAttributes->ProductGroup);
        $product->setCategory((string)$xml->ItemAttributes->ProductGroup);
        $product->setUpc((string)$xml->ItemAttributes->UPC);
        $units = 0;
        $weight = 0;
        if ($xml->ItemAttributes->PackageDimensions->Weight) {
            $data = $xml->ItemAttributes->PackageDimensions->Weight->attributes();
            $units = (string)$data['Units'];
            $weight = (double)$xml->ItemAttributes->PackageDimensions->Weight;
        } elseif (isset($xml->ItemAttributes->ItemDimensions->Weight)) {
            $data = $xml->ItemAttributes->ItemDimensions->Weight->attributes();
            $units = (string)$data['Units'];
            $weight = (double)$xml->ItemAttributes->ItemDimensions->Weight;
        }

        $product->setWeightAmazon($weight);
        $product->setUnits($units);
        $weight = $this->calculateWeight($weight, $units);
        $product->setWeight($weight);
        $product->setPrime(false);
        $product->setShippingRate(0);
        $priceNew = 0;
        $priceUsed = 0;
        if (isset($xml->OfferSummary->LowestNewPrice)) {
            $priceNew = (double)$xml->OfferSummary->LowestNewPrice->Amount / 100;
        }
        if (isset($xml->OfferSummary->LowestUsedPrice)) {
            $priceUsed = (double)$xml->OfferSummary->LowestUsedPrice->Amount / 100;
        }
        $priceNew = PriceUtil::calculatePrice($priceNew, $weight, true);
        $priceUsed = PriceUtil::calculatePrice($priceUsed, $weight, true);
        $product->setLowestNewPrice($priceNew);
        $product->setLowestUsedPrice($priceUsed);

        if (isset($xml->ItemAttributes->Feature)) {
            $description = "";
            foreach ($xml->ItemAttributes->Feature as $feature) {
                $description.=$feature."<br>";
            }
            $product->setDescription($description);
        }
        /*** get offers***/
        if (isset($xml->Offers->Offer)) {
            foreach ($xml->Offers->Offer as $o) {
                $price = 0;
                if (isset($o->OfferListing->SalePrice)) {
                    $price = $o->OfferListing->SalePrice->Amount / 100;
                } elseif (isset($o->OfferListing->Price)) {
                    $price = $o->OfferListing->Price->Amount / 100;
                }

                $offer = new Offer();
                $offer->setCondition((string)$o->OfferAttributes->Condition);
                $offer->setShippingSaver((int)$o->OfferListing->IsEligibleForSuperSaverShipping);
                if (isset($o->OfferListing->AmountSaved)) {
                    $offer->setAmountSaved((int)$o->OfferListing->AmountSaved->Amount / 100);
                }
                $offer->setPrime((int)$o->OfferListing->IsEligibleForPrime);
                $offer->setPriceAmazon($price);
                if ($offer->getPrime() === 1 || $offer->getShippingSaver() === 1) {
                    $offer->setPriceTotal(PriceUtil::calculatePrice($price, $weight, true));
                } else {
                    $offer->setPriceTotal(PriceUtil::calculatePrice($price, $weight, true) + $product->getShippingRate());
                }
                $offer->setPrice(PriceUtil::convertCurrency($offer->getPriceTotal()));
                $product->addOffer($offer);
            }
        }

        if (isset($xml->ItemAttributes->ListPrice->Amount)) {
            $priceAmazon = (double)$xml->ItemAttributes->ListPrice->Amount / 100;
            $product->setPriceAmazon($priceAmazon);
        } else {
            $product->setPriceAmazon($priceNew);
        }

        $product->setName((string)$xml->ItemAttributes->Title);
        $product->setCategoryId((string)$xml->ItemAttributes->Title);
        $product->setCode((!empty($xml->ParentASIN) && sizeof($xml->ParentASIN)) ? (string)$xml->ParentASIN : (string)$xml->ASIN);
        if ($xml->SmallImage->URL) {
            $product->addImages((string)$xml->SmallImage->URL, (string)$xml->MediumImage->URL, (string)$xml->LargeImage->URL);
        } else if ($xml->ImageSets->ImageSet) {
            $product->addImages((string)$xml->ImageSets->ImageSet->SmallImage->URL, (string)$xml->ImageSets->ImageSet->MediumImage->URL, (string)$xml->ImageSets->ImageSet->LargeImage->URL);
        }
        if($product->getPriceAmazon()>0){
            $priceTotal = round(PriceUtil::calculatePrice($product->getPriceAmazon(), $weight, true), 2) + $product->getShippingRate();
            $product->setPriceTotal($priceTotal);
            $product->setPrice(PriceUtil::convertCurrency($priceTotal));
        }

        if (count($product->offers)>0) {
            $offer = $product->offers[0];
            $product->setPriceAmazon($offer->priceAmazon);
            $product->setPriceTotal($offer->priceTotal);
            $product->setPrice($offer->price);
        }

        return $product;
    }

    private function cleanUrl($url)
    {
        $partUrl = substr($url, -4);
        if ($partUrl == "null") {
            $url = substr($url, 0, strlen($url) - 4);
        }
        return $url;
    }

    private function calculateWeight($weight, $units = null)
    {
        $factor = 100;
        if (isset($units)) {
            switch ($units) {
                case 'hundredths-pounds':
                    $factor = 100.00;
                    break;
                case 'pounds':
                    $factor = 1.00;
                    break;
                case 'ounces':
                    $factor = 16;
                    break;
            }
        }
        $finalWeight = (float)$weight / $factor;
        return $finalWeight;
    }


    // Format JSON
    public function json()
    {
        $xml = simplexml_load_string($this->response);
        $json = json_encode($xml);
        $json = json_decode($json, true);
        return $json;
    } // json

    // Format XML
    public function xml()
    {
        return simplexml_load_string($this->response);
    } // xml


    // Generate signed URL
    private function makeUrl($params)
    {

        // Set default paramaters
        $params["Service"] = "AWSECommerceService";
        $params["AWSAccessKeyId"] = $this->access_key;
        $params["AssociateTag"] = $this->associate_tag;
        $params["Condition"] = "All"; //;
        $params["Version"] = "2013-08-01";
        $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
        if (!$params["ResponseGroup"]) {
            $params["ResponseGroup"] = $this->response_group;
        }

        // Sort the parameters by key
        ksort($params);

        $pairs = array();
        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key) . "=" . rawurlencode($value));
        }

        // Generate the canonical query
        $canonical_query_string = join("&", $pairs);

        $uri = "/onca/xml";

        // Generate the string to be signed
        $string_to_sign = "GET\n" . $this->endpoint . "\n" . $uri . "\n" . $canonical_query_string;

        // Generate the signature required by the Product Advertising API
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $this->secret_key, true));

        // Generate the signed URL
        $request_url = 'http://' . $this->endpoint . $uri . '?' . $canonical_query_string . '&Signature=' . rawurlencode($signature);

        Log::info("Signed URL: \"" . $request_url . "\"");

        return $request_url;
    } // makeUrl

    // Validate that the category is
    private function validateCategory($category)
    {

        return "All";
    } // validateCategory


}