$(document).ready(function(){
    jQuery.noConflict();
    $('.marcas-slider').slick({
        dots: true,
        infinite: true,
        arrows:true,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
            {
            breakpoint: 1024,
            settings: {
                arrows:true,
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true,
                autoplay: true,
                autoplaySpeed: 3000,
            }
            },
            {
            breakpoint: 600,
            settings: {
                arrows:false,
                slidesToShow: 2,
                slidesToScroll: 2,
                autoplay: true,
                autoplaySpeed: 3000,
            }
            },
            {
            breakpoint: 480,
            settings: {
                dots: false,
                arrows:false,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
        autoplaySpeed: 3000,
            }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
});
