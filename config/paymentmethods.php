<?php

return [
    'payu_standard' => [
        'code' => 'payu_standard',
        'title' => 'Payu',
        'description' => 'description',
        'class' => 'App\Payu\Payment\Standard',
        'sandbox' => true,
        'active' => true,
        'business_account' => 'test@webkul.com'
    ]
];
?>
