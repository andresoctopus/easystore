<?php

return [
    [
        'key' => 'sales',
        'name' => 'Ventas',
        'sort' => 1
    ], [
        'key' => 'sales.paymentmethods',
        'name' => 'Métodos de pago',
        'sort' => 2,
    ], [
        'key' => 'sales.paymentmethods.cashondelivery',
        'name' => 'Pago contraentrega',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'title',
                'title' => 'Título',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => false,
                'locale_based' => true
            ], [
                'name' => 'description',
                'title' => 'Descripción',
                'type' => 'textarea',
                'channel_based' => false,
                'locale_based' => true
            ], [
                'name' => 'active',
                'title' => 'Estado',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Activo',
                        'value' => true
                    ], [
                        'title' => 'Inactivo',
                        'value' => false
                    ]
                ],
                'validation' => 'required'
            ]
        ]
    ], [
        'key' => 'sales.paymentmethods.moneytransfer',
        'name' => 'Transferencia bancaria',
        'sort' => 2,
        'fields' => [
            [
                'name' => 'title',
                'title' => 'Título',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => false,
                'locale_based' => true
            ], [
                'name' => 'description',
                'title' => 'Descripción',
                'type' => 'textarea',
                'channel_based' => false,
                'locale_based' => true
            ], [
                'name' => 'active',
                'title' => 'Estado',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Activo',
                        'value' => true
                    ], [
                        'title' => 'Inactivo',
                        'value' => false
                    ]
                ],
                'validation' => 'required'
            ]
        ]
    ], [
        'key' => 'sales.paymentmethods.paypal_standard',
        'name' => 'Payu',
        'sort' => 3,
        'fields' => [
            [
                'name' => 'title',
                'title' => 'Título',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => false,
                'locale_based' => true
            ], [
                'name' => 'description',
                'title' => 'Descripción ',
                'type' => 'textarea',
                'channel_based' => false,
                'locale_based' => true
            ],  [
                'name' => 'business_account',
                'title' => 'Cuenta de negocios',
                'type' => 'select',
                'type' => 'text',
                'validation' => 'required'
            ],  [
                'name' => 'active',
                'title' => 'Estado',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Activo',
                        'value' => true
                    ], [
                        'title' => 'Inactivo',
                        'value' => false
                    ]
                ],
                'validation' => 'required'
            ]
        ]
    ]
];
