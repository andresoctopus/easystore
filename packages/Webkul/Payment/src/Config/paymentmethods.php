<?php
return [
    'cashondelivery' => [
        'code' => 'cashondelivery',
        'title' => 'Pago contraentrega',
        'description' => 'Pago contraentrega',
        'class' => 'Webkul\Payment\Payment\CashOnDelivery',
        'active' => true
    ],

    'moneytransfer' => [
        'code' => 'moneytransfer',
        'title' => 'Transferencia bancaria',
        'description' => 'Transferencia bancaria',
        'class' => 'Webkul\Payment\Payment\MoneyTransfer',
        'active' => true
    ],

    'paypal_standard' => [
        'code' => 'paypal_standard',
        'title' => 'Paypal',
        'description' => 'Paypal',
        'class' => 'Webkul\Paypal\Payment\Standard',
        'sandbox' => true,
        'active' => true,
        'business_account' => 'test@webkul.com'
    ],
];
