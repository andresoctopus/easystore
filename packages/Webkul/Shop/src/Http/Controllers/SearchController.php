<?php

namespace Webkul\Shop\Http\Controllers;

use App\Services\AmazonService;
use Webkul\Product\Repositories\SearchRepository as Search;

/**
 * Search controller
 *
 * @author    Prashant Singh <prashant.singh852@webkul.com> @prashant-webkul
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class SearchController extends Controller
{
    protected $_config;

    protected $search;

    public function __construct(Search $search)
    {
        $this->_config = request('_config');

        $this->search = $search;
    }

    /**
     * Index to handle the view loaded with the search results
     */
    public function index()
    {
        $results = null;

        $results = $this->search->search(request()->all());

        if (config('app.amazon', false)) {
            $amazonService = new AmazonService();
            $response = $amazonService->search(request()->all()['term']);
            $amazonProducts = [];
            if ($response->success) {
                $amazonProducts = $response->data;
            }
        } else {
            $amazonProducts = [];
        }

        if ($results->count()) {
            return view($this->_config['view'])
                ->with('results', $results)
                ->with('resultsAmazon', $amazonProducts);

        } else {
            return view($this->_config['view'])
                ->with('results', null)
                ->with('resultsAmazon', $amazonProducts);

        }

    }
}
