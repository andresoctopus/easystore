<?php

namespace Webkul\Shop\Http\Controllers;

use App\Services\AmazonService;
use Illuminate\Support\Facades\Log;
use Webkul\Product\Repositories\ProductRepository as Product;

/**
 * Product controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ProductController extends Controller
{

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ProductRepository object
     *
     * @var array
     */
    protected $product;

    /**
     * Create a new controller instance.
     *
     * @param  Webkul\Product\Repositories\ProductRepository $product
     * @return void
     */
    public function __construct(Product $product)
    {
        $this->product = $product;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $product = $this->product->findBySlugOrFail($slug);

        $customer = auth()->guard('customer')->user();

        return view($this->_config['view'], compact('product', 'customer'));
    }


    /**
     * Display an amazon product
     *
     * @return \Illuminate\Http\Response
     */
    public function showAmazonProduct($code)
    {
        $amazonService = new AmazonService();

        $response = $amazonService->show($code);

        $product = $response->data;

        $isVariation = false;

        $variations = [];
        if (count($product->variations) > 0) {
            $variations = $product->variations;
            $product = $product->variations[0];
            $isVariation = true;
        }

        Log::debug("this is the amazon product", ['product' => $product]);

        return view("shop::products.view_amazon", compact('product', 'customer', 'variations', 'isVariation'));
    }


    public function showAmazonProductVariation($code, $id){
        $amazonService = new AmazonService();

        $response = $amazonService->show($code);

        $product = $response->data;
        $isVariation = true;

        $variations = [];
        if (count($product->variations) > 0) {
            $variations = $product->variations;

            $product = $product->variations[$id];
        }

        Log::debug("this is the amazon product", ['product' => $product]);

        return view("shop::products.view_amazon", compact('product', 'customer', 'variations','isVariation'));
    }
}
