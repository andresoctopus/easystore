@if ($products->count())
    <section class="featured-products container-fluid">

        <div class="destacados-panel text-center">
            <h1>{{ __('shop::app.home.featured-products') }}</h1>
            <hr />
        </div>

        {{--<div class="featured-grid product-grid-4">
            @foreach ($products as $productFlat)
                @include ('shop::products.list.card', ['product' => $productFlat->product])
            @endforeach
        </div>--}}

        <!-- Productos Destacados -->
        <div class="container-fuid panel-productos my-4">
            <div class="row">
              <div class="col-12 destacados-panel pb-3">
                <div class="products-slider py-3">
                  @foreach ($products as $productFlat)
                    @include ('shop::products.list.card', ['product' => $productFlat->product])
                  @endforeach
                </div>
              </div>
            </div>
        </div>
        <!-- END Productos Destacados -->

    </section>
@endif
