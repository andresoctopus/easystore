@if ($products->count())
    <section class="featured-products container-fluid">

        <div class="destacados-panel pb-3 text-center">
            <h1>{{ __('shop::app.home.new-products') }}</h1>
            <hr />
        </div>

         <!-- Productos Nuevos -->
            <div class="container-fuid panel-productos">
                <div class="row">
                  <div class="col-12 destacados-panel">
                    <div class="products-slider">
                         @foreach ($products as $productFlat)
                             @include ('shop::products.list.card', ['product' => $productFlat->product])
                         @endforeach
                    </div>
                  </div>
                </div>
            </div>
          <!-- END Productos Nuevos -->
        
    </section>
@endif
