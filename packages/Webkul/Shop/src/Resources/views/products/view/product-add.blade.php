{!! view_render_event('bagisto.shop.products.view.product-add.after', ['product' => $product]) !!}

<div class="add-to-buttons row px-0">
    <div class="col-12 col-md-6 px-0 mb-2">
        @include ('shop::products.add-to-cart', ['product' => $product])
    </div>
    <div class="col-12 col-md-6 px-0 mb-2">
        @include ('shop::products.buy-now')
    </div>
</div>

{!! view_render_event('bagisto.shop.products.view.product-add.after', ['product' => $product]) !!}
