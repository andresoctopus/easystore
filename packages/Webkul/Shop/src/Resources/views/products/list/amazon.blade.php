{!! view_render_event('bagisto.shop.products.list.card.before', ['product' => $product]) !!}

<div class="product-card card">

    @inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

    {{-- @if ($product->new)
         <div class="sticker new">
             {{ __('shop::app.products.new') }}
         </div>
     @endif--}}

    <div class="product-image">
        <a href="{{ url('products/amazon/'.$product->code) }}" title="{{ $product->name }}">
            <img src="{{ $product->images['large']  }}"/>
        </a>
    </div>

    <div class="product-information">

       <div class="product-name">
            <a href="{{ url('products/amazon/'.$product->code)  }}" title="{{ $product->name }}">
                <span>
                    {{ $product->name }}
                </span>
            </a>
        </div>
        <div class="descripcion-corta text-center my-3">
            {!! $product->description !!}
        </div>
        <div class="product-price">

            @inject ('priceHelper', 'Webkul\Product\Helpers\Price')
            <span>{{core()->currency($product->price) }}</span>
        </div>


    </div>

</div>

{!! view_render_event('bagisto.shop.products.list.card.after', ['product' => $product]) !!}
