@auth('customer')
    {!! view_render_event('bagisto.shop.products.wishlist.before') !!}

    @if(property_exists($product, 'id'))
        <a class="add-to-wishlist" href="{{ route('customer.wishlist.add', $product->id) }}" id="wishlist-changer">
            <span class="icon wishlist-icon"></span>
        </a>
    @endif


    {!! view_render_event('bagisto.shop.products.wishlist.after') !!}
@endauth
