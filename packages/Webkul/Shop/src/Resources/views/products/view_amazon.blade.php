@extends('shop::layouts.master')

@section('page_title')
    {{ trim($product->name) }}
@stop

@section('seo')
    <meta name="description" content="{{ trim($product->name) != "" ? $product->name : str_limit(strip_tags($product->name), 120, '') }}"/>
    <meta name="keywords" content="{{ $product->name }}"/>
@stop

@section('content-wrapper')

    {!! view_render_event('bagisto.shop.products.view.before', ['product' => $product]) !!}

    <section class="product-detail container-fluid my-5">

        <div class="layouter">
            <product-view>
                <div class="form-container">
                    @csrf()

                    <input type="hidden" name="product" value="{{ $product->code }}">

                   <!--Gallery-->
                    @inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
                    <?php
                    $images = $productImageHelper->getAmazonGalleryImages($product);
                    ?>

                    {!! view_render_event('bagisto.shop.products.view.gallery.before', ['product' => $product]) !!}

                    <div class="product-image-group">

                        <div class="cp-spinner cp-round" id="loader">
                        </div>

                        <product-gallery></product-gallery>


                        <!---Amazon product-->
                        <div class="add-to-buttons row px-0">
                            <div class="col-12 col-md-6 px-0 mb-2">
                                <button type="submit" class="btn btn-lg btn-primary addtocart">
                                    {{ __('shop::app.products.add-to-cart') }}
                                </button>
                            </div>
                            <div class="col-12 col-md-6 px-0 mb-2">
                                <button type="submit" data-href="{{ route('shop.product.buynowamazon', $isVariation?$product->upc: $product->code)}}" class="btn btn-lg btn-block btn-primary buynow" >
                                    {{ __('shop::app.products.buy-now') }}
                                </button>
                            </div>
                        </div>

                        <!---Add product--->


                    </div>

                    {!! view_render_event('bagisto.shop.products.view.gallery.after', ['product' => $product]) !!}

                    @push('scripts')

                        <script type="text/x-template" id="product-gallery-template">
                            <div>

                                <ul class="thumb-list">
                                    <li class="gallery-control top" @click="moveThumbs('top')" v-if="thumbs.length > 4">
                                        <span class="overlay"></span>
                                        <i class="icon arrow-up-white-icon"></i>
                                    </li>

                                    <li class="thumb-frame" v-for='(thumb, index) in thumbs' @click="changeImage(thumb)" :class="[thumb.large_image_url == currentLargeImageUrl ? 'active' : '']">
                                        <img :src="thumb.small_image_url" />
                                    </li>

                                    <li class="gallery-control bottom" @click="moveThumbs('bottom')" v-if="thumbs.length > 4">
                                        <span class="overlay"></span>
                                        <i class="icon arrow-down-white-icon"></i>
                                    </li>
                                </ul>

                                <div class="product-hero-image" id="product-hero-image">
                                    <img :src="currentLargeImageUrl" id="pro-img"/>

                                    {{-- Uncomment the line below for activating share links --}}
                                    {{-- @include('shop::products.sharelinks') --}}
                                    @auth('customer')
                                        <a class="add-to-wishlist" href="{{ route('customer.wishlist.add', $product->code) }}">
                                        </a>
                                    @endauth
                                </div>
                            </div>
                        </script>

                        <script>
                            var galleryImages = @json($images);

                            Vue.component('product-gallery', {

                                template: '#product-gallery-template',

                                data: () => ({
                                images: galleryImages,

                                thumbs: [],

                                currentLargeImageUrl: ''
                            }),

                                watch: {
                                'images': function(newVal, oldVal) {
                                    this.changeImage(this.images[0])

                                    this.prepareThumbs()
                                }
                            },

                            created () {
                                this.changeImage(this.images[0])

                                this.prepareThumbs()
                            },

                            methods: {
                                prepareThumbs () {
                                    var this_this = this;

                                    this_this.thumbs = [];

                                    this.images.forEach(function(image) {
                                        this_this.thumbs.push(image);
                                    });
                                },

                                changeImage (image) {
                                    this.currentLargeImageUrl = image.large_image_url;
                                },

                                moveThumbs(direction) {
                                    let len = this.thumbs.length;

                                    if (direction === "top") {
                                        const moveThumb = this.thumbs.splice(len - 1, 1);

                                        this.thumbs = [moveThumb[0], ...this.thumbs];
                                    } else {
                                        const moveThumb = this.thumbs.splice(0, 1);

                                        this.thumbs = [...this.thumbs, moveThumb[0]];
                                    }
                                },
                            }
                            });

                        </script>

                @endpush


                <!---- End Gallery -->

                    <div class="details">

                        <div class="product-heading">
                            <span>{{ $product->name }}</span>
                        </div>

                        <!-- REF # -->
                        <div class="ref my-1">
                        <span class="ref-tit mr-2">Ref:</span>{!!$product->code!!}
                        </div>
                        <!-- END REF # -->

                        <!--PRICE-->
                        <div class="product-price">

                            @inject ('priceHelper', 'Webkul\Product\Helpers\Price')
                            <span style="color:#c02c2b; font-size:25px;">{{core()->currency($product->price) }}</span>

                        </div>
                        <!--END PRICE-->

                        {{-- @include ('shop::products.review', ['product' => $product])

                         @include ('shop::products.view.stock', ['product' => $product])--}}

                        {!! view_render_event('bagisto.shop.products.view.short_description.before', ['product' => $product]) !!}

                        <div class="description descripcion-producto-short">
                            {!! $product->description !!}</br>
                        </div>
                        <div class="my-2"><a href="#descripcion-full">Leer la descripción completa<i class="fa fa-arrow-right ml-2" aria-hidden="true"></i></a></div>

                        {!! view_render_event('bagisto.shop.products.view.short_description.after', ['product' => $product]) !!}

                        <div class=" container-fluid col-12 info-productos-easy p-3 my-2">
                            <div class="info-envios my-2">
                                <h4><i class="fa fa-truck mr-2" aria-hidden="true"></i>Envío</h4>
                                <p class="ml-2"><span class="mr-2">Producto Internacional -</span>Tiempo de entrega estimado <strong><em>10</em></strong> días hábiles máximo.</em></strong></p>
                            </div>
                            <div class="met-pagos my-2">
                                <h4><i class="fa fa-money mr-2" aria-hidden="true"></i>Métodos de pago</h4>

                                @if ($_SERVER['SERVER_NAME'] == "co.easystore33.com")
                                   <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Colombia"/>
                                @elseif ($_SERVER['SERVER_NAME'] == "mx.easystore33.com")
                                   <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr-mx.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 México"/>
                                @elseif ($_SERVER['SERVER_NAME'] == "pe.easystore33.com")
                                   <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr-pe.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Perú"/>
                                @else ($_SERVER['SERVER_NAME'] == "easystore33.com")
                                   <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Colombia"/>
                                @endif

                            </div>
                            <div class="met-pagos my-2">
                                <h4><i class="fa fa-globe mr-2" aria-hidden="true"></i>Traduce a español</h4>
                                <p class="ml-2">Nuestros productos internacionales vienen en inglés. Puedes traducirlos dando <strong>Clic derecho</strong> y luego la opción <strong>Traducir al español.</strong></p>
                            </div>
                            <div class="met-pagos my-2">
                                <h4><i class="fa fa-envelope mr-2" aria-hidden="true"></i>¿Tienes dudas?</h4>
                                <p class="ml-2">Puedes contactárnos a través del botón <strong>"<i class="fa fa-envelope mr-2" aria-hidden="true"></i>Escríbenos a WhatsApp"</strong> o nuestro <strong>"Chat en línea."</strong></p>
                            </div>
                        </div>

                        {!! view_render_event('bagisto.shop.products.view.quantity.before', ['product' => $product]) !!}

                        <div id="descripcion-full" class="quantity control-group" :class="[errors.has('quantity') ? 'has-error' : '']">
                            <label class="mt-3">Variaciones</label>
                            @if(count($variations)>0)
                                <div class="form-group">
                                    <select class="form-control" id="variaciones" onchange="location = this.value;">
                                        @foreach($variations as $index => $variation)
                                          <option value="{{url('products/amazon/'.$product->code.'/variations/'.$index)}}">
                                              <a>
                                                  {{$variation->name}}, {{core()->currency($variation->price)}}
                                              </a>
                                          </option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif

                            <label class="required">{{ __('shop::app.products.quantity') }}</label>
                            <input name="quantity" class="control" value="1" v-validate="'required|numeric|min_value:1'" style="width: 60px;" data-vv-as="&quot;{{ __('shop::app.products.quantity') }}&quot;">
                            <span class="control-error" v-if="errors.has('quantity')">@{{ errors.first('quantity') }}</span>
                        </div>

                        {!! view_render_event('bagisto.shop.products.view.quantity.after', ['product' => $product]) !!}


                            <input type="hidden" value="false" name="is_configurable">

{{--
                        @include ('shop::products.view.configurable-options')
--}}


                        {!! view_render_event('bagisto.shop.products.view.description.before', ['product' => $product]) !!}

                        <accordian :title="'{{ __('shop::app.products.description') }}'" :active="true">
                            <div slot="header">
                                {{ __('shop::app.products.description') }}
                            </div>
                            <div slot="body">
                                <div class="full-description">
                                {!! view_render_event('bagisto.shop.products.view.short_description.before', ['product' => $product]) !!}
                                <p class="mb-3">{!! $product->description !!}</p>
                                {!! view_render_event('bagisto.shop.products.view.short_description.after', ['product' => $product]) !!}

                                    <!-- Datos # -->
                                    <div class="ref my-3 pl-2">
                                    <span class="ref-tit mr-2">Upc:</span>{!!$product->upc!!}</br>
                                    <span class="ref-tit mr-2">Condición:</span>{!!$product->condition!!}
                                    </div>
                                    <!-- END Datos # -->

                                </div>
                            </div>
                        </accordian>

                        {!! view_render_event('bagisto.shop.products.view.description.before', ['product' => $product]) !!}

                        {{--@include ('shop::products.view.attributes')

                        @include ('shop::products.view.reviews')--}}
                    </div>
                </div>
            </product-view>
        </div>

{{--
        @include ('shop::products.view.up-sells')
--}}

    </section>

    {!! view_render_event('bagisto.shop.products.view.after', ['product' => $product]) !!}
@endsection


@push('scripts')

    <script type="text/x-template" id="product-view-template">
        <form method="POST" id="product-form" action="{{ route('cart.amazonadd', $isVariation?$product->upc: $product->code) }}" @click="onSubmit($event)">

            <slot></slot>

        </form>
    </script>

    <script>

        Vue.component('product-view', {

            template: '#product-view-template',

            inject: ['$validator'],

            methods: {
                onSubmit (e) {
                    if (e.target.getAttribute('type') != 'submit')
                        return;

                    e.preventDefault();

                    this.$validator.validateAll().then(result => {
                        if (result) {
                            if (e.target.getAttribute('data-href')) {
                                window.location.href = e.target.getAttribute('data-href');
                            } else {
                                document.getElementById('product-form').submit();
                            }
                        }
                    });
                }
            }
        });

        document.onreadystatechange = function () {
            var state = document.readyState
            var galleryTemplate = document.getElementById('product-gallery-template');
            var addTOButton = document.getElementsByClassName('add-to-buttons')[0];

            if (galleryTemplate) {
                if (state != 'interactive') {
                    document.getElementById('loader').style.display="none";
                    addTOButton.style.display="flex";
                }
            }
        }

        window.onload = function() {
            var thumbList = document.getElementsByClassName('thumb-list')[0];
            var thumbFrame = document.getElementsByClassName('thumb-frame');
            var productHeroImage = document.getElementsByClassName('product-hero-image')[0];

            if (thumbList && productHeroImage) {

                for(let i=0; i < thumbFrame.length ; i++) {
                    thumbFrame[i].style.height = (productHeroImage.offsetHeight/4) + "px";
                    thumbFrame[i].style.width = (productHeroImage.offsetHeight/4)+ "px";
                }

                if (screen.width > 720) {
                    thumbList.style.width = (productHeroImage.offsetHeight/4) + "px";
                    thumbList.style.minWidth = (productHeroImage.offsetHeight/4) + "px";
                    thumbList.style.height = productHeroImage.offsetHeight + "px";
                }
            }

            window.onresize = function() {
                if (thumbList && productHeroImage) {

                    for(let i=0; i < thumbFrame.length; i++) {
                        thumbFrame[i].style.height = (productHeroImage.offsetHeight/4) + "px";
                        thumbFrame[i].style.width = (productHeroImage.offsetHeight/4)+ "px";
                    }

                    if (screen.width > 720) {
                        thumbList.style.width = (productHeroImage.offsetHeight/4) + "px";
                        thumbList.style.minWidth = (productHeroImage.offsetHeight/4) + "px";
                        thumbList.style.height = productHeroImage.offsetHeight + "px";
                    }
                }
            }
        };
    </script>
@endpush
