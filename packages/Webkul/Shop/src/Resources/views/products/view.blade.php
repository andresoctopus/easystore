@extends('shop::layouts.master')

@section('page_title')
    {{ trim($product->meta_title) != "" ? $product->meta_title : $product->name }}
@stop

@section('seo')
    <meta name="description" content="{{ trim($product->meta_description) != "" ? $product->meta_description : str_limit(strip_tags($product->description), 120, '') }}"/>
    <meta name="keywords" content="{{ $product->meta_keywords }}"/>
@stop

@section('content-wrapper')

    {!! view_render_event('bagisto.shop.products.view.before', ['product' => $product]) !!}

    <section class="product-detail container-fluid my-5">

        <div class="layouter">
            <product-view>
                <div class="form-container">
                    @csrf()

                    <input type="hidden" name="product" value="{{ $product->id }}">

                    @include ('shop::products.view.gallery')

                    <div class="details">

                        <div class="product-heading">
                            <span>{{ $product->name }}</span>
                        </div>

                        @include ('shop::products.review', ['product' => $product])

                        @include ('shop::products.price', ['product' => $product])

                        @include ('shop::products.view.stock', ['product' => $product])

                        {!! view_render_event('bagisto.shop.products.view.short_description.before', ['product' => $product]) !!}

                        <div class="description descripcion-producto-short">
                            {!! $product->short_description !!}
                        </div>
                        <div class="my-2"><a href="#descripcion-full">Leer la descripción completa<i class="fa fa-arrow-right ml-2" aria-hidden="true"></i></a></div>
                        <div>
                            <!-- Políticas -->
                                <div class=" container-fluid col-12 info-productos-easy p-3 my-2">
                                    <div class="info-envios my-2">
                                        <h4><i class="fa fa-truck mr-2" aria-hidden="true"></i>Envío</h4>
                                        <p class="ml-2">Tiempo de entrega estimado: <strong><em>{!! $product->politicas !!} días hábiles.</em></strong></p>
                                    </div>
                                    <div class="met-pagos my-2">
                                        <h4><i class="fa fa-money mr-2" aria-hidden="true"></i>Métodos de pago</h4>
                                        @if ($_SERVER['SERVER_NAME'] == "co.easystore33.com")
                                           <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Colombia"/>
                                        @elseif ($_SERVER['SERVER_NAME'] == "mx.easystore33.com")
                                           <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr-mx.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 México"/>
                                        @elseif ($_SERVER['SERVER_NAME'] == "pe.easystore33.com")
                                           <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr-pe.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Perú"/>
                                        @else ($_SERVER['SERVER_NAME'] == "easystore33.com")
                                           <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Colombia"/>
                                        @endif
                                    </div>
                                </div>
                            <!-- Políticas -->
                        </div>

                        {!! view_render_event('bagisto.shop.products.view.short_description.after', ['product' => $product]) !!}

                        {!! view_render_event('bagisto.shop.products.view.quantity.before', ['product' => $product]) !!}

                        <div id="descripcion-full" class="quantity control-group" :class="[errors.has('quantity') ? 'has-error' : '']">
                            <label class="required">{{ __('shop::app.products.quantity') }}</label>

                            <input name="quantity" class="control" value="1" v-validate="'required|numeric|min_value:1'" style="width: 60px;" data-vv-as="&quot;{{ __('shop::app.products.quantity') }}&quot;">

                            <span class="control-error" v-if="errors.has('quantity')">@{{ errors.first('quantity') }}</span>
                        </div>

                        {!! view_render_event('bagisto.shop.products.view.quantity.after', ['product' => $product]) !!}

                        @if ($product->type == 'configurable')
                            <input type="hidden" value="true" name="is_configurable">
                        @else
                            <input type="hidden" value="false" name="is_configurable">
                        @endif

                        @include ('shop::products.view.configurable-options')


                        {!! view_render_event('bagisto.shop.products.view.description.before', ['product' => $product]) !!}

                        <accordian :title="'{{ __('shop::app.products.description') }}'" :active="true">
                            <div slot="header">
                                {{ __('shop::app.products.description') }}
                                <i class="icon expand-icon right"></i>
                            </div>

                            <div slot="body">
                                <div class="full-description">
                                    {!! $product->short_description !!}</br>
                                    {!! $product->description !!}
                                </div>
                            </div>
                        </accordian>

                        {!! view_render_event('bagisto.shop.products.view.description.before', ['product' => $product]) !!}

                        @include ('shop::products.view.attributes')

                        @include ('shop::products.view.reviews')
                    </div>
                </div>
            </product-view>
        </div>

        @include ('shop::products.view.up-sells')

    </section>

    {!! view_render_event('bagisto.shop.products.view.after', ['product' => $product]) !!}
@endsection


@push('scripts')

    <script type="text/x-template" id="product-view-template">
        <form method="POST" id="product-form" action="{{ route('cart.add', $product->id) }}" @click="onSubmit($event)">

            <slot></slot>

        </form>
    </script>

    <script>

        Vue.component('product-view', {

            template: '#product-view-template',

            inject: ['$validator'],

            methods: {
                onSubmit (e) {
                    if (e.target.getAttribute('type') != 'submit')
                        return;

                    e.preventDefault();

                    this.$validator.validateAll().then(result => {
                        if (result) {
                            if (e.target.getAttribute('data-href')) {
                                window.location.href = e.target.getAttribute('data-href');
                            } else {
                                document.getElementById('product-form').submit();
                            }
                        }
                    });
                }
            }
        });

        document.onreadystatechange = function () {
            var state = document.readyState
            var galleryTemplate = document.getElementById('product-gallery-template');
            var addTOButton = document.getElementsByClassName('add-to-buttons')[0];

            if (galleryTemplate) {
                if (state != 'interactive') {
                    document.getElementById('loader').style.display="none";
                    addTOButton.style.display="flex";
                }
            }
        }

        window.onload = function() {
            var thumbList = document.getElementsByClassName('thumb-list')[0];
            var thumbFrame = document.getElementsByClassName('thumb-frame');
            var productHeroImage = document.getElementsByClassName('product-hero-image')[0];

            if (thumbList && productHeroImage) {

                for(let i=0; i < thumbFrame.length ; i++) {
                    thumbFrame[i].style.height = (productHeroImage.offsetHeight/4) + "px";
                    thumbFrame[i].style.width = (productHeroImage.offsetHeight/4)+ "px";
                }

                if (screen.width > 720) {
                    thumbList.style.width = (productHeroImage.offsetHeight/4) + "px";
                    thumbList.style.minWidth = (productHeroImage.offsetHeight/4) + "px";
                    thumbList.style.height = productHeroImage.offsetHeight + "px";
                }
            }

            window.onresize = function() {
                if (thumbList && productHeroImage) {

                    for(let i=0; i < thumbFrame.length; i++) {
                        thumbFrame[i].style.height = (productHeroImage.offsetHeight/4) + "px";
                        thumbFrame[i].style.width = (productHeroImage.offsetHeight/4)+ "px";
                    }

                    if (screen.width > 720) {
                        thumbList.style.width = (productHeroImage.offsetHeight/4) + "px";
                        thumbList.style.minWidth = (productHeroImage.offsetHeight/4) + "px";
                        thumbList.style.height = productHeroImage.offsetHeight + "px";
                    }
                }
            }
        };
    </script>
@endpush
