<div class="sidebar">
    @foreach ($menu->items as $menuItem)
        <div class="menu-block">
            <div class="menu-block-title">
                {{ trans($menuItem['name']) }}
                <i class="fa fa-lg fa-caret-down right" style="margin-right:20px;" aria-hidden="true"></i>
            </div>

            <div class="menu-block-content">
                <ul class="menubar">
                    @foreach ($menuItem['children'] as $subMenuItem)
                        <li class="menu-item {{ $menu->getActive($subMenuItem) }}">
                            <a href="{{ $subMenuItem['url'] }}">
                                {{ trans($subMenuItem['name']) }}
                            </a>

                            <i class="icon angle-right-icon"></i>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endforeach
</div>

@push('scripts')
<script>
    $(document).ready(function() {
        $(".fa.fa-lg.fa-caret-down.right").on('click', function(e){
            var currentElement = $(e.currentTarget);
            if (currentElement.hasClass('fa-caret-down')) {
                $(this).parents('.menu-block').find('.menubar').show();
                currentElement.removeClass('fa-caret-down');
                currentElement.addClass('fa-caret-up');
            } else {
                $(this).parents('.menu-block').find('.menubar').hide();
                currentElement.removeClass('fa-caret-up');
                currentElement.addClass('fa-caret-down');
            }
        });
    });
</script>
@endpush
