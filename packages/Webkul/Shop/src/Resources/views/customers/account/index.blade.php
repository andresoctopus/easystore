@extends('shop::layouts.master')

@section('content-wrapper')
    <div class="account-content container my-5">
        <h1 class="my-3 text-center">Área de clientes</h1>
        @include('shop::customers.account.partials.sidemenu')
        <div class="container text-center mt-5">
            <a href="/customer/logout" class="btn btn-primary btn-block">
            <i class="fa fa-times mr-2" aria-hidden="true"></i>
            Cerrar Sesión
            </a>
        </div>
    </div>
    
@endsection
