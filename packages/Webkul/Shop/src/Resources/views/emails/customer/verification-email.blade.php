@component('shop::emails.layouts.master')

    <div>
        <div style="text-align: center;">
            <a href="{{ config('app.url') }}">
                <img src="{{ bagisto_asset('images/logo/easystore33-logo-negro.png') }}">
            </a>
        </div>

        <div  style="font-size:16px; color:#242424; font-weight:600; margin-top: 60px; margin-bottom: 15px">
            Verificación de correo - Easy Store 33
        </div>

        <div>
            Te enviamos éste correo para asegurarnos que la dirección de correo que ingresaste es tuya.
            Haz clic en el botón 'Verificar Correo' para continuar con el proceso.
        </div>

        <div  style="margin-top: 40px; text-align: center">
            <a href="{{ route('customer.verify', $data['token']) }}" style="font-size: 16px;
            color: #FFFFFF; text-align: center; background: #c02c2b; padding: 10px 100px;text-decoration: none;">Verificar Correo</a>
        </div>
    </div>

@endcomponent
