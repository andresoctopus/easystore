@component('shop::emails.layouts.master')

    <div style="background-color:white; border-radius:5px;padding:20px; text-align:center;">
        <div style="text-align: center;">
            <a href="{{ config('app.url') }}">
                <img width="240px" height:auto; src="{{ bagisto_asset('images/logo/easystore33-logo-negro.png') }}">
            </a>
        </div>

        <div  style="font-size:16px; color:#242424; font-weight:600; margin-top: 60px; margin-bottom: 15px; text-align:center;">
            Bienvenid@ a nuestro Boletín de Noticias - Easy Store 33
        </div>

        <div style="padding:10px 10px;">
            Bienvenid@ a nuestra lista de correo, a través de tu correo recibirás noticias y promociones. No te preocupes, no te llenaremos de Spam, solo vamos a enviarte información que sabemos te gustará.
        </div>

        <div style="padding:10px 10px;">
            Si quieres darte de baja en cualquier momento puedes enviarnos un mensaje a info@easystore33.com y con gusto te eliminaremos de nuestras bases de datos.
        </div>

        <div  style="margin-top: 40px; text-align: center">
            <a href="{{ route('shop.unsubscribe', $data['token']) }}" style="font-size: 16px;
            color: #FFFFFF; text-align: center; background: #c02c2b; padding: 10px 100px;text-decoration: none;">Darme de Baja</a>
        </div>
    </div>

@endcomponent
