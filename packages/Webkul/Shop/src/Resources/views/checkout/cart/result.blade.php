@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.checkout.cart.title') }}
@stop

@section('content-wrapper')
    @inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')
    <section class="cart container">
        @if($result)
            <div class="order-summary my-3 text-center">
            <img class="py-2" src="{{ bagisto_asset('images/result-easystore33-tienda-en-linea.png') }}" />
                <h3>Este es el estado de tu transacción</h3>

                <div class="item-detail">
                    <label>
                        <b>Estado:</b>
                    </label>
                    <label>{{ $result->status }}</label>
                </div>

                <div class="item-detail">
                    <label>
                        <b>Mensaje:</b>
                    </label>
                    <label>{{ $result->message }}</label>
                </div>

                <div class="item-detail">
                    <label><b>Valor de la transacción:</b></label>
                    <label>{{ $result->value }}</label>
                </div>

                <div class="misc-controls my-3">
                    <a style="display: inline-block" href="{{ route('shop.home.index') }}" class="btn btn-lg btn-primary">
                        <i aria-hidden="true" class="fa fa-shopping-cart mr-2"></i>Volver a la tienda
                    </a>
                </div>

            </div>

        @endif
    </section>

@endsection

@push('scripts')
    <script>
        function removeLink(message) {
            if (!confirm(message))
                event.preventDefault();
        }
    </script>
@endpush
