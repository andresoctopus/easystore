@inject ('productImageHelper', 'Webkul\Product\Helpers\ProductImage')

<?php $cart = cart()->getCart(); ?>

@if ($cart)
    @php
        Cart::collectTotals();
    @endphp

    <?php $items = $cart->items; ?>

    <div class="nav-link dropdown-toggle">
        <a class="cart-link" href="{{ route('shop.checkout.cart.index') }}">
            <i class="fa fa-shopping-cart mr-2" aria-hidden="true"></i>
        </a>

        <span class="name">
            {{ __('shop::app.header.cart') }}
            <span class="count"> ({{ $cart->items->count() }})</span>
        </span>

        <i class="icon arrow-down-icon"></i>
    </div>

    <div class="dropdown-list carrito" style="display: none; top: 50px; right: 0px">
        <div class="dropdown-container">
            <div class="dropdown-cart row">
                <div class="dropdown-header col-12 col-md-12" style="width:100%;margin-bottom:20px;">
                    <p class="heading" style="margin-bottom:auto; margin-top:auto;">
                        {{ __('shop::app.checkout.cart.cart-subtotal') }} -

                        {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.before', ['cart' => $cart]) !!}

                        {{ core()->currency($cart->base_sub_total) }}

                        {!! view_render_event('bagisto.shop.checkout.cart-mini.subtotal.after', ['cart' => $cart]) !!}
                    </p>
                </div>

                <div class="dropdown-content">
                    @foreach ($items as $item)
                        <div class="item row mb-3">
                            @if($item->type == 'amazon')
                                <img src="{{ $item->url_image }}"  style="margin-right:8px;width:75px; height:75px;" />
                            @else
                                <div class="item-image col-12 col-md-4">
                                    <?php
                                    if ($item->type == "configurable")
                                        $images = $productImageHelper->getProductBaseImage($item->child->product);
                                    else
                                        $images = $productImageHelper->getProductBaseImage($item->product);
                                    ?>
                                    <img src="{{ $images['small_image_url'] }}"  style="margin-right:8px;width:75px; height:75px;" />
                                </div>
                            @endif


                            <div class="item-details col-12 col-md-8">
                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.name.before', ['item' => $item]) !!}

                                <div class="item-name"><strong>{{ $item->name }}</strong></div>

                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.name.after', ['item' => $item]) !!}


                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.options.before', ['item' => $item]) !!}

                                @if ($item->type == "configurable")
                                    <div class="item-options">
                                        {{ trim(Cart::getProductAttributeOptionDetails($item->child->product)['html']) }}
                                    </div>
                                @endif

                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.options.after', ['item' => $item]) !!}


                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.price.before', ['item' => $item]) !!}

                                <div class="item-price">{{ core()->currency($item->base_total) }}</div>

                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.price.after', ['item' => $item]) !!}


                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.before', ['item' => $item]) !!}

                                <div class="item-qty">Cantidad - {{ $item->quantity }}</div>

                                {!! view_render_event('bagisto.shop.checkout.cart-mini.item.quantity.after', ['item' => $item]) !!}
                            </div>
                        </div>

                    @endforeach
                </div>

                <div class="dropdown-footer row text-center">
                    <div class="col-12">
                    <a href="{{ route('shop.checkout.cart.index') }}">{{ __('shop::app.minicart.view-cart') }}</a>

                    <a class="btn btn-primary btn-lg pagar" style="color: white;" href="{{ route('shop.checkout.onepage.index') }}">{{ __('shop::app.minicart.checkout') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@else

    <div class="nav-item dropdown-toggle">
        <div style="display: inline-block; cursor: pointer;padding-top: 8px;">
            <i class="fa fa-shopping-cart mr-2" aria-hidden="true"></i>
            <span class="name">{{ __('shop::app.minicart.cart') }}<span class="count"> ({{ __('shop::app.minicart.zero') }}) </span></span>
        </div>
    </div>
@endif
