<form data-vv-scope="payment-form">
    <div class="form-container">
        <div class="form-header">
            <h1>{{ __('shop::app.checkout.onepage.payment-information') }}</h1>
        </div>
        <div class="payment-methods">
            <div class="control-group" :class="[errors.has('payment-form.payment[method]') ? 'has-error' : '']">
                @foreach ($paymentMethods as $payment)
                    @if($payment['method'] == 'cashondelivery')
                        @if(!cart()->isAmazon())
                            <span class="radio">
                                <input v-validate="'required'" type="radio" id="{{ $payment['method'] }}" name="payment[method]"
                                       value="{{ $payment['method'] }}" v-model="payment.method" @change="methodSelected()"
                                       data-vv-as="&quot;{{ __('shop::app.checkout.onepage.payment-method') }}&quot;">
                                <label class="radio-view" for="{{ $payment['method'] }}"></label>
                                        {{ $payment['method_title'] }}
                                </span>
                                <img width="500px" src="/themes/default/assets/images/contraentrega-metodos-de-pago-easy-store-33.png" />
                        @endif

                    @elseif($payment['method'] == 'moneytransfer')
                        <span class="radio">
                        <input v-validate="'required'" type="radio" id="{{ $payment['method'] }}" name="payment[method]"
                               value="{{ $payment['method'] }}" v-model="payment.method" @change="methodSelected()"
                               data-vv-as="&quot;{{ __('shop::app.checkout.onepage.payment-method') }}&quot;">
                        <label class="radio-view" for="{{ $payment['method'] }}"></label>
                            {{ $payment['method_title'] }}
                        </span>
                        @if ($_SERVER['SERVER_NAME'] == "co.easystore33.com")
                           <img width="500px" src="/themes/default/assets/images/transferencia-metodos-de-pago-easy-store-33.png" />
                        @elseif ($_SERVER['SERVER_NAME'] == "mx.easystore33.com")
                           <img width="500px" src="/themes/default/assets/images/transferencia-metodos-de-pago-easy-store-33-mx.png" />
                        @elseif ($_SERVER['SERVER_NAME'] == "pe.easystore33.com")
                           <img width="500px" src="/themes/default/assets/images/transferencia-metodos-de-pago-easy-store-33-pe.png" />
                        @else ($_SERVER['SERVER_NAME'] == "easystore33.com")
                           <img width="500px" src="/themes/default/assets/images/transferencia-metodos-de-pago-easy-store-33.png" />
                        @endif

                    @elseif($payment['method'] == 'payu_standard')
                        <span class="radio">
                            <input v-validate="'required'" type="radio" id="{{ $payment['method'] }}" name="payment[method]"
                                   value="{{ $payment['method'] }}" v-model="payment.method" @change="methodSelected()"
                                   data-vv-as="&quot;{{ __('shop::app.checkout.onepage.payment-method') }}&quot;">
                            <label class="radio-view" for="{{ $payment['method'] }}"></label>
                                {{ $payment['method_title'] }}
                        </span>

                        @if ($_SERVER['SERVER_NAME'] == "co.easystore33.com")
                           <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Colombia"/>
                        @elseif ($_SERVER['SERVER_NAME'] == "mx.easystore33.com")
                           <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr-mx.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 México"/>
                        @elseif ($_SERVER['SERVER_NAME'] == "pe.easystore33.com")
                           <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr-pe.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Perú"/>
                        @else ($_SERVER['SERVER_NAME'] == "easystore33.com")
                           <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Colombia"/>
                        @endif

                    @else
                        <span class="radio">
                            <input v-validate="'required'" type="radio" id="{{ $payment['method'] }}" name="payment[method]"
                                   value="{{ $payment['method'] }}" v-model="payment.method" @change="methodSelected()"
                                   data-vv-as="&quot;{{ __('shop::app.checkout.onepage.payment-method') }}&quot;">
                            <label class="radio-view" for="{{ $payment['method'] }}"></label>
                                {{ $payment['method_title'] }}
                        </span>
                        <span class="control-info">{{ $payment['description'] }}</span>

                    @endif
                @endforeach

                <span class="control-error" v-if="errors.has('payment-form.payment[method]')">
                    @{{ errors.first('payment-form.payment[method]') }}
                </span>

            </div>
        </div>
    </div>
</form>
