@extends('shop::layouts.master')

@section('page_title')
    {{ __('shop::app.search.page-title') }}
@endsection

@section('content-wrapper')
    @if ($results)
        <div class="resultados main container my-5" style="min-height: 27vh;">
            @if ($results->isEmpty())
                <div class="search-result-status">
                    <h2>{{ __('shop::app.products.whoops') }}</h2>
                    <span>{{ __('shop::app.search.no-results') }}</span>
                </div>
            @else
            <div class=" row">
                <div class="destacados-panel col-12 text-center">
                    <h1>Productos Nacionales</h1>
                    <hr />
                </div>
            </div>
                {{-- @if ($results->count() == 1)
                    <div class="search-result-status mb-20 my-5">
                        <span><b>{{ $results->count() }} </b>{{ __('shop::app.search.found-result') }}</span>
                    </div>
                @else
                    <div class="search-result-status mb-20 my-5">
                        <span><b>{{ $results->count() }} </b>{{ __('shop::app.search.found-results') }}</span>
                    </div>
                @endif --}}

                <div class="product-grid-4 my-3">
                    @foreach ($results as $productFlat)

                        @include('shop::products.list.card', ['product' => $productFlat->product])

                    @endforeach
                </div>

                @include('ui::datagrid.pagination')
            @endif
        </div>
    @endif
    @if($resultsAmazon)
        <div class="resultados row">
            <div class="destacados-panel text-center col-12">
                <h1>Productos Internacionales</h1>
                <hr />
            </div>
        </div>
        {{-- @if (count($resultsAmazon) == 1)
            <div class="search-result-status mb-20">
                <span><b>{{ count($resultsAmazon) }} </b>{{ __('shop::app.search.found-result') }}</span>
            </div>
        @else
            <div class="search-result-status mb-20">
                <span><b>{{ count($resultsAmazon) }} </b>{{ __('shop::app.search.found-results') }}</span>
            </div>
        @endif --}}
        <div class="product-grid-4 my-3 container">
            @foreach ($resultsAmazon as $productAmazon)

                @include('shop::products.list.amazon', ['product' => $productAmazon])

            @endforeach
        </div>
    @endif

@endsection
