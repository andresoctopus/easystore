<!--Navegacion-->
<div class="header sticky-top shadow d-none d-lg-block" id="header">
    <div class="container-fluid bg-dark-easy">
        <div class="row align-items-center py-2">
            <div class="col-12 col-md-3 px-0 text-center">
                <a class="navbar-brand logo-easy" href="{{ route('shop.home.index') }}">
                    @if ($logo = core()->getCurrentChannel()->logo_url)
                    <img src="{{ $logo }}" />
                    @else
                    <img src="{{ bagisto_asset('images/logo/easystore33-logo-blanco.png') }}" />
                    @endif
                </a>
            </div>
            <div class="col-12 col-md-4 px-0">
                <form class="form-inline" role="search" action="{{ route('shop.search.index') }}" method="GET" style="display: inherit;">
                    <input type="search" name="term" class="search-field form-control" placeholder="{{ __('shop::app.header.search-text') }}" required>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
            <div class="col-12 col-md-5 px-0">
                <ul class="nav nav-pills justify-content-center" id="menu-derecha">
                    <li class="nav-item">
                        {!! view_render_event('bagisto.shop.layout.header.currency-item.after') !!}
                        {!! view_render_event('bagisto.shop.layout.header.account-item.before') !!}
                        <!-- #Guest -->
                        @guest('customer')
                        <a class="btn btn-primary nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user mr-2" aria-hidden="false"></i> Mi cuenta</a>
                        <div class="dropdown-menu menu-login account guest">
                            {!! view_render_event('bagisto.shop.layout.header.currency-item.after') !!}

                            {!! view_render_event('bagisto.shop.layout.header.account-item.before') !!}
                            <ul>
                                <li>
                                    <div>
                                        <label style="color: #9e9e9e; font-weight: 700; text-transform: uppercase; font-size: 15px;">
                                            {{ __('shop::app.header.title') }}
                                        </label>
                                    </div>

                                    <div style="margin-top: 5px;">
                                        <span style="font-size: 12px;">{{ __('shop::app.header.dropdown-text') }}</span>
                                    </div>

                                    <div style="margin-top: 15px;">
                                        <a class="btn btn-primary btn-sm" href="{{ route('customer.session.index') }}" style="color: #ffffff">
                                            {{ __('shop::app.header.sign-in') }}
                                        </a>

                                        <a class="btn btn-primary btn-sm" href="{{ route('customer.register.index') }}" style="float: right; color: #ffffff">
                                            {{ __('shop::app.header.sign-up') }}
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        @endguest

                        <!-- #Auth -->
                        @auth('customer')
                        <a class="btn btn-primary nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user mr-2" aria-hidden="false"></i>Hola! <strong>{{ auth()->guard('customer')->user()->first_name }}</strong></a>
                        <div class="dropdown-menu menu-login account customer">
                            <ul class="menu-drop">
                                <li>
                                    <div>
                                        <label style="color: #9e9e9e; font-weight: 700; text-transform: uppercase; font-size: 15px;">
                                            {{ auth()->guard('customer')->user()->first_name }}
                                        </label>
                                    </div>

                                    <ul>
                                        <li>
                                            <a href="{{ route('customer.profile.index') }}">{{ __('shop::app.header.profile') }}</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('customer.wishlist.index') }}">{{ __('shop::app.header.wishlist') }}</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('shop.checkout.cart.index') }}">{{ __('shop::app.header.cart') }}</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('customer.session.destroy') }}">{{ __('shop::app.header.logout') }}</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            @endauth
                            {!! view_render_event('bagisto.shop.layout.header.account-item.after') !!}
                    <li class="nav-item dropdown">
                        <a href="#" data-toggle="modal" data-target="#pais-alt" class="nav-link"><i class="fa fa-globe mr-2" aria-hidden="true"></i>País</a>
                    </li>
                    {!! view_render_event('bagisto.shop.layout.header.cart-item.before') !!}
                    <li class="nav-item cart-dropdown-container">
                        @include('shop::checkout.cart.mini-cart')
                    </li>
                    {!! view_render_event('bagisto.shop.layout.header.cart-item.after') !!}
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid bg-easy categorias">
        <div class="container-fluid row">
            <div class="header-bottom col-7 pt-2" id="header-bottom">
                @include('shop::layouts.header.nav-menu.navmenu')
            </div>
            <div class="col-5 px-0">
                <ul class="nav nav-pills pt-1">
                    <li class="ml-auto nav-link"><a href="#" data-toggle="modal" data-target="#politicas"><i class="fa fa-info-circle mr-2" aria-hidden="true"></i>Políticas</a></li>
                    <li class="ml-auto nav-link"><a href="#" data-toggle="modal" data-target="#nosotros"><i class="fa fa-building mr-2" aria-hidden="true"></i>Nosotros</a></li>
                    @if ($_SERVER['SERVER_NAME'] == "co.easystore33.com")
                        <li class="nav-link"><a href="https://api.whatsapp.com/send?phone=573013336313&text=Hola%20Easy%20Store%2033,%20Me%20gustaría%20obtener%20información%20acerca%20de%20sus%20productos"><i class="fa fa-whatsapp mr-2" aria-hidden="true"></i>Escríbenos a WhatsApp</a></li>
                    @elseif ($_SERVER['SERVER_NAME'] == "mx.easystore33.com")
                        <li class="nav-link"><a href="https://api.whatsapp.com/send?phone=5218116757658&text=Hola%20Easy%20Store%2033,%20Me%20gustaría%20obtener%20información%20acerca%20de%20sus%20productos"><i class="fa fa-whatsapp mr-2" aria-hidden="true"></i>Escríbenos a WhatsApp</a></li>
                    @elseif ($_SERVER['SERVER_NAME'] == "pe.easystore33.com")
                        <li class="nav-link"><a href="https://api.whatsapp.com/send?phone=5218116757658&text=Hola%20Easy%20Store%2033,%20Me%20gustaría%20obtener%20información%20acerca%20de%20sus%20productos"><i class="fa fa-whatsapp mr-2" aria-hidden="true"></i>Escríbenos a WhatsApp</a></li>
                    @else ($_SERVER['SERVER_NAME'] == "easystore33.com")
                        <li class="nav-link"><a href="https://api.whatsapp.com/send?phone=573013336313&text=Hola%20Easy%20Store%2033,%20Me%20gustaría%20obtener%20información%20acerca%20de%20sus%20productos"><i class="fa fa-whatsapp mr-2" aria-hidden="true"></i>Escríbenos a WhatsApp</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="header sticky-top shadow d-md-block d-lg-none text-center" id="header">
    <!-- #Responsive Menu -->
    <nav class="navbar navbar-dark bg-dark-easy" id="responsive-menu">
        <a class="navbar-brand logo-easy" href="{{ route('shop.home.index') }}">
            @if ($logo = core()->getCurrentChannel()->logo_url)
            <img src="{{ $logo }}" />
            @else
            <img src="{{ bagisto_asset('images/logo/easystore33-logo-blanco.png') }}" />
            @endif
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Menu Easy Store 33">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="menu">
            <div class="navbar-nav">
                <div class="col-12 buscador px-0">
                    <form role="search" action="{{ route('shop.search.index') }}" method="GET" style="display: inherit;">
                        <input type="search" name="term" class="search-field form-control" placeholder="Busqueda de productos..." required>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12 mt-3 px-0">
                                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search mr-2" aria-hidden="true"></i>Buscar</button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="row">
                    <div class="col-6">
                        <a href="{{ route('shop.home.index') }}" class="nav-link link-menu-responsive"><i class="fa fa-home mr-2"></i>Inicio</a>
                    </div>
                    {!! view_render_event('bagisto.shop.layout.header.account-item.before') !!}
                    @guest('customer')
                    <div class="col-6">
                        <a href="{{ route('customer.session.index') }}" class="nav-link link-menu-responsive"><i class="fa fa-user mr-2"></i>Mi cuenta</a>
                    </div>
                    @endguest
                    @auth('customer')
                    <div class="col-6">
                        <a href="/customer/account/index" class="nav-link link-menu-responsive"><i class="fa fa-user mr-2"></i>{{ auth()->guard('customer')->user()->first_name }}</a>
                    </div>
                    @endauth
                    {!! view_render_event('bagisto.shop.layout.header.account-item.after') !!}
                    <div class="col-6">
                        <a href="{{ route('shop.checkout.cart.index') }}" class="nav-link link-menu-responsive"><i class="fa fa-shopping-cart mr-2" aria-hidden="true"></i>Carrito</a>
                    </div>
                    <div class="col-6">
                        <a href="#" data-toggle="modal" data-target="#pais-alt" class="nav-link link-menu-responsive"><i class="fa fa-globe mr-2" aria-hidden="true"></i>País</a>
                    </div>
                    <div class="col-6">
                        <a href="#" data-toggle="modal" data-target="#nosotros" class="nav-link link-menu-responsive"><i class="fa fa-building mr-2" aria-hidden="true"></i>Nosotros</a>
                    </div>
                    <div class="col-6">
                        <a href="#" data-toggle="modal" data-target="#politicas" class="nav-link link-menu-responsive"><i class="fa fa-info-circle mr-2" aria-hidden="true"></i>Políticas</a>
                    </div>
                    <div class="col-12">
                        @if ($_SERVER['SERVER_NAME'] == "co.easystore33.com")
                           <a class="nav-link link-menu-responsive" href="https://api.whatsapp.com/send?phone=573013336313&text=Hola%20Easy%20Store%2033,%20Me%20gustaría%20obtener%20información%20acerca%20de%20sus%20productos"><i class="fa fa-whatsapp mr-2" aria-hidden="true"></i>WhatsApp</a>
                        @elseif ($_SERVER['SERVER_NAME'] == "mx.easystore33.com")
                           <a class="nav-link link-menu-responsive" href="https://api.whatsapp.com/send?phone=5218116757658&text=Hola%20Easy%20Store%2033,%20Me%20gustaría%20obtener%20información%20acerca%20de%20sus%20productos"><i class="fa fa-whatsapp mr-2" aria-hidden="true"></i>WhatsApp</a>
                        @elseif ($_SERVER['SERVER_NAME'] == "pe.easystore33.com")
                           <a class="nav-link link-menu-responsive" href="https://api.whatsapp.com/send?phone=5218116757658&text=Hola%20Easy%20Store%2033,%20Me%20gustaría%20obtener%20información%20acerca%20de%20sus%20productos"><i class="fa fa-whatsapp mr-2" aria-hidden="true"></i>WhatsApp</a>
                        @else ($_SERVER['SERVER_NAME'] == "easystore33.com")
                           <a class="nav-link link-menu-responsive" href="https://api.whatsapp.com/send?phone=573013336313&text=Hola%20Easy%20Store%2033,%20Me%20gustaría%20obtener%20información%20acerca%20de%20sus%20productos"><i class="fa fa-whatsapp mr-2" aria-hidden="true"></i>WhatsApp</a>
                        @endif
                    </div>

                    <div class="col-12" style="border-top: 2px #404040 solid; margin-top: 20px; padding-top: 20px;">
                        <a href="#" data-toggle="modal" data-target="#categorias" class="nav-link link-menu-responsive"><i class="fa fa-bars mr-2" aria-hidden="true"></i>Categorías</a>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- #Responsive Menu -->
</div>

<!--END Navegación-->

<!-- Modal Nosotros -->
<div class="modal fade" id="nosotros" tabindex="-1" role="dialog" aria-labelledby="EasyStore33"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content p-3">
            <div class="modal-header">
                <div class="col-12 text-center">
                    <h3 class="modal-title pop-up-titulo">Nosotros</h3>
                </div>
            </div>
            <div class="modal-body nosotros">
                <div class="row">
                    <div class="col-12 p-3">
                        <p style="text-align:justify;">Somos una tu tienda con <strong>más de 1.000 millones</strong> de productos nuevos y originales de las mejores marcas de Estados Unidos. En easystore33.com vives una experiencia de compra única desde la comodidad de tu casa, con una sola cuenta, todo en un solo lugar. Encuentra las últimas tendencias y productos novedosos, cómpralos on-line y te los enviamos por un servicio postal (del tipo Courier) para que lo recibas en tus manos, con todos los costos incluidos. <a href="#" target="_blank">Ver políticas de envío.</a></p>
                        </br>
                        <p style="text-align:justify;">En easystore33.com, el precio que ves al finalizar tu compra, incluye todos los costos de envío e impuestos hasta tu hogar u oficina, por lo cual el producto te llega sin costos adicionales hasta tus manos. <strong>¡Sin sorpresas!</strong></p>
                        </br>
                        <div class="my-3">
                            <h5 style="color:#c02c2b;">Beneficios de comprar en easystore33</h5>
                            <ul class="ml-3">
                                <li>· Mayor variedad de productos en una misma tienda</li>
                                <li>· Mejor precio que comprar en tu país</li>
                                <li>· Productos 100% originales de las mejores marcas</li>
                                <li>· Más opciones para abonar tu orden</li>
                                <li>· Precio de tu orden con todos los costos incluidos</li>
                                <li>· Compra y seguimiento de tu orden en un mismo sitio</li>
                                <li>· Servicio al cliente e información en tu idioma</li>
                                <li>· Los productos viajan asegurados</li>
                                <li>· Seguridad de cumplir con requisitos aduaneros</li>
                            </ul>
                        </div>
                        <div class="my-3">
                            <h5 style="color:#c02c2b;">Misión</h5>
                            <p class="ml-3" style="text-align:justify;">
                                Ofrecer a nuestros clientes el mejor servicio, variedad, calidad y valor de productos.
                            </p>
                        </div>
                        <div class="my-3">
                            <h5 style="color:#c02c2b;">Visión</h5>
                            <p class="ml-3" style="text-align:justify;">
                                Ser la tienda virtual preferida por su excelente atención, además de la gran variedad y calidad de los productos.
                            </p>
                        </div>
                        <div class="my-3">
                            <h5 style="color:#c02c2b;">Valores</h5>
                            <p class="ml-3" style="text-align:justify;">
                                Seriedad, honestidad y cumplimiento, son nuestros tres pilares en el que se basa nuestro compromiso de responsabilidad como norma ética.
                            </p>
                        </div>
                        <div class="my-3">
                            <h5 style="color:#c02c2b;">Objetivo</h5>
                            <p class="ml-3" style="text-align:justify;">
                                Superar a la competencia en visión y ventas en el marco del comercio electrónico.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-block" data-dismiss="modal"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            Volver a comprar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Nosotros -->

<!-- Modal Políticas -->
<div class="modal fade" id="politicas" tabindex="-1" role="dialog" aria-labelledby="EasyStore33"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content p-3">
            <div class="modal-header">
                <div class="col-12 text-center">
                    <h3 class="modal-title pop-up-titulo">Nuestras Políticas</h3>
                </div>
            </div>
            <div class="modal-body nosotros">
                <div class="row">
                    <div class="col-12 p-3">
                        <p style="text-align:justify;color:#c02c2b; font-size:25px;"><i class="fa fa-plane mr-2" aria-hidden="true"></i><strong>ENTREGA DEL PRODUCTO</strong></p>
                        </br>
                        <p style="text-align:justify;"><span class="mr-2" style="color:#c02c2b; font-weight:700;">Nacionales:</span><span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> enviará el (los) producto(s) adquiridos a través de nuestra plataforma <span style="color:#c02c2b;">www.easystore33.com</span> por medio de Servientrega o a su defecto por otra plataforma de envíos, este llegará a su destino en un tiempo máximo de ocho (4) días hábiles siguientes al momento en que se generó la compra según la ciudad de destino. El plazo de entrega comenzará a contar a partir de la confirmación de tu pago por parte de <span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> y el análisis de los datos, lo cual puede tardar (1) un día hábil.</p>
                        </br>
                        <p style="text-align:justify;"><span class="mr-2" style="color:#c02c2b; font-weight:700;">Internacionales:</span><span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> enviará el (los) producto(s) adquiridos a través de nuestra plataforma <span style="color:#c02c2b;">www.easystore33.com</span> por medio de Servientrega o a su defecto por otra plataforma de envíos, este llegará a su destino en un tiempo máximo de ocho (10) días hábiles siguientes al momento en que se generó la compra según la ciudad de destino. El plazo de entrega comenzará a contar a partir de la confirmación de tu pago por parte de <span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> y el análisis de los datos, lo cual puede tardar (1) un día hábil.</p>
                        </br>
                        <p style="text-align:justify;">Los pedidos se envían solo en días hábiles. Los días hábiles son de lunes a viernes, excluyendo los festivos nacionales de cada país. Para las compras realizadas los días sábados, domingo y/o días festivos, el plazo de entrega comenzará a partir del siguiente día hábil.</p>
                        </br>
                        <p style="text-align:justify;color:#c02c2b; font-size:25px;"><i class="fa fa-reply mr-2" aria-hidden="true"></i><strong>DEVOLUCIONES</strong></p>
                        </br>
                        <p style="text-align:justify;"><span class="mr-2" style="color:#c02c2b; font-weight:700;">Derecho de Devolución:</span>En consideración a los términos y condiciones de entrega previstos, y en cumplimiento a los estándares normativos que le son aplicables, el proceso de devolución de productos podrá llevarse a cabo teniendo en cuenta las siguientes políticas de devolución definidas por <span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> y amparadas por la normatividad vigente: a) La solicitud de devolución deberá ser notificada a <span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> dentro de los diez (10) días siguientes al recibo del producto. Para solicitar una devolución, el cliente deberá comunicarse con la línea de atención al cliente respectivo a cada país, a través de nuestra tienda, o mediante el correo electrónico <span style="color:#c02c2b;">servicioalcliente@easystore33.com.</span>  b) Los productos deberán ser enviados, sin usar y conservando las etiquetas o/y embalaje original con los que fueron entregados. c) El cliente deberá devolver el producto por los mismos medios y en las mismas condiciones en que lo recibió. Los costos de transporte y los demás que conlleve la devolución del bien serán cubiertos por el cliente. En caso de producirse una devolución válida de acuerdo con estas políticas de devolución y, por consiguiente, se haga uso de la facultad de retracto, <span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> deberá reintegrar el precio de compra que el cliente haya pagado por el producto devuelto, dentro de los diez días siguientes al recibo del producto o la cancelación del contrato. Sin embargo, el proceso de reintegro del valor pagado podrá tardar máximo hasta 30 días calendario contados desde el momento en que el cliente ejerció el derecho de retracto, ya que, en caso de pago con tarjeta de crédito, la institución financiera (Redeban o Credibanco) pueden demorar más tiempo en aplicar la devolución a su cuenta. De encontrarse que, a pesar de los requisitos debidamente informados al cliente para la configuración del derecho de retracto, <span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> encuentra que el producto no fue devuelto en las mismas condiciones en que fue entregado, o que el mismo ha sido dañado por un acto u omisión imputables al cliente, <span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> estará facultado para abstenerse de reconocer el derecho de retracto solicitado por el cliente. d) El producto en promoción o adquirido a través de sección de ofertas no es sujeto de devolución, solo se podrá devolver dicho producto en caso de presentarse inconformidades de calidad, no aplica la devolución del producto de promoción por gusto, referencia o por talla.</p>
                        </br>
                        <p style="text-align:justify;"><span class="mr-2" style="color:#c02c2b; font-weight:700;">Exoneración de responsabilidad:</span><span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> quedará exonerado de cualquier responsabilidad frente a cualquier daño o variación que pudiere sufrir el producto derivado de un hecho o circunstancia causada por fuerza mayor o caso fortuito, que se haya presentado un uso indebido del bien por parte del cliente, desconociendo las instrucciones de etiquetado relativas al uso y cuidado del producto, o que por actuaciones de un tercero se pueda haber causado daño al producto.</p>
                        </br>
                        <p style="text-align:justify;"><span class="mr-2" style="color:#c02c2b; font-weight:700;">Política de Devoluciones:</span>Puedes devolver la compra <span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> realizada en nuestra tienda online, en un plazo de 10 días desde la fecha de recepción del paquete por motivos de cambio o 30 días por motivos de calidad. Si el motivo de la devolución es calidad puedes escoger el cambio del producto, el reintegro del dinero o un saldo para realizar tu próxima compra. Si la devolución se realiza fuera de este periodo o el artículo se ha utilizado, estropeado o no se envía en su embalaje original, <span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> no podrá aceptar la devolución y no podrá reembolsar el pago. Las devoluciones de ropa solo se aceptarán si no se ha quitado el etiquetado original. Es importante que cuando realices la devolución consideres todos los productos del mismo pedido que quieres devolver. En caso de que solicites una segunda devolución del mismo pedido <span style="color:#c02c2b;font-weight:700;">EASYSTORE33</span> se reserva el derecho de aceptar o rechazar la segunda solicitud.</p>
                        </br>
                        <p style="text-align:justify;">Los productos en promoción o comprados en la sección de ofertas solo tienen cambio por calidad, no aplica la devolución del producto de promoción por referencia, color o por talla.</p>
                        </br>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 text-center">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btn-block" data-dismiss="modal"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            Volver a comprar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Modal Políticas -->

<!-- Modal País -->
<div class="modal fade" id="pais-alt" tabindex="-1" role="dialog" aria-labelledby="EasyStore33"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content p-3">
          <div class="modal-header">
              <h3 class="modal-title pop-up-titulo">Selecciona tu país</h3>
              <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <div class="row">
                  <div class="col-12 col-md-4 p-3">
                      <div class="pais-blq">
                          <a href="https://co.easystore33.com">
                              <img class="img-fluid" src="{{ bagisto_asset('images/img/paises/colombia.png') }}" />
                              <h4>Colombia</h4>
                          </a>
                      </div>
                  </div>
                  <div class="col-12 col-md-4 p-3">
                      <div class="pais-blq">
                          <a href="https://mx.easystore33.com">
                              <img class="img-fluid" src="{{ bagisto_asset('images/img/paises/mexico.png') }}" />
                              <h4>México</h4>
                          </a>
                      </div>
                  </div>
                  <div class="col-12 col-md-4 p-3">
                      <div class="pais-blq">
                          <a href="https://pe.easystore33.com">
                              <img class="img-fluid" src="{{ bagisto_asset('images/img/paises/peru.png') }}" />
                              <h4>Perú</h4>
                          </a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
</div>
<!-- END Modal País -->

<!-- Modal País -->
<div class="modal fade" id="categorias" tabindex="-1" role="dialog" aria-labelledby="EasyStore33"
     aria-hidden="true">
    <div class="modal-dialog modal-cats modal-lg modal-dialog-centered" role="document">
        <div class="modal-content p-3">
          <div class="modal-header">
              <h5 class="modal-title pop-up-titulo">Categorías</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <div class="header-bottom col-12 menu-cats-responsive" id="header-bottom">
                @include('shop::layouts.header.nav-menu.navmenu')
            </div>
          </div>
      </div>
    </div>
</div>
<!-- END Modal País -->

@push('scripts')
<script>
    $(document).ready(function() {

        $('body').delegate('#search, .icon-menu-close, .icon.icon-menu', 'click', function(e) {
            toggleDropdown(e);
        });

        function toggleDropdown(e) {
            var currentElement = $(e.currentTarget);

            if (currentElement.hasClass('icon-search')) {
                currentElement.removeClass('icon-search');
                currentElement.addClass('icon-menu-close');
                $('#hammenu').removeClass('icon-menu-close');
                $('#hammenu').addClass('icon-menu');
                $("#search-responsive").css("display", "block");
                $("#header-bottom").css("display", "none");
            } else if (currentElement.hasClass('icon-menu')) {
                currentElement.removeClass('icon-menu');
                currentElement.addClass('icon-menu-close');
                $('#search').removeClass('icon-menu-close');
                $('#search').addClass('icon-search');
                $("#search-responsive").css("display", "none");
                $("#header-bottom").css("display", "block");
            } else {
                currentElement.removeClass('icon-menu-close');
                $("#search-responsive").css("display", "none");
                $("#header-bottom").css("display", "none");
                if (currentElement.attr("id") == 'search') {
                    currentElement.addClass('icon-search');
                } else {
                    currentElement.addClass('icon-menu');
                }
            }
        }
    });
</script>
@endpush
