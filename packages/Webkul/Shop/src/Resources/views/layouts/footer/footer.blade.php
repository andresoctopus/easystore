<div class="footer">
    <div class="footer-content">
        <div class="footer-list-container">

            <!--@if (count($categories))
                <div class="list-container">
                    <span class="list-heading">Categorías</span>

                    <ul class="list-group">
                        @foreach ($categories as $key => $category)
                            <li>
                                <a href="{{ route('shop.categories.index', $category->slug) }}">{{ $category->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif-->

            {!! DbView::make(core()->getCurrentChannel())->field('footer_content')->render() !!}

            <div class="list-container">
                <span class="list-heading">{{ __('shop::app.footer.subscribe-newsletter') }}</span>
                <div class="form-container">
                    <form action="{{ route('shop.subscribe') }}">
                        <div class="control-group" :class="[errors.has('email') ? 'has-error' : '']">
                            <input type="email" class="control subscribe-field" name="email" placeholder="Correo electrónico" required><br/>

                            <button class="btn btn-md btn-primary">{{ __('shop::app.subscription.subscribe') }}</button>
                        </div>
                    </form>
                </div>

                <!--<span class="list-heading">{{ __('shop::app.footer.locale') }}</span>
                <div class="form-container">
                    <div class="control-group">
                        <select class="control locale-switcher" onchange="window.location.href = this.value">

                            @foreach (core()->getCurrentChannel()->locales as $locale)
                                <option value="?locale={{ $locale->code }}" {{ $locale->code == app()->getLocale() ? 'selected' : '' }}>{{ $locale->name }}</option>
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="currency">
                    <span class="list-heading">{{ __('shop::app.footer.currency') }}</span>
                    <div class="form-container">
                        <div class="control-group">
                            <select class="control locale-switcher" onchange="window.location.href = this.value">

                                @foreach (core()->getCurrentChannel()->currencies as $currency)
                                    <option value="?currency={{ $currency->code }}" {{ $currency->code == core()->getCurrentCurrencyCode() ? 'selected' : '' }}>{{ $currency->code }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
        <hr class="divisor">
        <div class="my-5">
          <div class="container-fluid adicion-footer" style="padding-right: 10px; padding-left: 10px;">
            <div class="row">
              <div class="col-12 col-md-8">
                <div class="mb-2">
                  <span class="list-heading-adicional">Métodos de pago</span>
                  <div class="adicion-footer-img">
                    @if ($_SERVER['SERVER_NAME'] == "co.easystore33.com")
                       <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Colombia"/>
                    @elseif ($_SERVER['SERVER_NAME'] == "mx.easystore33.com")
                       <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr-mx.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 México"/>
                    @elseif ($_SERVER['SERVER_NAME'] == "pe.easystore33.com")
                       <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr-pe.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Perú"/>
                    @else ($_SERVER['SERVER_NAME'] == "easystore33.com")
                       <img src="{{ bagisto_asset('images/metodos-de-pago-easy-store-33-hr.png') }}" class="img-fluid img-footer-es" alt="Métodos de pago Easy Store 33 Colombia"/>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-4">
                  <div class="mb-2">
                  <span class="list-heading-adicional">Transacciones Seguras</span>
                    <div class="adicion-footer-img">
                      <img src="{{ bagisto_asset('images/img/src/easy-store-33-transacciones-seguras.png') }}" class="img-fluid img-footer-es" alt="Transacciones Seguras Easy Store 33"/>
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
    </div>
</div>
