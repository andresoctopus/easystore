<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>@yield('page_title')</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/estilos.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ bagisto_asset('css/glyphicons.css') }}">

    <link rel="stylesheet" href="{{ bagisto_asset('vendor/webkul/ui/assets/css/ui.css') }}">

    @if ($favicon = core()->getCurrentChannel()->favicon_url)
        <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
    @else
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ bagisto_asset('images/favicon/apple-touch-icon-57x57.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ bagisto_asset('images/favicon/apple-touch-icon-114x114.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ bagisto_asset('images/favicon/apple-touch-icon-72x72.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ bagisto_asset('images/favicon/apple-touch-icon-144x144.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ bagisto_asset('images/favicon/apple-touch-icon-60x60.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ bagisto_asset('images/favicon/apple-touch-icon-120x120.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ bagisto_asset('images/favicon/apple-touch-icon-76x76.png') }}" />
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ bagisto_asset('images/favicon/apple-touch-icon-152x152.png') }}" />
        <link rel="icon" type="image/png" href="{{ bagisto_asset('images/favicon/favicon-196x196.png') }}" sizes="196x196" />
        <link rel="icon" type="image/png" href="{{ bagisto_asset('images/favicon/favicon-96x96.png') }}" sizes="96x96" />
        <link rel="icon" type="image/png" href="{{ bagisto_asset('images/favicon/favicon-32x32.png') }}" sizes="32x32" />
        <link rel="icon" type="image/png" href="{{ bagisto_asset('images/favicon/favicon-16x16.png') }}" sizes="16x16" />
        <link rel="icon" type="image/png" href="{{ bagisto_asset('images/favicon/favicon-128.png') }}" sizes="128x128" />
        <meta name="application-name" content="&nbsp;"/>
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="{{ bagisto_asset('images/favicon/mstile-144x144.png') }}" />
        <meta name="msapplication-square70x70logo" content="{{ bagisto_asset('images/favicon/mstile-70x70.png') }}" />
        <meta name="msapplication-square150x150logo" content="{{ bagisto_asset('images/favicon/mstile-150x150.png') }}" />
        <meta name="msapplication-wide310x150logo" content="{{ bagisto_asset('images/favicon/mstile-310x150.png') }}" />
        <meta name="msapplication-square310x310logo" content="{{ bagisto_asset('images/favicon/mstile-310x310.png') }}" />
    @endif

    @yield('head')

    @section('seo')
        <meta name="description" content="{{ core()->getCurrentChannel()->description }}"/>
    @show

    @stack('css')

    {!! view_render_event('bagisto.shop.layout.head') !!}

</head>

<body @if (app()->getLocale() == 'ar')class="rtl"@endif>

<style>
p {margin-bottom:0px;}
</style>

    {!! view_render_event('bagisto.shop.layout.body.before') !!}

    <div id="app">
        <flash-wrapper ref='flashes'></flash-wrapper>

        <div class="main-container-wrapper container-fluid" style="padding-right:0px; padding-left:0px; margin:0px;">

            {!! view_render_event('bagisto.shop.layout.header.before') !!}

            @include('shop::layouts.header.index')

            {!! view_render_event('bagisto.shop.layout.header.after') !!}

            @yield('slider')

            <div class="content-container">

                {!! view_render_event('bagisto.shop.layout.content.before') !!}

                @yield('content-wrapper')

                {!! view_render_event('bagisto.shop.layout.content.after') !!}

            </div>

        </div>

        {!! view_render_event('bagisto.shop.layout.footer.before') !!}

        @include('shop::layouts.footer.footer')

        {!! view_render_event('bagisto.shop.layout.footer.after') !!}

        <div class="footer-bottom">
            <p>
                {{ __('shop::app.webkul.copy-right') }}
            </p>
        </div>

    </div>
    <script type="text/javascript">
        window.flashMessages = [];

        @if ($success = session('success'))
            window.flashMessages = [{'type': 'alert-success', 'message': "{{ $success }}" }];
        @elseif ($warning = session('warning'))
            window.flashMessages = [{'type': 'alert-warning', 'message': "{{ $warning }}" }];
        @elseif ($error = session('error'))
            window.flashMessages = [{'type': 'alert-error', 'message': "{{ $error }}" }
            ];
        @elseif ($info = session('info'))
            window.flashMessages = [{'type': 'alert-info', 'message': "{{ $info }}" }
            ];
        @endif

        window.serverErrors = [];
        @if(isset($errors))
            @if (count($errors))
                window.serverErrors = @json($errors->getMessages());
            @endif
        @endif
    </script>

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ bagisto_asset('js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ bagisto_asset('js/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ bagisto_asset('js/shop.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/webkul/ui/assets/js/ui.js') }}"></script>

    <script type="text/javascript" src="{{ bagisto_asset('js/scripts.js') }}"></script>
    <script src="https://use.fontawesome.com/8d73bff381.js"></script>

    <!--Chat -->
        <script src="//code.tidio.co/r5w1uyk9qjv9iafv4xf8rvzydmfkld62.js"></script>
    <!-- End Chat -->

    <!--#Totop-->
        <button class="d-none d-md-block" onclick="topFunction()" id="totop" title="Ir arriba"><i class="fa fa-chevron-up" aria-hidden="true"></i></button>
    <!--#Totop-->

    @stack('scripts')

    {!! view_render_event('bagisto.shop.layout.body.after') !!}

    <div class="modal-overlay"></div>
</body>

</html>
