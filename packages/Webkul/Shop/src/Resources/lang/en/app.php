<?php

return [
    'layouts' => [
        'my-account' => 'Mi cuenta',
        'profile' => 'Perfil',
        'address' => 'Dirección',
        'reviews' => 'Opiniones',
        'wishlist' => 'Lista de deseos',
        'orders' => 'Pedidos',
    ],
    
    'common' => [
        'error' => 'Ups! Algo salió mal, intenta de nuevo.'
    ],

    'home' => [
        'page-title' => 'Easy Store 33 - Inicio',
        'featured-products' => 'Productos destacados',
        'new-products' => 'Nuevos productos',
        'verify-email' => 'Verifica tu cuenta de correo',
        'resend-verify-email' => 'Reenviar correo de verificación'
    ],

    'header' => [
        'title' => 'Cuenta',
        'dropdown-text' => 'Gestionar carrito, pedidos y lista de deseos.',
        'sign-in' => 'Iniciar sesión',
        'sign-up' => 'Registro',
        'account' => 'Cuenta',
        'cart' => 'Carrito',
        'profile' => 'Perfil',
        'wishlist' => 'Lista de deseos',
        'cart' => 'Carrito',
        'logout' => 'Cerrar sesión',
        'search-text' => 'Escribe el producto que buscas'
    ],

    'minicart' => [
        'view-cart' => 'Ver carrito de compras',
        'checkout' => 'Pagar',
        'cart' => 'Carrito',
        'zero' => '0'
    ],

    'footer' => [
        'subscribe-newsletter' => 'Boletín de noticias',
        'subscribe' => 'Suscribirme',
        'locale' => 'Lenguaje',
        'currency' => 'Moneda',
    ],

    'subscription' => [
        'unsubscribe' => 'Darme de baja',
        'subscribe' => 'Suscribirme',
        'subscribed' => 'Ahora estás suscrito a nuestro boletín de noticias',
        'not-subscribed' => 'No pudimos suscribirte a nuestro boletín de noticias, intenta de nuevo mas tarde',
        'already' => 'Ya estás suscrito a nuestro boletín de noticias',
        'unsubscribed' => 'Estas dado de baja de nuestro boletín de noticias',
        'already-unsub' => 'Ya estás dado de baja',
        'not-subscribed' => '¡Error! El correo no se puede enviar actualmente, inténtalo de nuevo más tarde'
    ],

    'search' => [
        'no-results' => 'No se encontraron resultados',
        'page-title' => 'Easy Store 33 - Buscar',
        'found-results' => 'Resultados de la búsqueda encontrados',
        'found-result' => 'Resultado de búsqueda encontrado'
    ],

    'reviews' => [
        'title' => 'Título',
        'add-review-page-title' => 'Añadir opinión',
        'write-review' => 'Escribir una opinión',
        'review-title' => 'Dale un título a tu opinión',
        'product-review-page-title' => 'Opinión de producto',
        'rating-reviews' => 'Calificación y Opiniones',
        'submit' => 'ENVIAR',
        'delete-all' => 'Todas las opiniones han sido eliminadas correctamente',
        'ratingreviews' => ':rating Calificaciones y :review Opiniones',
        'star' => 'Estrellas',
        'percentage' => ':percentage %',
        'id-star' => 'star'
    ],

    'customer' => [
        'signup-text' => [
            'account_exists' => 'Si ya tienes cuenta',
            'title' => 'Iniciar sesión'
        ],

        'signup-form' => [
            'page-title' => 'Clientes - Registro',
            'title' => 'Registro',
            'firstname' => 'Nombre',
            'lastname' => 'Apellidos',
            'email' => 'Correo electrónico',
            'password' => 'Contraseña',
            'confirm_pass' => 'Confirmación de contraseña',
            'button_title' => 'Registrarme',
            'agree' => 'Acepto',
            'terms' => 'Términos',
            'conditions' => 'Condiciones',
            'using' => 'Al usar éste sitio',
            'agreement' => 'Aceptación',
            'success' => 'Cuenta creada con éxito, un correo electrónico ha sido enviado para veririficar tu cuenta.',
            'success-verify-email-not-sent' => 'Cuenta creada con éxito, pero el correo electrónico de verificación no se envió.',
            'failed' => '¡Error! No pudimos crear tu cuenta, inténtalo nuevamente más tarde',
            'already-verified' => 'Tu cuenta ya está verificada o intenta enviando un nuevo correo de verificación.',
            'verification-not-sent' => '¡Error! Problema al enviar un correo electrónico de verificación, inténtalo de nuevo más tarde',
            'verification-sent' => 'El correo de verificación ha sido enviado',
            'verified' => 'Tu cuenta ha sido verificada, intenta iniciar sesión ahora',
            'verify-failed' => 'No podemos verificar tu cuenta de correo',
            'dont-have-account' => 'No tienes cuenta con nosotros, intenta creando una.',
        ],

        'login-text' => [
            'no_account' => 'No tienes cuenta',
            'title' => 'Regístrate',
        ],

        'login-form' => [
            'page-title' => 'Clientes - Inicio de sesión',
            'title' => 'Iniciar sesión',
            'email' => 'Correo electrónico',
            'password' => 'Contraseña',
            'forgot_pass' => 'Olvidaste tu contraseña?',
            'button_title' => 'Iniciar sesión',
            'remember' => 'Recuérdame',
            'footer' => '© Copyright 2019 Todos los derechos reservados EasyStore33.',
            'invalid-creds' => 'Por favor revisa tus credenciales y vuelve a intentarlo',
            'verify-first' => 'Verifica primero tu cuenta de correo electrónico',
            'resend-verification' => 'Reenviar correo de verificación'
        ],

        'forgot-password' => [
            'title' => 'Recuperar contraseña',
            'email' => 'Correo electrónico',
            'submit' => 'Enviar',
            'page_title' => 'Clientes - Reestablecimiento de contraseña'
        ],

        'reset-password' => [
            'title' => 'Reestablecer contraseña',
            'email' => 'Correo registrado',
            'password' => 'Contraseña',
            'confirm-password' => 'Confirmación de contraseña',
            'back-link-title' => 'Volver al Inicio de sesión',
            'submit-btn-title' => 'Reestablecer contraseña'
        ],

        'account' => [
            'dashboard' => 'Clientes - Editar perfil',
            'menu' => 'Menú',

            'profile' => [
                'index' => [
                    'page-title' => 'Clientes - Perfil',
                    'title' => 'Perfil',
                    'edit' => 'Editar',
                ],

                'edit-success' => 'Perfil actualizado con éxito',
                'edit-fail' => '¡Error! El perfil no se puede actualizar, por favor intenta nuevamente más tarde',
                'unmatch' => 'La contraseña antigua no coincide',

                'fname' => 'Nombre',
                'lname' => 'Apellido',
                'gender' => 'Género',
                'dob' => 'Fecha de nacimiento',
                'phone' => 'Teléfono',
                'email' => 'Correo electrónico',
                'opassword' => 'Contraseña anterior',
                'password' => 'Contraseña',
                'cpassword' => 'Confirmación de contraseña',
                'submit' => 'Actualizar perfil',

                'edit-profile' => [
                    'title' => 'Editar Perfil',
                    'page-title' => 'Clientes - Editar perfil'
                ]
            ],

            'address' => [
                'index' => [
                    'page-title' => 'Clientes - Dirección',
                    'title' => 'Dirección',
                    'add' => 'Añadir dirección',
                    'edit' => 'Editar',
                    'empty' => 'No tienes ninguna dirección guardada aquí, intenta crearla haciendo clic en el siguiente enlace',
                    'create' => 'Crear dirección',
                    'delete' => 'Eliminar',
                    'make-default' => 'Hacer predeterminada',
                    'default' => 'Predeterminada',
                    'contact' => 'Contacto',
                    'confirm-delete' =>  'En serio quieres eliminar ésta dirección?',
                    'default-delete' => 'La dirección predeterminada no se puede cambiar'
                ],

                'create' => [
                    'page-title' => 'Clientes - Añadir dirección',
                    'title' => 'Añadir dirección',
                    'address1' => 'Dirección línea 1',
                    'address2' => 'Dirección línea 2',
                    'country' => 'País',
                    'state' => 'Departamento / Estado / Provincia',
                    'select-state' => 'Selecciona una región, estado o provincia',
                    'city' => 'Ciudad',
                    'postcode' => 'Código postal',
                    'phone' => 'Teléfono',
                    'submit' => 'Guardar Dirección',
                    'success' => 'Dirección añadida exitosamente!',
                    'error' => 'La dirección no se puede agregar.'
                ],

                'edit' => [
                    'page-title' => 'Clientes - Editar dirección',
                    'title' => 'Editar dirección',
                    'submit' => 'Guardar dirección',
                    'success' => 'Dirección actualizada exitosamente.'
                ],
                'delete' => [
                    'success' => 'Dirección eliminada con éxito',
                    'failure' => 'La dirección no se puede eliminar'
                ]
            ],

            'order' => [
                'index' => [
                    'page-title' => 'Clientes - Ordenes',
                    'title' => 'Ordenes',
                    'order_id' => 'Pedido #',
                    'date' => 'Fecha',
                    'status' => 'Estado',
                    'total' => 'Total'
                ],

                'view' => [
                    'page-tile' => 'Orden #:order_id',
                    'info' => 'Incormación',
                    'placed-on' => 'Realizado',
                    'products-ordered' => 'Productos ordenados',
                    'invoices' => 'Facturas',
                    'shipments' => 'Envíos',
                    'SKU' => 'Ref',
                    'product-name' => 'Nombre',
                    'qty' => 'Cantidad',
                    'item-status' => 'Estado del item',
                    'item-ordered' => 'Ordenado (:qty_ordered)',
                    'item-invoice' => 'Facturado (:qty_invoiced)',
                    'item-shipped' => 'Enviado (:qty_shipped)',
                    'item-canceled' => 'Cancelado(:qty_canceled)',
                    'price' => 'Precio',
                    'total' => 'Total',
                    'subtotal' => 'Subtotal',
                    'shipping-handling' => 'Gastos de envío y manipulación',
                    'tax' => 'Impuesto',
                    'tax-percent' => 'Impuesto %',
                    'tax-amount' => 'Importe del impuesto',
                    'discount-amount' => 'Importe de descuento',
                    'grand-total' => 'Gran total',
                    'total-paid' => 'Total Pagado',
                    'total-refunded' => 'Total Reintegrado',
                    'total-due' => 'Total Debido',
                    'shipping-address' => 'Dirección de Envío',
                    'billing-address' => 'Dirección de Facturación',
                    'shipping-method' => 'Método de envío',
                    'payment-method' => 'Método de pago',
                    'individual-invoice' => 'Factura #:invoice_id',
                    'individual-shipment' => 'Envío #:shipment_id',
                    'print' => 'Imprimir',
                    'invoice-id' => 'Factura Id',
                    'order-id' => 'Orden Id',
                    'order-date' => 'Fecha de Orden',
                    'bill-to' => 'Facturar a',
                    'ship-to' => 'Enviar a',
                    'contact' => 'Contacto'
                ]
            ],

            'review' => [
                'index' => [
                    'title' => 'Opiniones',
                    'page-title' => 'Clientes - Opiniones'
                ],

                'view' => [
                    'page-tile' => 'Opinión #:id',
                ]
            ]
        ]
    ],

    'products' => [
        'layered-nav-title' => 'Comprar por',
        'price-label' => 'Tan bajo como',
        'remove-filter-link-title' => 'Limpiar todo',
        'sort-by' => 'Organizar por',
        'from-a-z' => 'De A-Z',
        'from-z-a' => 'De Z-A',
        'newest-first' => 'Los más recientes primero',
        'oldest-first' => 'Los más viejos primero',
        'cheapest-first' => 'Más baratos primero',
        'expensive-first' => 'Más costosos primero',
        'show' => 'Show',
        'pager-info' => 'Mostrando :showing de :total items',
        'description' => 'Descripción',
        'specification' => 'Especificación',
        'total-reviews' => ':total Opiniones',
        'total-rating' => ':total_rating Calificaciones y :total_reviews Opiniones',
        'by' => 'Por :name',
        'up-sell-title' => 'Hemos encontrado otros productos que te pueden gustar!',
        'reviews-title' => 'Calificaciones y Opinones',
        'write-review-btn' => 'Escribir Opinión',
        'choose-option' => 'Seleccionar una opinión',
        'sale' => 'Oferta',
        'new' => 'Nuevo',
        'empty' => 'No hay productos disponibles en esta categoría.',
        'add-to-cart' => 'Al Carrito',
        'buy-now' => 'Comprar ahora',
        'whoops' => 'Whoops!',
        'quantity' => 'Cantidad',
        'in-stock' => 'Disponible',
        'out-of-stock' => 'No disponible',
        'view-all' => 'Ver todos'
    ],

    'wishlist' => [
        'title' => 'Lista de deseos',
        'deleteall' => 'Eliminar todo',
        'moveall' => 'Mover todos los productos al carrito',
        'move-to-cart' => 'Mover al carrito',
        'error' => 'No se puede agregar el producto a tu lista de deseos debido a problemas desconocidos, por favor revisa más tarde',
        'add' => 'Artículo añadido exitosamente a tu lista de deseos',
        'remove' => 'Artículo eliminado exitosamente de tu lista de deseos',
        'moved' => 'Artículo movido exitosamente a tu lista de deseos',
        'move-error' => 'No se puede agregar el producto a tu lista de deseos, intenta de nuevo mas tarde',
        'success' => 'Artículo añadido exitosamente a tu lista de deseos',
        'failure' => 'No se puede agregar el artículo a tu lista de deseos, intenta de nuevo mas tarde',
        'already' => 'El artículo ya se encuentra en tu lista de deseos',
        'removed' => 'Artículo eliminado exitosamente de tulista de deseos',
        'remove-fail' => 'No se puede agregar el artículo a tu lista de deseos, intenta de nuevo mas tarde',
        'empty' => 'No tienes ningún artículo o producto en tu lista de deseos',
        'remove-all-success' => 'Todos éstos artículos fueron eliminados de tu lista de deseos',
    ],

    // 'reviews' => [
    //     'empty' => 'No tenemos opiniones tuyas aún'
    // ]

    'buynow' => [
        'no-options' => 'Por favor, selecciona las opciones antes de comprar este producto'
    ],


    'checkout' => [
        'cart' => [
            'integrity' => [
                    'missing_fields' =>'Violación de integridad del sistema del carrito, faltan algunos campos obligatorios',
                'missing_options' =>'Violación de integridad del sistema del carrito, faltan opciones para el producto configurable',
            ],
            'create-error' => 'Se encontró algún problema al crear instancia de carrito',
            'title' => 'Carrito de compras',
            'empty' => 'Tu carrito de compras está vacío',
            'update-cart' => 'Actualizar carrito',
            'continue-shopping' => 'Continuar comprando',
            'proceed-to-checkout' => 'Proceder al pago',
            'remove' => 'Eliminar',
            'remove-link' => 'Eliminar',
            'move-to-wishlist' => 'Mover a tu lista de deseos',
            'move-to-wishlist-success' => 'Artículo movido a tu lista de deseos',
            'move-to-wishlist-error' => 'No se puede mover el artículo a tu lista de deseos, intenta de nuevo mas tarde',
            'add-config-warning' => 'Por favor, selecciona una opción antes de agregar al carrito',
            'quantity' => [
                'quantity' => 'Cantidad',
                'success' => 'Atículo(s) del carrito actualizados exitosamente',
                'illegal' => 'La cantidad no puede ser menor que una',
                'inventory_warning' => 'La cantidad solicitada no está disponible, vuelve a intentarlo más tarde',
                'error' => 'No se pueden actualizar los articulos en el momento, inténtalo de nuevo más tarde'
            ],
            'item' => [
                'error_remove' => 'No hay artículos para quitar del carrito',
                'success' => 'El artículo fue agregado exitosamente al carrito',
                'success-remove' => 'El artículo fue removido exitosamente del carrito',
                'error-add' => 'El artículo no se puede agregar al carrito, inténtalo de nuevo más tarde',
            ],
            'quantity-error' => 'La cantidad solicitada no está disponible',
            'cart-subtotal' => 'Subtotal del carrito',
            'cart-remove-action' => 'En serio quieres hacer ésto?'
        ],

        'onepage' => [
            'title' => 'Pago',
            'information' => 'Información',
            'shipping' => 'Envío',
            'payment' => 'Pago',
            'complete' => 'Completo',
            'billing-address' => 'Dirección de facturación',
            'sign-in' => 'Iniciar sesión',
            'first-name' => 'Nombre',
            'last-name' => 'Apellido',
            'email' => 'Correo electrónico',
            'address1' => 'Dirección línea 1',
            'address2' => 'Dirección línea 2',
            'city' => 'Ciudad',
            'state' => 'Departamento / Estado / Provincia',
            'select-state' => 'Selecciona una región, estado o provicia.',
            'postcode' => 'Zip/Código postal',
            'phone' => 'Teléfono',
            'country' => 'País',
            'order-summary' => 'Resumen del pedido',
            'shipping-address' => 'Dirección de envío',
            'use_for_shipping' => 'Enviar a esta dirección',
            'continue' => 'Continuar',
            'shipping-method' => 'Método de envío',
            'payment-information' => 'Información del pago',
            'payment-method' => 'Método de pago',
            'summary' => 'Resumen de la orden',
            'price' => 'Precio',
            'quantity' => 'Cantidad',
            'billing-address' => 'Dirección de facturación',
            'shipping-address' => 'Dirección de envío',
            'contact' => 'Contacto',
            'place-order' => 'Realizar pedido'
        ],

        'total' => [
            'order-summary' => 'Resumen de la orden',
            'sub-total' => 'Artículos',
            'grand-total' => 'Gran total',
            'delivery-charges' => 'Los gastos de envío',
            'tax' => 'Impuestos',
            'price' => 'Precio'
        ],

        'success' => [
            'title' => 'Orden colocada exitosamente',
            'thanks' => '¡Gracias por tu compra!',
            'order-id-info' => 'El ID de tu orden es #:order_id',
            'info' => 'Te enviaremos un correo electrónico con los detalles de tu pedido y la información de seguimiento.'
        ]
    ],

    'mail' => [
        'order' => [
            'subject' => 'Confirmación de nuevo pedido',
            'heading' => 'Confirmación de pedido!',
            'dear' => 'Querido(a) :customer_name',
            'greeting' => 'Gracias por tu compra :order_id realizada el :created_at',
            'summary' => 'Resumen de tu pedido',
            'shipping-address' => 'Dirección de envío',
            'billing-address' => 'Dirección de facturación',
            'contact' => 'Contacto',
            'shipping' => 'Envío',
            'payment' => 'Pago',
            'price' => 'Precio',
            'quantity' => 'Cantidad',
            'subtotal' => 'Subtotal',
            'shipping-handling' => 'Envío y manipulación',
            'tax' => 'Impuesto',
            'grand-total' => 'Gran Total',
            'final-summary' => 'Gracias por mostrar su interés en nuestra tienda, te enviaremos un número de seguimiento una vez que se haya enviado tu pedido',
            'help' => 'Si necesitas algún tipo de ayuda o tienes dudas podrás contactarnos a info@easystore33.com',
            'thanks' => 'Gracias!'
        ],
        'invoice' => [
            'heading' => 'Tu factura #:invoice_id del pedido #:order_id',
            'subject' => 'Factura de tu pedido #:order_id',
            'summary' => 'Resumen de factura',
        ],
        'shipment' => [
            'heading' => 'Tu envío #:shipment_id del pedido #:order_id',
            'subject' => 'Envío de tu pedido #:order_id',
            'summary' => 'Resumen de envío',
            'carrier' => 'Transportador',
            'tracking-number' => 'Número de seguimiento'
        ],
        'forget-password' => [
            'dear' => 'Querido(a) :name',
            'info' => 'Estás recibiendo este correo electrónico porque recibimos una solicitud de restablecimiento de contraseña para tu cuenta',
            'reset-password' => 'Reestablecer contraseña',
            'final-summary' => 'Si no solicitáste el restablecimiento de tu contraseña, no es necesario realizar ninguna otra acción.',
            'thanks' => 'Gracias!'
        ]
    ],

    'webkul' => [
        'copy-right' => '© Copyright 2019 Todos los derechos reservados EasyStore33.'
    ],

    'response' => [
        'create-success' => ':name creado exitosamente!',
        'update-success' => ':name actualizado exitosamente!',
        'delete-success' => ':name eliminado exitosamente!',
        'submit-success' => ':name enviado exitosamente!'
    ],
];
