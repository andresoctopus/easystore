<?php

return [
    [
        'key' => 'sales',
        'name' => 'Ventas',
        'sort' => 1
    ], [
        'key' => 'sales.carriers',
        'name' => 'Métodos de envío',
        'sort' => 1,
    ], [
        'key' => 'sales.carriers.free',
        'name' => 'Envío gratuito',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'title',
                'title' => 'Título',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => false,
                'locale_based' => true
            ], [
                'name' => 'description',
                'title' => 'Descripción',
                'type' => 'textarea',
                'channel_based' => false,
                'locale_based' => true
            ], [
                'name' => 'active',
                'title' => 'Status',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Activo',
                        'value' => true
                    ], [
                        'title' => 'Inactivo',
                        'value' => false
                    ]
                ],
                'validation' => 'required'
            ]
        ]
    ], [
        'key' => 'sales.carriers.flatrate',
        'name' => 'Costo de envío base',
        'sort' => 2,
        'fields' => [
            [
                'name' => 'title',
                'title' => 'Título',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => true,
                'locale_based' => true
            ], [
                'name' => 'description',
                'title' => 'Descripción',
                'type' => 'textarea',
                'channel_based' => true,
                'locale_based' => false
            ], [
                'name' => 'default_rate',
                'title' => 'Tarifa',
                'type' => 'text',
                'channel_based' => true,
                'locale_based' => false
            ], [
                'name' => 'type',
                'title' => 'Tipo',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Por unidad',
                        'value' => 'per_unit'
                    ], [
                        'title' => 'Por pedido',
                        'value' => 'per_order'
                    ]
                ],
                'validation' => 'required'
            ], [
                'name' => 'active',
                'title' => 'Estado',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Activo',
                        'value' => true
                    ], [
                        'title' => 'Inactivo',
                        'value' => false
                    ]
                ],
                'validation' => 'required'
            ]
        ]
    ], [
        'key' => 'sales.shipping',
        'name' => 'Envíos',
        'sort' => 0
    ], [
        'key' => 'sales.shipping.origin',
        'name' => 'Origen',
        'sort' => 0,
        'fields' => [
            [
                'name' => 'country',
                'title' => 'País',
                'type' => 'country',
                'validation' => 'required',
                'channel_based' => true,
                'locale_based' => true
            ], [
                'name' => 'state',
                'title' => 'Estado',
                'type' => 'state',
                'validation' => 'required',
                'channel_based' => true,
                'locale_based' => true
            ], [
                'name' => 'address1',
                'title' => 'Dirección',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => true,
                'locale_based' => false
            ], [
                'name' => 'address2',
                'title' => 'Detalles de dirección',
                'type' => 'text',
                'channel_based' => true,
                'locale_based' => false
            ], [
                'name' => 'zipcode',
                'title' => 'Código zip',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => true,
                'locale_based' => false
            ], [
                'name' => 'city',
                'title' => 'Ciudad',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => true,
                'locale_based' => false
            ]
        ]
    ]
];
