<?php

return [
    'wishlist' => [
        'success' => 'Artículo añadido exitosamente a tu lista de deseos',
        'failure' => 'El artículo no se puede agregar a tu lista de deseos',
        'already' => 'Artículo ya presente en tu lista de deseos',
        'removed' => 'Artículo eliminado correctamente de tu lista de deseos',
        'remove-fail' => 'El artículo no se puede eliminar de tu lista de deseos',
        'empty' => 'No tienes ningún artículo en tu lista de deseos',
        'select-options' => 'Necesitas seleccionar las opciones antes de agregarlo a tu lista de deseos',
        'remove-all-success' => 'Todos los artículos de tu lista de deseos han sido eliminados',
    ],
    'reviews' => [
        'empty' => 'Aún no has opinado acerca de ninguno de los productos'
    ]
];
