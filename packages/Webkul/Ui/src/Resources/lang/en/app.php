<?php
return [
    'datagrid' => [
        'actions' => 'Opciones',

        'massaction' => [
            'mass-delete-confirm' => 'Realmente quieres actualizar los :resource seleccionados?',
            'mass-update-status' => 'Realmente quieres actualizar el :resource seleccionado?',
            'delete' => 'Realmente quieres eliminar este :resource?',
            'edit' => 'Realmente quieres eliminar este :resource?',
        ],

        'no-records' => 'No se encontraron registros',
        'filter-fields-missing' => 'Parte del campo requerido es nulo, verifique la columna, la condición y el valor correctamente'
    ]
];
