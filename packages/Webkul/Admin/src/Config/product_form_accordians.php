<?php

return [
    [
        'key' => 'inventories',
        'name' => 'Inventarios',
        'view' => 'admin::catalog.products.accordians.inventories',
        'sort' => 1
    ], [
        'key' => 'images',
        'name' => 'Imágenes',
        'view' => 'admin::catalog.products.accordians.images',
        'sort' => 2
    ], [
        'key' => 'categories',
        'name' => 'Categorías',
        'view' => 'admin::catalog.products.accordians.categories',
        'sort' => 3
    ], [
        'key' => 'variations',
        'name' => 'Variaciones',
        'view' => 'admin::catalog.products.accordians.variations',
        'sort' => 4
    ]
];
