<?php

return [
    [
        'key' => 'sales',
        'name' => 'Ventas',
        'sort' => 1
    ], [
        'key' => 'sales.carriers',
        'name' => 'Métodos de envío',
        'sort' => 1,
    ], [
        'key' => 'sales.carriers.free',
        'name' => 'Envío gratuito',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'title',
                'title' => 'Título',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => false,
                'locale_based' => true
            ], [
                'name' => 'description',
                'title' => 'Descripción',
                'type' => 'textarea',
                'channel_based' => false,
                'locale_based' => true
            ], [
                'name' => 'active',
                'title' => 'Estado',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Activo',
                        'value' => true
                    ], [
                        'title' => 'Inactivo',
                        'value' => false
                    ]
                ],
                'validation' => 'required'
            ]
        ]
    ], [
        'key' => 'sales.carriers.flatrate',
        'name' => 'Costo de envío base',
        'sort' => 2,
        'fields' => [
            [
                'name' => 'title',
                'title' => 'Título',
                'type' => 'text',
                'validation' => 'required',
                'channel_based' => true,
                'locale_based' => true
            ], [
                'name' => 'description',
                'title' => 'Descripción',
                'type' => 'textarea',
                'channel_based' => true,
                'locale_based' => false
            ], [
                'name' => 'active',
                'title' => 'Estado',
                'type' => 'select',
                'options' => [
                    [
                        'title' => 'Activo',
                        'value' => true
                    ], [
                        'title' => 'Inactivo',
                        'value' => false
                    ]
                ],
                'validation' => 'required'
            ]
        ]
    ]
];
