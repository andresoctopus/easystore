<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <title>@yield('page_title')</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @if ($favicon = core()->getCurrentChannel()->favicon_url)
                <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
        @else
            <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('images/favicon/apple-touch-icon-57x57.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/favicon/apple-touch-icon-114x114.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/favicon/apple-touch-icon-72x72.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/favicon/apple-touch-icon-144x144.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ asset('images/favicon/apple-touch-icon-60x60.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ asset('images/favicon/apple-touch-icon-120x120.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ asset('images/favicon/apple-touch-icon-76x76.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('images/favicon/apple-touch-icon-152x152.png') }}" />
            <link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-196x196.png') }}" sizes="196x196" />
            <link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-96x96.png') }}" sizes="96x96" />
            <link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-32x32.png') }}" sizes="32x32" />
            <link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-16x16.png') }}" sizes="16x16" />
            <link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-128.png') }}" sizes="128x128" />
            <meta name="application-name" content="&nbsp;"/>
            <meta name="msapplication-TileColor" content="#FFFFFF" />
            <meta name="msapplication-TileImage" content="{{ asset('images/favicon/mstile-144x144.png') }}" />
            <meta name="msapplication-square70x70logo" content="{{ asset('images/favicon/mstile-70x70.png') }}" />
            <meta name="msapplication-square150x150logo" content="{{ asset('images/favicon/mstile-150x150.png') }}" />
            <meta name="msapplication-wide310x150logo" content="{{ asset('images/favicon/mstile-310x150.png') }}" />
            <meta name="msapplication-square310x310logo" content="{{ asset('images/favicon/mstile-310x310.png') }}" />
        @endif

        <link rel="stylesheet" href="{{ asset('vendor/webkul/admin/assets/css/admin.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/webkul/ui/assets/css/ui.css') }}">

        @yield('head')

        @yield('css')

        {!! view_render_event('bagisto.admin.layout.head') !!}

    </head>

    <body style="scroll-behavior: smooth;">
        {!! view_render_event('bagisto.admin.layout.body.before') !!}

        <div id="app">

            <flash-wrapper ref='flashes'></flash-wrapper>

            {!! view_render_event('bagisto.admin.layout.nav-top.before') !!}

            @include ('admin::layouts.nav-top')

            {!! view_render_event('bagisto.admin.layout.nav-top.after') !!}


            {!! view_render_event('bagisto.admin.layout.nav-left.before') !!}

            @include ('admin::layouts.nav-left')

            {!! view_render_event('bagisto.admin.layout.nav-left.after') !!}


            <div class="content-container">

                {!! view_render_event('bagisto.admin.layout.content.before') !!}

                @yield('content-wrapper')

                {!! view_render_event('bagisto.admin.layout.content.after') !!}

            </div>

        </div>

        <script type="text/javascript">
            window.flashMessages = [];

            @if ($success = session('success'))
                window.flashMessages = [{'type': 'alert-success', 'message': "{{ $success }}" }];
            @elseif ($warning = session('warning'))
                window.flashMessages = [{'type': 'alert-warning', 'message': "{{ $warning }}" }];
            @elseif ($error = session('error'))
                window.flashMessages = [{'type': 'alert-error', 'message': "{{ $error }}" }];
            @elseif ($info = session('info'))
                window.flashMessages = [{'type': 'alert-error', 'message': "{{ $info }}" }];
            @endif

            window.serverErrors = [];
            @if (isset($errors))
                @if (count($errors))
                    window.serverErrors = @json($errors->getMessages());
                @endif
            @endif
        </script>

        <script type="text/javascript" src="{{ asset('vendor/webkul/admin/assets/js/admin.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendor/webkul/ui/assets/js/ui.js') }}"></script>

        @stack('scripts')

        {!! view_render_event('bagisto.admin.layout.body.after') !!}

        <div class="modal-overlay"></div>
    </body>
</html>
