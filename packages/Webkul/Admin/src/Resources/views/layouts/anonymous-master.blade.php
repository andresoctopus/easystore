<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <title>@yield('page_title')</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @if ($favicon = core()->getCurrentChannel()->favicon_url)
            <link rel="icon" sizes="16x16" href="{{ $favicon }}" />
        @else
            <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ asset('images/favicon/apple-touch-icon-57x57.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('images/favicon/apple-touch-icon-114x114.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('images/favicon/apple-touch-icon-72x72.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('images/favicon/apple-touch-icon-144x144.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="60x60" href="{{ asset('images/favicon/apple-touch-icon-60x60.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="120x120" href="{{ asset('images/favicon/apple-touch-icon-120x120.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="76x76" href="{{ asset('images/favicon/apple-touch-icon-76x76.png') }}" />
            <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('images/favicon/apple-touch-icon-152x152.png') }}" />
            <link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-196x196.png') }}" sizes="196x196" />
            <link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-96x96.png') }}" sizes="96x96" />
            <link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-32x32.png') }}" sizes="32x32" />
            <link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-16x16.png') }}" sizes="16x16" />
            <link rel="icon" type="image/png" href="{{ asset('images/favicon/favicon-128.png') }}" sizes="128x128" />
            <meta name="application-name" content="&nbsp;"/>
            <meta name="msapplication-TileColor" content="#FFFFFF" />
            <meta name="msapplication-TileImage" content="{{ asset('images/favicon/mstile-144x144.png') }}" />
            <meta name="msapplication-square70x70logo" content="{{ asset('images/favicon/mstile-70x70.png') }}" />
            <meta name="msapplication-square150x150logo" content="{{ asset('images/favicon/mstile-150x150.png') }}" />
            <meta name="msapplication-wide310x150logo" content="{{ asset('images/favicon/mstile-310x150.png') }}" />
            <meta name="msapplication-square310x310logo" content="{{ asset('images/favicon/mstile-310x310.png') }}" />
        @endif

        <link rel="stylesheet" href="{{ asset('vendor/webkul/admin/assets/css/admin.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/webkul/admin/assets/css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('vendor/webkul/ui/assets/css/ui.css') }}">

        <style>
            .container {
                text-align: center;
                position: absolute;
                width: 100%;
                height: 100%;
                display: table;
                z-index: 1;
                background: #F8F9FA;
            }
            .center-box {
                display: table-cell;
                vertical-align: middle;
            }
            .adjacent-center {
                width: 365px;
                display: inline-block;
                text-align: left;
            }

            .form-container .control-group .control {
                width: 100%;
            }

            h1 {
                font-size: 24px;
                font-weight: 600;
                margin-bottom: 30px;
            }

            .brand-logo {
                margin-bottom: 30px;
                text-align: center;
            }

            .brand-logo img {max-width:250px; max-height:60px;}

            .footer {
                margin-top: 40px;
                padding: 0 20px;
            }

            .footer p {
                font-size: 14px;
                color: #8E8E8E;
                text-align: center;
            }

            .btn.btn-primary {
                width: 100%;
            }
        </style>

        @yield('css')
    </head>
    <body>
        <div id="app" class="container">

            <flash-wrapper ref='flashes'></flash-wrapper>

            <div class="center-box">
            
                <div class="adjacent-center">

                    <div class="brand-logo">
                        <img src="{{ bagisto_asset('images/logo/easystore33-logo-negro.png') }}" alt="Easy Store 33"/>
                    </div>

                    @yield('content')

                    <div class="footer">
                        <p>
                            © Copyright 2019 Todos los derechos reservados EasyStore33.
                        </p>
                    </div>

                </div>

            </div>

        </div>

        <script type="text/javascript">
            window.flashMessages = [];
            @if ($success = session('success'))
                window.flashMessages = [{'type': 'alert-success', 'message': "{{ $success }}" }];
            @elseif ($warning = session('warning'))
                window.flashMessages = [{'type': 'alert-warning', 'message': "{{ $warning }}" }];
            @elseif ($error = session('error'))
                window.flashMessages = [{'type': 'alert-error', 'message': "{{ $error }}" }];
            @endif

            window.serverErrors = [];
            @if (count($errors))
                window.serverErrors = @json($errors->getMessages());
            @endif
        </script>

        <script type="text/javascript" src="{{ asset('vendor/webkul/admin/assets/js/admin.js') }}"></script>
        <script type="text/javascript" src="{{ asset('vendor/webkul/ui/assets/js/ui.js') }}"></script>

        @yield('javascript')
        
    </body>
</html>
