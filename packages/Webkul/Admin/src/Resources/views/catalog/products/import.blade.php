@extends('admin::layouts.content')

@section('page_title')
    {{ __('admin::app.catalog.products.title') }}
@stop

@section('content')
    <div class="content" style="height: 100%;">
        <div class="page-header">
            <div class="page-title">
                <h1>Importación masiva de productos</h1>
            </div>


        </div>

        <div class="page-content">
            <div class="page-action">
                <form class="form-control" method="POST" action="{{route('admin.catalog.import.store')}}" enctype="multipart/form-data">
                    @csrf()
                    <input type="file" name="fileProducts" required accept=".xls,.xlsx">
                    <br/>
                    <br/>
                    <br/>
                    <button  type="submit" class="btn btn-lg btn-primary" >Importar</button>
                </form>

            </div>
        </div>
    </div>
@stop