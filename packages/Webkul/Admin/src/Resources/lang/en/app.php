<?php

return [
    'common' => [
        'no-result-found' => 'No pudimos encontrar registros.',
        'country' => 'País',
        'state' => 'Estado',
        'true' => 'Verdadero',
        'false' => 'Falso'
    ],

    'layouts' => [
        'my-account' => 'Mi cuenta',
        'logout' => 'Cerrar sesión',
        'visit-shop' => 'Visitar tienda',
        'dashboard' => 'Panel de control',
        'sales' => 'Ventas',
        'orders' => 'Pedidos',
        'shipments' => 'Envíos',
        'invoices' => 'Facturas',
        'catalog' => 'Catálogo',
        'products' => 'Productos',
        'categories' => 'Categorías',
        'attributes' => 'Atributos',
        'attribute-families' => 'Familias de atributos',
        'attribute-import' => 'Importar productos',
        'customers' => 'Clientes',
        'groups' => 'Grupos',
        'reviews' => 'Opiniones',
        'newsletter-subscriptions' => 'Suscripciones',
        'configure' => 'Configurar',
        'settings' => 'Ajustes',
        'locales' => 'Locales',
        'currencies' => 'Monedas',
        'exchange-rates' => 'Tipos de cambio',
        'inventory-sources' => 'Fuentes de inventario',
        'channels' => 'Canales',
        'users' => 'Usuarios',
        'roles' => 'Roles',
        'sliders' => 'Banners',
        'taxes' => 'Impuestos',
        'statuses' => 'Estados personalizados',
        'tax-categories' => 'Categorías de impuestos',
        'tax-rates' => 'Tarifas de impuestos'
    ],

    'acl' => [
        'dashboard' => 'Panel de control',
        'sales' => 'Ventas',
        'orders' => 'Pedidos',
        'shipments' => 'Envíos',
        'invoices' => 'Facturas',
        'catalog' => 'Catálogo',
        'products' => 'Productos',
        'categories' => 'Categorías',
        'attributes' => 'Atributos',
        'attribute-families' => 'Familias de atributos',
        'customers' => 'Clientes',
        'groups' => 'Grupos',
        'reviews' => 'Opiniones',
        'newsletter-subscriptions' => 'Suscripciones',
        'configure' => 'Configurar',
        'settings' => 'Ajustes',
        'locales' => 'Locales',
        'currencies' => 'Monedas',
        'exchange-rates' => 'Tipos de cambio',
        'inventory-sources' => 'Fuentes de inventario',
        'channels' => 'Canales',
        'users' => 'Usuarios',
        'roles' => 'Roles',
        'sliders' => 'Banners',
        'taxes' => 'Impuestos',
        'tax-categories' => 'Categorías de impuestos',
        'tax-rates' => 'Tarifas de impuestos',
    ],

    'dashboard' => [
        'title' => 'Panel de control',
        'from' => 'De',
        'to' => 'Para',
        'total-customers' => 'Clientes totales',
        'total-orders' => 'Pedidos totales',
        'total-sale' => 'Venta total',
        'average-sale' => 'Orden de venta promedio',
        'increased' => ':progress% Aumentado',
        'decreased' => ':progress% Reducido',
        'sales' => 'Ventas',
        'top-performing-categories' => 'Categorías de mejor desempeño',
        'product-count' => ':count Productos',
        'top-selling-products' => 'Los productos más vendidos',
        'sale-count' => ':count ventas',
        'customer-with-most-sales' => 'Cliente con más ventas',
        'order-count' => ':count pedidos',
        'revenue' => 'Ingresos :total',
        'stock-threshold' => 'Umbral de stock',
        'qty-left' => ':qty restante',
    ],

    'datagrid' => [
        'mass-ops' => [
            'method-error' => 'Error! Se detectó un método incorrecto, verifica la configuración de acción masiva',
            'delete-success' => 'El índice seleccionado de :resource fue completamente eliminado',
            'partial-action' => 'Algunas acciones no se realizaron debido a restricciones del sistema en :resource',
            'update-success' => 'El índice seleccionado de :resource fue completamente actualizado',
        ],

        'id' => 'ID',
        'status' => 'Estado',
        'code' => 'Código',
        'admin-name' => 'Nombre',
        'name' => 'Nombre',
        'fullname' => 'Nombre completo',
        'type' => 'Tipo',
        'required' => 'Requerido',
        'unique' => 'Único',
        'per-locale' => 'Basado en el lenguaje',
        'per-channel' => 'Basado en el canal',
        'position' => 'Posición',
        'locale' => 'Lenguaje',
        'hostname' => 'Nombre de host',
        'email' => 'Correo',
        'group' => 'Grupo',
        'title' => 'Título',
        'comment' => 'Comentario',
        'product-name' => 'Producto',
        'currency-name' => 'Nombre de moneda',
        'exch-rate' => 'Tipo de cambio',
        'priority' => 'Prioridad',
        'subscribed' => 'Subscríto',
        'base-total' => 'Base Total',
        'grand-total' => 'Gran Total',
        'order-date' => 'Fecha de pedido',
        'channel-name' => 'Canal',
        'billed-to' => 'Facturado a',
        'shipped-to' => 'Enviado a',
        'order-id' => 'ID Pedido',
        'invoice-date' => 'Fecha de factura',
        'total-qty' => 'Cantidad total',
        'inventory-source' => 'Fuente de inventario',
        'shipment-date' => 'Fecha de envío',
        'shipment-to' => 'Enviado a',
        'sku' => 'Ref',
        'price' => 'Precio',
        'qty' => 'Cantidad',
        'permission-type' => 'Tipo de permiso',
        'identifier' => 'Identificador',
        'state' => 'Estado',
        'country' => 'País',
        'tax-rate' => 'Tarifa de impuesto',
        'role' => 'Rol',
        'sub-total' => 'Sub Total',
        'no-of-products' => 'Número de productos',
    ],

    'account' => [
        'title' => 'Mi cuenta',
        'save-btn-title' => 'Guardar',
        'general' => 'General',
        'name' => 'Nombre',
        'email' => 'Correo',
        'password' => 'Contraseña',
        'confirm-password' => 'Confirmar contraseña',
        'change-password' => 'Cambiar contraseña de cuenta',
        'current-password' => 'Contraseña actual'
    ],

    'users' => [
        'forget-password' => [
            'title' => 'Contraseña olvidada',
            'header-title' => 'Recuperar contraseña',
            'email' => 'Correo registrado',
            'password' => 'Contraseña',
            'confirm-password' => 'Confirmar contraseña',
            'back-link-title' => 'Volver al inicio de sesión',
            'submit-btn-title' => 'Correo electrónico para restablecer contraseña'
        ],

        'reset-password' => [
            'title' => 'Recuperar contraseña',
            'title' => 'Recuperar contraseña',
            'email' => 'Correo registrado',
            'password' => 'Contraseña',
            'confirm-password' => 'Confirmar contraseña',
            'back-link-title' => 'Volver al Inicio de sesión',
            'submit-btn-title' => 'Reestablecer contraseña'
        ],

        'roles' => [
            'title' => 'Roles',
            'add-role-title' => 'Añadir rol',
            'edit-role-title' => 'Editar rol',
            'save-btn-title' => 'Guardar rol',
            'general' => 'General',
            'name' => 'Nombre',
            'description' => 'Descripción',
            'access-control' => 'Control de acceso',
            'permissions' => 'Permisos',
            'custom' => 'Personalizado',
            'all' => 'Todo'
        ],

        'users' => [
            'title' => 'Usuario',
            'add-user-title' => 'Agregar usuario',
            'edit-user-title' => 'Editar usuario',
            'save-btn-title' => 'Guardar usuario',
            'general' => 'General',
            'email' => 'Correo',
            'name' => 'Nombre',
            'password' => 'Contraseña',
            'confirm-password' => 'Confirmar Contraseña',
            'status-and-role' => 'Estado y rol',
            'role' => 'Rol',
            'status' => 'Estado',
            'account-is-active' => 'Cuenta activa',
            'current-password' => 'Ingresa la contraseña actual',
            'confirm-delete' => 'Confirma eliminar cuenta',
            'confirm-delete-title' => 'Confirma tu contraseña antes de eliminar',
            'delete-last' => 'Al menos un administrador es requerido.',
            'delete-success' => 'Éxito! Usuario eliminado',
            'incorrect-password' => 'La contraseña que ingresaste es incorrecta',
        ],

        'sessions' => [
            'title' => 'Iniciar sesión',
            'email' => 'Correo',
            'password' => 'Contraseña',
            'forget-password-link-title' => 'Contraseña olvidada?',
            'remember-me' => 'Recuerdame',
            'submit-btn-title' => 'Iniciar sesión'
        ]
    ],

    'sales' => [
        'orders' => [
            'title' => 'Ventas',
            'view-title' => 'Pedido #:order_id',
            'cancel-btn-title' => 'Cancelar',
            'shipment-btn-title' => 'Enviar',
            'invoice-btn-title' => 'Factura',
            'info' => 'Información',
            'invoices' => 'Facturas',
            'shipments' => 'Envíos',
            'order-and-account' => 'Pedido y monto',
            'order-info' => 'Información de pedido',
            'order-date' => 'Fecha de pedido',
            'order-status' => 'Estado del pedido',
            'channel' => 'Canal',
            'customer-name' => 'Nombre del cliente',
            'email' => 'Correo',
            'contact-number' => 'Número de contacto',
            'account-info' => 'Información de la cuenta',
            'address' => 'Dirección',
            'shipping-address' => 'Dirección de envío',
            'billing-address' => 'Dirección de facturación',
            'payment-and-shipping' => 'Pago y envío',
            'payment-info' => 'Información del pago',
            'payment-method' => 'Método de pago',
            'currency' => 'Moneda',
            'shipping-info' => 'Información de envío',
            'shipping-method' => 'Método de envío',
            'shipping-price' => 'Precio de envío',
            'products-ordered' => 'Productos ordenados',
            'SKU' => 'Ref',
            'product-name' => 'Nombre del producto',
            'qty' => 'Cantidad',
            'item-status' => 'Estado del artículo',
            'item-ordered' => 'Ordenado (:qty_ordered)',
            'item-invoice' => 'Facturado (:qty_invoiced)',
            'item-shipped' => 'Enviado (:qty_shipped)',
            'item-canceled' => 'Cancelado (:qty_canceled)',
            'price' => 'Precio',
            'total' => 'Total',
            'subtotal' => 'Subtotal',
            'shipping-handling' => 'Envío y manipulación',
            'tax' => 'Impuesto',
            'tax-percent' => 'Porcentaje de impuesto',
            'tax-amount' => 'Monto de impuesto',
            'discount-amount' => 'Monto de descuento',
            'grand-total' => 'Gran Total',
            'total-paid' => 'Total pagado',
            'total-refunded' => 'Total reintegrado',
            'total-due' => 'Total debido',
            'cancel-confirm-msg' => 'Estás seguro de cancelar el pedido?'
        ],

        'invoices' => [
            'title' => 'Facturas',
            'id' => 'Id',
            'invoice-id' => 'ID de factura',
            'date' => 'Fecha de factura',
            'order-id' => 'ID de pedido',
            'customer-name' => 'Nombre de cliente',
            'status' => 'Estado',
            'amount' => 'Monto',
            'action' => 'Opción',
            'add-title' => 'Crear factura',
            'save-btn-title' => 'Guardar factura',
            'qty' => 'Cantidad',
            'qty-ordered' => 'Cantidad ordenada',
            'qty-to-invoice' => 'Cantidad a facturar',
            'view-title' => 'Factura #:invoice_id',
            'bill-to' => 'Cobrar a',
            'ship-to' => 'Enviar a',
            'print' => 'Imprimir',
            'order-date' => 'Fecha de pedido'
        ],

        'shipments' => [
            'title' => 'Envíos',
            'id' => 'Id',
            'date' => 'Fecha de envío',
            'order-id' => 'ID de pedido',
            'order-date' => 'Fecha de pedido',
            'customer-name' => 'Nombre del cliente',
            'total-qty' => 'Cantidad total',
            'action' => 'Opción',
            'add-title' => 'Crear envío',
            'save-btn-title' => 'Guardar envío',
            'qty-ordered' => 'Cantidad ordenada',
            'qty-to-ship' => 'Cantidad a enviar',
            'available-sources' => 'Recursos disponibles',
            'source' => 'Recurso',
            'select-source' => 'Por favor selecciona un recurso',
            'qty-available' => 'Cantidad disponible',
            'inventory-source' => 'Recurso de inventario',
            'carrier-title' => 'Título de transportista',
            'tracking-number' => 'Número de rastreo',
            'view-title' => 'Envío #:shipment_id'
        ]
    ],

    'catalog' => [
        'products' => [
            'title' => 'Productos',
            'add-product-btn-title' => 'Añadir producto',
            'add-title' => 'Añadir producto',
            'edit-title' => 'Editar producto',
            'save-btn-title' => 'Guardar producto',
            'general' => 'General',
            'product-type' => 'Tipo de producto',
            'simple' => 'Simple',
            'configurable' => 'Configurable',
            'familiy' => 'Familia de atributo',
            'sku' => 'Ref',
            'configurable-attributes' => 'Atributos configurables',
            'attribute-header' => 'Atributo(s)',
            'attribute-option-header' => 'Opción(es) de atributo',
            'no' => 'No',
            'yes' => 'Si',
            'disabled' => 'Dehabilitar',
            'enabled' => 'Habilitar',
            'add-variant-btn-title' => 'Añadir variación',
            'name' => 'Nombre',
            'qty' => 'Cantidad',
            'price' => 'Precio',
            'weight' => 'Peso',
            'status' => 'Estado',
            'enabled' => 'Habilitado',
            'disabled' => 'Deshabilitado',
            'add-variant-title' => 'Añadir variación',
            'variant-already-exist-message' => 'Ya existe una variante con las mismas opciones de atributo.',
            'add-image-btn-title' => 'Añadir imagen',
            'mass-delete-success' => 'Todo el índice de productos seleccionado ha sido eliminado exitosamente.',
            'mass-update-success' => 'Todo el índice de productos seleccionado ha sido actualizado exitosamente.'
        ],

        'attributes' => [
            'title' => 'Atributos',
            'add-title' => 'Añadir atributo',
            'edit-title' => 'Editar atributo',
            'save-btn-title' => 'Guardar atributo',
            'general' => 'General',
            'code' => 'Código de atributo',
            'type' => 'Tipo de atributo',
            'text' => 'Texto',
            'textarea' => 'Área de texto',
            'price' => 'Precio',
            'boolean' => 'Boolean',
            'select' => 'Seleccionar',
            'multiselect' => 'Multi selección',
            'datetime' => 'Fecha y hora',
            'date' => 'Fecha',
            'label' => 'Etiqueta',
            'admin' => 'Administrador',
            'options' => 'Opciones',
            'position' => 'Posición',
            'add-option-btn-title' => 'Añadir opción',
            'validations' => 'Validaciones',
            'input_validation' => 'Validación de entrada',
            'is_required' => 'Es requerido',
            'is_unique' => 'Es único',
            'number' => 'Número',
            'decimal' => 'Decimal',
            'email' => 'Correo',
            'url' => 'URL',
            'configuration' => 'Configuración',
            'status' => 'Estado',
            'yes' => 'Si',
            'no' => 'No',
            'value_per_locale' => 'Valor por lenguaje',
            'value_per_channel' => 'Valor por canal',
            'value_per_channel' => 'Valor por canal',
            'is_filterable' => 'Usado en la navegación por capas',
            'is_configurable' => 'Usado para crear un producto configurable',
            'admin_name' => 'Nombre',
            'is_visible_on_front' => 'Visible en la página de vista del producto en la parte frontal'
        ],
        'families' => [
            'title' => 'Familias',
            'add-family-btn-title' => 'Añadir familia',
            'add-title' => 'Añadir familia',
            'edit-title' => 'Editar familia',
            'save-btn-title' => 'Guardar familia',
            'general' => 'General',
            'code' => 'Código de familia',
            'name' => 'Nombre',
            'groups' => 'Grupos',
            'add-group-title' => 'Añadir grupo',
            'position' => 'Posición',
            'attribute-code' => 'Código',
            'type' => 'Tipo',
            'add-attribute-title' => 'Añadir atributos',
            'search' => 'Buscar',
            'group-exist-error' => 'Ya existe un grupo con el mismo nombre.'
        ],
        'categories' => [
            'title' => 'Categorías',
            'add-title' => 'Añadir categoría',
            'edit-title' => 'Editar categoría',
            'save-btn-title' => 'Guardar categoría',
            'general' => 'General',
            'name' => 'Nombre',
            'visible-in-menu' => 'Visible en el menú',
            'yes' => 'Si',
            'no' => 'No',
            'position' => 'Posición',
            'description-and-images' => 'Descripción e imágenes',
            'description' => 'Descripción',
            'parent-category' => 'Categoría principal',
            'seo' => 'Optimización de motores de búsqueda',
            'slug' => 'Link',
            'meta_title' => 'Título meta',
            'meta_description' => 'Descripción meta',
            'meta_keywords' => 'Palabras clave meta',
            'image' => 'Imágen',
        ]
    ],

    'configuration' => [
        'title' => 'Configuración',
        'save-btn-title' => 'Guardar',
        'save-message' => 'Configuración guardada exitosamente',
        'yes' => 'Si',
        'no' => 'No',
        'delete' => 'Eliminar',

        'tax-categories' => [
            'title' => 'Categorías de Impuestos',
            'add-title' => 'Añadir categoría de Impuesto',
            'edit-title' => 'Editar categoría de Impuesto',
            'save-btn-title' => 'Guardar categoría de Impuesto',
            'general' => 'Categoría de Impuesto',
            'select-channel' => 'Seleccionar canal',
            'name' => 'Nombre',
            'code' => 'Código',
            'description' => 'Descripción',
            'select-taxrates' => 'Selecciona las tasas de impuestos',
            'edit' => [
                'title' => 'Editar categoría de Impuesto',
                'edit-button-title' => 'Editar categoría de Impuesto'
            ]
        ],

        'tax-rates' => [
            'title' => 'Tarifa de impuestos',
            'add-title' => 'Añadir tasa de impuestos',
            'edit-title' => 'Editar tasa de impuestos',
            'save-btn-title' => 'Guardar tasa de impuestos',
            'general' => 'Tarifa de impuestos',
            'identifier' => 'Identificador',
            'is_zip' => 'Habilitar rango de despacho',
            'zip_from' => 'Desde',
            'zip_to' => 'Hasta',
            'state' => 'Estado',
            'select-state' => 'Seleccione una región, estado o provincia.',
            'country' => 'País',
            'tax_rate' => 'Tarifa',
            'edit' => [
                'title' => 'Editar tasa de impuestos',
                'edit-button-title' => 'Editar tasa'
            ],
            'zip_code' => 'Código Zip',
            'is_zip' => 'Habilitar rango de despacho',
        ],

        'sales' => [
            'shipping-method' => [
                'title' => 'Métodos de envío',
                'save-btn-title' => 'Guardar',
                'description' => 'Descripción',
                'active' => 'Activo',
                'status' => 'Estado'
            ]
        ]
    ],

    'settings' => [
        'locales' => [
            'title' => 'Lenguajes',
            'add-title' => 'Añadir lenguaje',
            'edit-title' => 'Editar lenguaje',
            'add-title' => 'Añadir lenguaje',
            'save-btn-title' => 'Guardar lenguaje',
            'general' => 'General',
            'code' => 'Código',
            'name' => 'Nombre'
        ],
        'countries' => [
            'title' => 'Paises',
            'add-title' => 'Añadir país',
            'save-btn-title' => 'Guardar país',
            'general' => 'General',
            'code' => 'Código',
            'name' => 'Nombre'
        ],
        'currencies' => [
            'title' => 'Monedas',
            'add-title' => 'Añadir moneda',
            'edit-title' => 'Editar moneda',
            'save-btn-title' => 'Guardar moneda',
            'general' => 'General',
            'code' => 'Código',
            'name' => 'Nombre',
            'symbol' => 'Símbolo'
        ],
        'exchange_rates' => [
            'title' => 'Tipo de cambio',
            'add-title' => 'Añadir tipo de cambio',
            'edit-title' => 'Editar tipo de cambio',
            'save-btn-title' => 'Guardar tipo de cambio',
            'general' => 'General',
            'source_currency' => 'Moneda de origen',
            'target_currency' => 'Moneda de destino',
            'rate' => 'Tarifa'
        ],
        'inventory_sources' => [
            'title' => 'Recursos de inventario',
            'add-title' => 'Añadir recurso de inventario',
            'edit-title' => 'Editar recurso de inventario',
            'save-btn-title' => 'Guardar recurso de inventario',
            'general' => 'General',
            'code' => 'Código',
            'name' => 'Nombre',
            'description' => 'Descripción',
            'source-is-active' => 'Recurso activo',
            'contact-info' => 'Información de contacto',
            'contact_name' => 'Nombre',
            'contact_email' => 'Correo',
            'contact_number' => 'Numero de contacto',
            'contact_fax' => 'Fax',
            'address' => 'Dirección de recurso',
            'country' => 'País',
            'state' => 'Estado',
            'city' => 'Ciudad',
            'street' => 'Calle',
            'postcode' => 'Código postal',
            'priority' => 'Prioridad',
            'latitude' => 'Latitud',
            'longitude' => 'Longitud',
            'status' => 'Estado'
        ],
        'channels' => [
            'title' => 'Canales',
            'add-title' => 'Añadir canal',
            'edit-title' => 'Editar canal',
            'save-btn-title' => 'Guardar canal',
            'general' => 'General',
            'code' => 'Código',
            'name' => 'Nombre',
            'description' => 'Descripción',
            'hostname' => 'Nombre de host',
            'currencies-and-locales' => 'Moneda',
            'locales' => 'Lenguajes',
            'default-locale' => 'Lenguaje por defecto',
            'currencies' => 'Monedas',
            'base-currency' => 'Moneda base',
            'root-category' => 'Categoría raíz',
            'inventory_sources' => 'Fuentes de inventario',
            'design' => 'Diseño',
            'theme' => 'Tema',
            'home_page_content' => 'Contenido de la página de inicio',
            'footer_content' => 'Contenido del pie de página',
            'logo' => 'Logo',
            'favicon' => 'Favicon'
        ],

        'sliders' => [
            'title' => 'Banners',
            'add-title' => 'Crear banner',
            'edit-title' => 'Editar banner',
            'save-btn-title' => 'Guardar banner',
            'general' => 'General',
            'image' => 'Imagen',
            'content' => 'Contenido',
            'channels' => 'Canal',
            'created-success' => 'Banner creado con éxito',
            'created-fault' => 'Error creando el banner',
            'update-success' => 'Banner actualizado con éxito',
            'update-fail' => 'El banner no se puede actualizar',
            'delete-success' => 'No se puede eliminar el último banner',
            'delete-fail' => 'Banner eliminado exitosamente'
        ],

        'tax-categories' => [
            'title' => 'Categorías de impuesto',
            'add-title' => 'Crear categoría de impuesto',
            'edit-title' => 'Editar categoría de impuesto',
            'save-btn-title' => 'Guardar categoría de impuesto',
            'general' => 'Categoría de impuesto',
            'select-channel' => 'Seleccionar canal',
            'name' => 'Nombre',
            'code' => 'Código',
            'description' => 'Descripción',
            'select-taxrates' => 'Selecciona las tasas de impuestos',
            'edit' => [
                'title' => 'Editar categoría de impuesto',
                'edit-button-title' => 'Editar categoría de impuesto'
            ],
            'create-success' => 'Nueva categoría de impuesto creada',
            'create-error' => 'Error creando categoría de impuesto',
            'update-success' => 'Categoría de impuesto actualizada exitosamente',
            'update-error' => 'Error actualizando categoría de impuesto',
            'atleast-one' => 'No se puede eliminar la última categoría de impuesto',
            'delete' => 'Categoría de impuesto eliminada exitosamente'
        ],

        'tax-rates' => [
            'title' => 'Tarifas de impuesto',
            'add-title' => 'Crear tasa de impuesto',
            'edit-title' => 'Editar tasa de impuesto',
            'save-btn-title' => 'Guardar tasa de impuesto',
            'general' => 'Tarifa de impuesto',
            'identifier' => 'Identificador',
            'is_zip' => 'Habilitar rango de despacho',
            'zip_from' => 'Desde',
            'zip_to' => 'Hasta',
            'state' => 'Estado',
            'select-state' => 'Seleccione una región, estado o provincia.',
            'country' => 'País',
            'tax_rate' => 'Tarifa',
            'edit' => [
                'title' => 'Editar tarifa de impuesto',
                'edit-button-title' => 'Editar tarifa'
            ],
            'zip_code' => 'Código Zip',
            'is_zip' => 'Habilitar rango de despacho',
            'create-success' => 'Tarifa de impuestos creada con éxito',
            'create-error' => 'No se pudo crear la tarifa de impuestos',
            'update-success' => 'Tarifa de impuestos actualizada con éxito',
            'update-error' => 'Error! Actualizando la tarifa de impuestos',
            'delete' => 'Tarifa de impuestos eliminada con éxito',
            'atleast-one' => 'No se puede eliminar la tarifa de impuestos'
        ]
    ],

    'customers' => [
        'groups' =>[
            'add-title' => 'Añadir grupo',
            'edit-title' => 'Editar grupo',
            'save-btn-title' => 'Guardar grupo',
            'title' => 'Grupos',
            'save-btn-title' => 'Guardar grupo',
            'name' => 'Nombre',
            'is_user_defined' => 'Usuario definido',
            'yes' => 'Si'
        ],
        'customers' => [
            'add-title' => 'Añadir cliente',
            'edit-title' => 'Editar cliente',
            'title' => 'Clientes',
            'first_name' => 'Nombre',
            'last_name' => 'Apellido',
            'gender' => 'Genero',
            'email' => 'Correo',
            'date_of_birth' => 'Fecha de nacimiento',
            'phone' => 'Teléfono',
            'customer_group' => 'Grupo de clientes',
            'save-btn-title' => 'Guardar cliente',
            'channel_name' => 'Nombre del canal',
            'state' => 'Estado',
            'select-state' => 'Seleccione una región, estado o provincia.',
            'country' => 'País',
            'male' => 'Masculino',
            'female' => 'Femenino',
            'phone' => 'Teléfono',
            'group-default' => 'No se puede eliminar el grupo predeterminado.',
        ],
        'reviews' => [
            'title' => 'Opiniones',
            'edit-title' => 'Editar opinión',
            'rating' => 'Calificación',
            'status' => 'Estado',
            'comment' => 'Comentario',
            'pending' => 'Pendiente',
            'approved' => 'Aprovado'
        ],

        'subscribers' => [
            'title' => 'Suscriptores',
            'title-edit' => 'Editar suscriptor',
            'email' => 'Correo',
            'is_subscribed' => 'Suscrito',
            'edit-btn-title' => 'Editar suscriptor',
            'update-success' => 'Suscriptor actualizado con éxito',
            'update-failed' => 'Error! No pudimos dar de baja al suscriptor',
            'delete' => 'Suscriptor eliminado con éxito',
            'delete-failed' => 'Erorr! No pudimos eliminar al suscriptor'
        ]
    ],

    'customers' => [
        'groups' =>[
            'add-title' => 'Añadir grupo',
            'edit-title' => 'Editar grupo',
            'save-btn-title' => 'Guardar grupo',
            'title' => 'Grupos',
            'save-btn-title' => 'Guardar grupo',
            'name' => 'Nombre',
            'is_user_defined' => 'Usuario definido',
            'yes' => 'Si'
        ],
        'customers' => [
            'add-title' => 'Añadir cliente',
            'edit-title' => 'Editar cliente',
            'title' => 'Clientes',
            'first_name' => 'Nombre',
            'last_name' => 'Apellido',
            'gender' => 'Genero',
            'email' => 'Correo',
            'date_of_birth' => 'Fecha de nacimiento',
            'phone' => 'Teléfono',
            'customer_group' => 'Grupo de clientes',
            'save-btn-title' => 'Guardar cliente',
            'channel_name' => 'Nombre del canal',
            'state' => 'Estado',
            'select-state' => 'Seleccione una región, estado o provincia.',
            'country' => 'País',
            'male' => 'Masculino',
            'female' => 'Femenino',
            'phone' => 'Teléfono',
            'group-default' => 'No se puede eliminar el grupo predeterminado.',
        ],
        'reviews' => [
            'title' => 'Opiniones',
            'edit-title' => 'Editar opinión',
            'rating' => 'Calificación',
            'status' => 'Estado',
            'comment' => 'Comentario',
            'pending' => 'Pendiente',
            'approved' => 'Aprovado'
        ],

        'subscribers' => [
            'title' => 'Suscriptores',
            'title-edit' => 'Editar suscriptor',
            'email' => 'Correo',
            'is_subscribed' => 'Suscrito',
            'edit-btn-title' => 'Editar suscriptor',
            'update-success' => 'Suscriptor actualizado con éxito',
            'update-failed' => 'Error! No pudimos dar de baja al suscriptor',
            'delete' => 'Suscriptor eliminado con éxito',
            'delete-failed' => 'Erorr! No pudimos eliminar al suscriptor'
        ]
    ],

    'error' => [
        'go-to-home' => 'IR AL INICIO',

        '404' => [
            'page-title' => '404 Página no encontrada',
            'name' => '404',
            'title' => 'Página no encontrada',
            'message' => 'La página que buscas no existe o se ha movido. Utiliza el menú para volver a navegar.'
        ],
        '403' => [
            'page-title' => '403 Acceso denegado',
            'name' => '403',
            'title' => 'Acceso denegado',
            'message' => 'Usted no tiene permiso para acceder a esta página'
        ],
        '500' => [
            'page-title' => '500 Error interno en el servidor',
            'name' => '500',
            'title' => 'Error interno en el servidor',
            'message' => 'El servidor encontró un error interno.'
        ],
        '401' => [
            'page-title' => '401 Error, no autorizado',
            'name' => '401',
            'title' => 'Error, no autorizado',
            'message' => 'La solicitud no se ha aplicado porque careces de credenciales de autenticación válidas para el recurso de destino.'
        ],
    ],

    'export' => [
       'export' => 'Exportar',
       'format' => 'Selecciona el formato',
       'download' => 'Descargar',
       'csv' => 'CSV',
       'xls' => 'XLS'
    ],

    'response' => [
        'create-success' => ':name creado exitosamente.',
        'update-success' => ':name actualizado exitosamente.',
        'delete-success' => ':name eliminado exitosamente.',
        'last-delete-error' => 'Al menos un :name es requerido.',
        'user-define-error' => 'No se puede eliminar el sistema :name',
        'attribute-error' => ':name se utiliza en productos configurables.',
        'attribute-product-error' => ':name se utiliza en productos.',
        'customer-associate' => ':name no se puede eliminar porque el cliente está asociado a este grupo.',
        'currency-delete-error' => 'Esta moneda se establece como la moneda base del canal por lo que no se puede eliminar.'
    ],
];
