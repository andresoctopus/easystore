<?php

namespace Webkul\Admin\Http\Controllers\Sales;

use App\Models\OrderStatus;
use App\Notifications\OrderStatusNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Sales\Repositories\OrderRepository as Order;

/**
 * Sales Order controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;

    /**
     * OrderRepository object
     *
     * @var array
     */
    protected $order;

    /**
     * Create a new controller instance.
     *
     * @param Webkul\Sales\Repositories\OrderRepository $order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->middleware('admin');

        $this->_config = request('_config');

        $this->order = $order;

    }

    public function updateOrderStatus(Request $request)
    {

        $input = $request->all();

        $order = $this->order->find($input['order_id']);

        $order->order_status_id = $input['order_status_id'];

        $order->save();

        $orderStatus = OrderStatus::find($input['order_status_id']);


        if ($input['order_status_id'] != null && !empty($orderStatus)) {

            Notification::route('mail', $order->customer_email)->notify(new OrderStatusNotification($order, $orderStatus->template));

            session()->flash('success', trans('Estado actualizado'));
        }

        return redirect()->to('/admin/sales/orders/view/' . $input['order_id']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the view for the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $order = $this->order->find($id);

        $orderStatuses = OrderStatus::all();

        return view($this->_config['view'], compact('order', 'orderStatuses'));
    }

    /**
     * Cancel action for the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id)
    {
        $result = $this->order->cancel($id);

        if ($result) {
            session()->flash('success', trans('Orden cancelada.'));
        } else {
            session()->flash('error', trans('La orden no puede ser cancelada.'));
        }

        return redirect()->back();
    }
}