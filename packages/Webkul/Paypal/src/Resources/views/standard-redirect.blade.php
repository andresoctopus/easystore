<?php $paypalStandard = app('Webkul\Paypal\Payment\Standard') ?>

<body data-gr-c-s-loaded="true" cz-shortcut-listen="true">
   Serás redireccionado(a) en unos segunndos.
    

    <form action="{{ $paypalStandard->getPaypalUrl() }}" id="paypal_standard_checkout" method="POST">
        <input value="Haz clic aquí si no eres redireccionado(a) en 10 segundos..." type="submit">

        @foreach ($paypalStandard->getFormFields() as $name => $value)

            <input type="hidden" name="{{ $name }}" value="{{ $value }}">

        @endforeach
    </form>

    <script type="text/javascript">
        document.getElementById("paypal_standard_checkout").submit();
    </script>
</body>
