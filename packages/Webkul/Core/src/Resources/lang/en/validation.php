<?php

return [
    'slug' => 'El :attribute debe ser una ruta válida.',
    'code' => 'El :attribute debe ser válido.',
    'decimal' => 'El :attribute debe ser válido.'
];
