<?php

namespace Webkul\Product\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Webkul\Attribute\Repositories\AttributeFamilyRepository as AttributeFamily;
use Webkul\Category\Repositories\CategoryRepository as Category;
use Webkul\Inventory\Repositories\InventorySourceRepository as InventorySource;
use Webkul\Product\Repositories\ProductAttributeValueRepository as ProductAttributeValue;
use Webkul\Product\Repositories\ProductFlatRepository as ProductFlat;
use Webkul\Product\Repositories\ProductGridRepository as ProductGrid;
use Webkul\Product\Repositories\ProductRepository as Product;
use Webkul\Product\Util\ProductImport;

/**
 * Product controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class ImportProductController extends Controller {
	/**
	 * Contains route related configuration
	 *
	 * @var array
	 */
	protected $_config;

	/**
	 * AttributeFamilyRepository object
	 *
	 * @var array
	 */
	protected $attributeFamily;

	/**
	 * CategoryRepository object
	 *
	 * @var array
	 */
	protected $category;

	/**
	 * InventorySourceRepository object
	 *
	 * @var array
	 */
	protected $inventorySource;

	/**
	 * ProductRepository object
	 *
	 * @var array
	 */
	protected $product;

	/**
	 * ProductGrid Repository object
	 *
	 * @var array
	 */
	protected $productGrid;
	protected $productFlat;
	protected $productAttributeValue;

	/**
	 * Create a new controller instance.
	 *
	 * @param Webkul\Attribute\Repositories\AttributeFamilyRepository $attributeFamily
	 * @param Webkul\Category\Repositories\CategoryRepository $category
	 * @param Webkul\Inventory\Repositories\InventorySourceRepository $inventorySource
	 * @param Webkul\Product\Repositories\ProductRepository $product
	 *
	 * @return void
	 */
	public function __construct(
		AttributeFamily $attributeFamily,
		Category $category,
		InventorySource $inventorySource,
		Product $product,
		ProductGrid $productGrid,
		ProductFlat $productFlat,
		ProductAttributeValue $productAttributeValue
	) {
		$this->attributeFamily = $attributeFamily;

		$this->category = $category;

		$this->inventorySource = $inventorySource;

		$this->product = $product;

		$this->productGrid = $productGrid;

		$this->productFlat = $productFlat;

		$this->productAttributeValue = $productAttributeValue;

		$this->_config = request( '_config' );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		return view( $this->_config['view'] );
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {


		return view( $this->_config['view'] );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store() {
		$file = request()->file( 'fileProducts' );
		$name = $file->getClientOriginalName();
		$now  = Carbon::now()->timestamp;
		$name = $now . $name;
		Storage::put( $name, File::get( $file ) );
		$path = storage_path( "app/public/" . $name );

		try {
			DB::beginTransaction();
			Excel::import( new ProductImport, $path );
			DB::commit();
		} catch ( \Exception $e ) {
			Log::error( "An exception whe import product", [ 'exception' => $e ] );
			DB::rollback();
		}

		session()->flash( 'success', 'Operación ejecutada exitosamente!' );

		return view( $this->_config['view'] );
	}


}
