<?php

namespace Webkul\Product\Util;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToCollection;
use Webkul\Category\Models\Category;
use Webkul\Core\Models\Channel;
use Webkul\Inventory\Models\InventorySource;
use Webkul\Product\Models\Product;
use Webkul\Product\Models\ProductAttributeValue;
use Webkul\Product\Models\ProductFlat;
use Webkul\Product\Models\ProductGrid;
use Webkul\Product\Models\ProductImage;
use Webkul\Product\Models\ProductInventory;

/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 29/03/19
 * Time: 02:58 PM
 */
class ProductImport implements ToCollection
{

    /**
     * @param Collection $collection
     */

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function collection(Collection $collection)
    {

        foreach ($collection as $index => $row) {
            if ($index > 0) {
                $sku = $row[0];
                $title = $row[1];
                $dataCategories = explode(',', trim($row[3]));
                $categories = [];
                foreach ($dataCategories as $id) {
                    Log::debug("this is the item", ['category' => $id]);
                    $item = Category::where('id', '=', trim($id))->get()->first();
                    if (!empty($item)) {
                        array_push($categories, $id);
                    }
                }

                $descriptionHtml = $row[4];
                $description = $row[5];
                $price = $row[6];

                $priceSpecial = $row[7];
                $startPriceSpecial = $row[8];
                $endPriceSpecial = $row[9];

                $quantity = $row[10];


                $dataChannel = explode(',', $row[17]);
                $channels = [];
                foreach (Channel::all() as $channel) {
                    foreach ($dataChannel as $id) {
                        if (trim($channel->code) == trim($id)) {
                            array_push($channels, $channel->code);
                        }
                    }
                }
                if (count($channels) <= 0) {
                    $channels = ['default'];
                }

                $policies = $row[18];

                $paths = [];
                $images = [];

                if ($title != null && !empty($title)) {
                    $images[] = $row[11];
                    $images[] = $row[12];
                    $images[] = $row[13];
                    $images[] = $row[14];
                    $images[] = $row[15];
                    $images[] = $row[16];


                    foreach ($images as $url) {
                        if ($url != null) {
                            try {
                                /*Log::debug( "this is the image", [ 'mage1' => $url ] );
                                $now   = Carbon::now()->timestamp;
                                $image = Image::make( $url );
                                $mime  = $image->mime();
                                if ( $mime == 'image/jpeg' ) {
                                    $extension = '.jpg';
                                } elseif ( $mime == 'image/png' ) {
                                    $extension = '.png';
                                } elseif ( $mime == 'image/gif' ) {
                                    $extension = '.gif';
                                } elseif ( $mime == 'image/webp' ) {
                                    $extension = '.webp';
                                } else {
                                    $extension = '';
                                }
                                Log::debug( "this is the image", [ 'mage1' => $url, 'extension' => $extension ] );
                                $image->save( storage_path( 'app/public/product/' . $now . $extension ) );
                                array_push( $paths, 'product/' . $now . $extension );*/
                                $type = substr($url, strrpos($url, '.') + 0);
                                $content = file_get_contents($url);
                                $now = $this->generateRandomString(50);
                                file_put_contents(storage_path('app/public/product/' . $now . $type), $content);
                                array_push($paths, 'product/' . $now . $type);
                                Log::debug("this is the path", ['type' => $type]);

                            } catch (\Exception $exception) {
                                Log::error("error", ['exception' => $exception]);
                            }
                        }
                    }


                    $product = new Product();
                    $product->sku = $sku != null ? $sku : rand(0, 100000000);
                    $product->type = 'simple';
                    $product->attribute_family_id = 1;
                    $product->save();


                    foreach ($categories as $category) {
                        DB::insert('insert into product_categories (product_id, category_id) values (?, ?)', [
                            $product->id,
                            $category
                        ]);
                    }

                    $productGrid = new ProductGrid();
                    $productGrid->product_id = $product->id;
                    $productGrid->attribute_family_name = 'default';
                    $productGrid->sku = $product->sku;
                    $productGrid->type = 'simple';
                    $productGrid->name = $title;
                    $productGrid->quantity = $quantity != null ? $quantity : 0;
                    $productGrid->price = $price;
                    $productGrid->status = 1;
                    $productGrid->save();

                    foreach ($channels as $channelCode) {
                        $productAttributeValues = new ProductAttributeValue();
                        $productAttributeValues->locale = 'en';
                        $productAttributeValues->channel = $channelCode;
                        $productAttributeValues->text_value = $title;
                        $productAttributeValues->product_id = $product->id;
                        $productAttributeValues->attribute_id = 2;
                        $productAttributeValues->save();

                        $productAttributeValues = new ProductAttributeValue();
                        $productAttributeValues->locale = 'en';
                        $productAttributeValues->channel = $channelCode;
                        $productAttributeValues->text_value = urlencode($product->sku);
                        $productAttributeValues->product_id = $product->id;
                        $productAttributeValues->attribute_id = 3;
                        $productAttributeValues->save();

                        $productAttributeValues = new ProductAttributeValue();
                        $productAttributeValues->locale = 'en';
                        $productAttributeValues->channel = $channelCode;
                        $productAttributeValues->text_value = $description;
                        $productAttributeValues->product_id = $product->id;
                        $productAttributeValues->attribute_id = 9;

                        $productAttributeValues->save();


                        $productAttributeValues = new ProductAttributeValue();
                        $productAttributeValues->locale = 'en';
                        $productAttributeValues->channel = $channelCode;
                        $productAttributeValues->boolean_value = 1;
                        $productAttributeValues->product_id = $product->id;
                        $productAttributeValues->attribute_id = 8;

                        $productAttributeValues->save();

                        $productAttributeValues = new ProductAttributeValue();
                        $productAttributeValues->locale = 'en';
                        $productAttributeValues->channel = $channelCode;
                        $productAttributeValues->boolean_value = 1;
                        $productAttributeValues->product_id = $product->id;
                        $productAttributeValues->attribute_id = 7;

                        $productAttributeValues->save();

                        $productAttributeValues = new ProductAttributeValue();
                        $productAttributeValues->locale = 'en';
                        $productAttributeValues->channel = $channelCode;
                        $productAttributeValues->text_value = $descriptionHtml;
                        $productAttributeValues->product_id = $product->id;
                        $productAttributeValues->attribute_id = 10;

                        $productAttributeValues->save();

                        $productAttributeValues = new ProductAttributeValue();
                        $productAttributeValues->locale = 'en';
                        $productAttributeValues->channel = $channelCode;
                        $productAttributeValues->float_value = $price;
                        $productAttributeValues->product_id = $product->id;
                        $productAttributeValues->attribute_id = 11;
                        $productAttributeValues->save();

                        /* $productAttributeValues = new ProductAttributeValue();
                         $productAttributeValues->locale = 'en';
                         $productAttributeValues->channel = $channelCode;
                         $productAttributeValues->text_value = $color;
                         $productAttributeValues->product_id = $product->id;
                         $productAttributeValues->attribute_id = 23;

                         $productAttributeValues->save();*/


                        $productAttributeValues = new ProductAttributeValue();
                        $productAttributeValues->locale = 'en';
                        $productAttributeValues->channel = $channelCode;
                        $productAttributeValues->text_value = 1;
                        $productAttributeValues->product_id = $product->id;
                        $productAttributeValues->attribute_id = 22;
                        $productAttributeValues->save();


                        $productAttributeValues = new ProductAttributeValue();
                        $productAttributeValues->locale = 'en';
                        $productAttributeValues->channel = $channelCode;
                        $productAttributeValues->text_value = $policies;
                        $productAttributeValues->product_id = $product->id;
                        $productAttributeValues->attribute_id = 29;
                        $productAttributeValues->save();

                        $inventorySource = InventorySource::where('code', '=', trim($channelCode))->get()->first();
                        $productInventory = new ProductInventory();
                        $productInventory->qty = $quantity != null ? $quantity : 0;
                        $productInventory->product_id = $product->id;
                        $productInventory->inventory_source_id = $inventorySource->id;
                        $productInventory->save();
                    }


                    foreach ($paths as $uri) {
                        $productImage = new ProductImage();
                        $productImage->path = $uri;
                        $productImage->product_id = $product->id;
                        $productImage->save();
                    }


                    Event::fire('catalog.product.create.after', $product);

                    foreach ($channels as $channelCode) {
                        if (!empty($priceSpecial) && !empty($startPriceSpecial) && !empty($endPriceSpecial)) {
                            try {
                                $productFlat = ProductFlat::where('product_id', '=', $product->id)
                                    ->where('channel', '=', $channelCode)
                                    ->get()->first();
                                if (!empty($productFlat)) {
                                    $productFlat->special_price = $priceSpecial;
                                    $productFlat->special_price_from = Carbon::parse($startPriceSpecial)->toDateString();
                                    $productFlat->special_price_to = Carbon::parse($endPriceSpecial)->toDateString();
                                    $productFlat->save();

                                    $productAttributeValues = new ProductAttributeValue();
                                    $productAttributeValues->locale = 'en';
                                    $productAttributeValues->channel = $channelCode;
                                    $productAttributeValues->float_value = $priceSpecial;
                                    $productAttributeValues->product_id = $product->id;
                                    $productAttributeValues->attribute_id = 13;
                                    $productAttributeValues->save();

                                    $productAttributeValues = new ProductAttributeValue();
                                    $productAttributeValues->locale = 'en';
                                    $productAttributeValues->channel = $channelCode;
                                    $productAttributeValues->date_Value = Carbon::parse($startPriceSpecial)->toDateString();
                                    $productAttributeValues->product_id = $product->id;
                                    $productAttributeValues->attribute_id = 14;
                                    $productAttributeValues->save();

                                    $productAttributeValues = new ProductAttributeValue();
                                    $productAttributeValues->locale = 'en';
                                    $productAttributeValues->channel = $channelCode;
                                    $productAttributeValues->date_Value = Carbon::parse($endPriceSpecial)->toDateString();
                                    $productAttributeValues->product_id = $product->id;
                                    $productAttributeValues->attribute_id = 15;
                                    $productAttributeValues->save();
                                }
                            } catch (\Exception $exception) {
                                Log::error('Que imbecil,', [
                                    'error' => $exception,
                                    '1' => $priceSpecial,
                                    '2' => $startPriceSpecial,
                                    '3' => $endPriceSpecial
                                ]);
                            }
                        }
                    }
                }

            }
        }
    }
}
