<?php

namespace Webkul\Product\Util;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Concerns\ToCollection;
use Webkul\Category\Models\CategoryTranslation;
use Webkul\Core\Models\Channel;
use Webkul\Inventory\Models\InventorySource;
use Webkul\Product\Models\Product;
use Webkul\Product\Models\ProductAttributeValue;
use Webkul\Product\Models\ProductFlat;
use Webkul\Product\Models\ProductGrid;
use Webkul\Product\Models\ProductImage;
use Webkul\Product\Models\ProductInventory;

/**
 * Created by PhpStorm.
 * User: andresdkm
 * Date: 29/03/19
 * Time: 02:58 PM
 */
class ProductImport implements ToCollection
{

    /**
     * @param Collection $collection
     */
    public function collection(Collection $collection)
    {

        foreach ($collection as $index => $row) {
            if (count($row) == 21 && $index > 0) {
                $sku = $row[0];
                $title = $row[1];
                $category = $row[2];
                $descriptionHtml = $row[3];
                $description = $row[4];
                $price = $row[5];
                if(!is_numeric($price)){
                    $price = 0;
                }
                $special_price = $row[6];
                $quantity = $row[8];
                $politicas = $row[21];
	            $channelName = $row[20];
				$channelCode = 'default';
	            $channelModel = Channel::where('code','=',$channelName)->get()->first();

	            if(!empty($channelModel)){
		            $channelCode = $channelModel->code;
	            }


                if ($title != null && !empty($title)) {
                    $images = [];
                    $images[] = $row[14];
                    $images[] = $row[15];
                    $images[] = $row[16];
                    $images[] = $row[17];
                    $images[] = $row[18];
                    $images[] = $row[19];

                    $paths = [];

                    foreach ($images as $url) {
                        if ($url != null) {
                            try {
                                Log::debug("this is the image", ['mage1' => $url]);
                                $now = Carbon::now()->timestamp;
                                $image = Image::make($url);
                                $mime = $image->mime();
                                if ($mime == 'image/jpeg')
                                    $extension = '.jpg';
                                elseif ($mime == 'image/png')
                                    $extension = '.png';
                                elseif ($mime == 'image/gif')
                                    $extension = '.gif';
                                else
                                    $extension = '';
                                Log::debug("this is the image", ['mage1' => $url, 'extension' => $extension]);
                                $image->save(storage_path('app/public/product/' . $now . $extension));
                                array_push($paths, 'product/' . $now . $extension);
                            } catch (\Exception $exception) {
                                Log::error("error", ['exception' => $exception]);
                            }
                        }
                    }


                    $categoryTranslation = CategoryTranslation::where('name', '=', $category)->get()->first();
                    if (empty($categoryTranslation)) {
                        $categoryTranslation = CategoryTranslation::where('category_id', '=', 1)->get()->first();
                    }

                    $product = new Product();
                    $product->sku = $sku != null ? $sku : rand(0, 100000000);
                    $product->type = 'simple';
                    $product->attribute_family_id = 1;
                    $product->save();

                    $productGrid = new ProductGrid();
                    $productGrid->product_id = $product->id;
                    $productGrid->attribute_family_name = 'Default';
                    $productGrid->sku = $product->sku;
                    $productGrid->type = 'simple';
                    $productGrid->name = $title;
                    $productGrid->quantity = $quantity != null ? $quantity : 0;
                    $productGrid->price = $price;
                    $productGrid->special_price = $special_price;
                    $productGrid->status = 1;
                    $productGrid->save();


                    $productAttributeValues = new ProductAttributeValue();
                    $productAttributeValues->locale = 'en';
                    $productAttributeValues->channel = $channelCode;
                    $productAttributeValues->text_value = $title;
                    $productAttributeValues->product_id = $product->id;
                    $productAttributeValues->attribute_id = 2;
                    $productAttributeValues->save();

                    $productAttributeValues = new ProductAttributeValue();
                    $productAttributeValues->locale = 'en';
                    $productAttributeValues->channel = $channelCode;
                    $productAttributeValues->text_value = urlencode($product->sku);
                    $productAttributeValues->product_id = $product->id;
                    $productAttributeValues->attribute_id = 3;
                    $productAttributeValues->save();

                    $productAttributeValues = new ProductAttributeValue();
                    $productAttributeValues->locale = 'en';
                    $productAttributeValues->channel = $channelCode;
                    $productAttributeValues->text_value = $description;
                    $productAttributeValues->product_id = $product->id;
                    $productAttributeValues->attribute_id = 9;

                    $productAttributeValues->save();


                    $productAttributeValues = new ProductAttributeValue();
                    $productAttributeValues->locale = 'en';
                    $productAttributeValues->channel = $channelCode;
                    $productAttributeValues->boolean_value = 1;
                    $productAttributeValues->product_id = $product->id;
                    $productAttributeValues->attribute_id = 8;

                    $productAttributeValues->save();

                    $productAttributeValues = new ProductAttributeValue();
                    $productAttributeValues->locale = 'en';
                    $productAttributeValues->channel = $channelCode;
                    $productAttributeValues->boolean_value = 1;
                    $productAttributeValues->product_id = $product->id;
                    $productAttributeValues->attribute_id = 7;

                    $productAttributeValues->save();

                    $productAttributeValues = new ProductAttributeValue();
                    $productAttributeValues->locale = 'en';
                    $productAttributeValues->channel = $channelCode;
                    $productAttributeValues->text_value = $descriptionHtml;
                    $productAttributeValues->text_value = $politicas;
                    $productAttributeValues->product_id = $product->id;
                    $productAttributeValues->attribute_id = 10;

                    $productAttributeValues->save();

                    $productAttributeValues = new ProductAttributeValue();
                    $productAttributeValues->locale = 'en';
                    $productAttributeValues->channel = $channelCode;
                    $productAttributeValues->float_value = $price;
                    $productAttributeValues->product_id = $product->id;
                    $productAttributeValues->attribute_id = 11;
                    $productAttributeValues->save();

                   /* $productAttributeValues = new ProductAttributeValue();
                    $productAttributeValues->locale = 'en';
                    $productAttributeValues->channel = $channelCode;
                    $productAttributeValues->text_value = $color;
                    $productAttributeValues->product_id = $product->id;
                    $productAttributeValues->attribute_id = 23;

                    $productAttributeValues->save();*/


                    $productAttributeValues = new ProductAttributeValue();
                    $productAttributeValues->locale = 'en';
                    $productAttributeValues->channel = $channelCode;
                    $productAttributeValues->text_value = 1;
                    $productAttributeValues->product_id = $product->id;
                    $productAttributeValues->attribute_id = 22;

                    $productAttributeValues->save();

                   /* $productAttributeValues = new ProductAttributeValue();
                    $productAttributeValues->locale = 'en';
                    $productAttributeValues->channel = $channelCode;
                    $productAttributeValues->text_value = $size;
                    $productAttributeValues->product_id = $product->id;
                    $productAttributeValues->attribute_id = 24;

                    $productAttributeValues->save();*/

                    foreach ($paths as $path) {
                        $productImage = new ProductImage();
                        $productImage->path = $path;
                        $productImage->product_id = $product->id;
                        $productImage->save();
                    }

                    $inventorySource = InventorySource::where('code','=',$channelCode)->get()->first();
					$inventoryStore = 1;
	                if(!empty($inventorySource)){
		                $inventoryStore = $inventorySource->id;
	                }

                    $productInventory = new ProductInventory();
                    $productInventory->qty = $quantity != null ? $quantity : 0;
                    $productInventory->product_id = $product->id;
                    $productInventory->inventory_source_id = $inventoryStore;
                    $productInventory->save();


                    Event::fire('catalog.product.create.after', $product);

                }

            }
        }
    }
}
